{!! $email->top !!}

<p>Hi {{ $user->first_name }} {{ $user->last_name }},</p>

{!! $email->middle !!}

<hr size="1" color="#c0c0c0"/>
<p style="font-size: 15px; color: #89408d; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 25px 0 25px 0;">
    Order details</p>
<table width="670" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="335" align="left" valign="top">
            <p style="font-size: 13px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                Order</p>
            <p style="font-size: 20px; color: #89408d; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 30px 0;">{{ $order->id }}</p>

            <p style="font-size: 13px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                PO Number</p>
            <p style="font-size: 15px; color: #89408d; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 30px 0;">
                {{ $order->po_number }}</p>

            <p style="font-size: 13px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                Order date/time</p>
            <p style="font-size: 15px; color: #89408d; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 30px 0;">@dateTime($order->created_at)</p>

        </td>
        <td width="335" align="left" valign="top">

            @if (!empty($order->organisation))

                <p style="font-size: 13px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                    School/Organisation</p>
                <p style="font-size: 15px; color: #89408d; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 20px 0;">{{ $order->organisation }}</p>

            @endif

            @if (!empty($order->urn))

                <p style="font-size: 13px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                    School DfE Number</p>
                <p style="font-size: 15px; color: #89408d; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 20px 0;">{{ $order->urn }}</p>

            @endif

            <p style="font-size: 13px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                Delivery address</p>

            <p style="font-size: 15px; color: #89408d; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 20px 0;">

                @if($order->same_address=='no')


                        @if (!empty($order->delivery_contact))
                            {{ $order->delivery_contact }}<br/>
                        @endif
                        @if (!empty($order->delivery_address_line_1))
                            {{ $order->delivery_address_line_1 }}<br/>
                        @endif
                        @if (!empty($order->delivery_address_line_2))
                            {{ $order->delivery_address_line_2 }}<br/>
                        @endif
                        @if (!empty($order->delivery_address_line_3))
                            {{ $order->delivery_address_line_3 }}<br/>
                        @endif
                        @if (!empty($order->delivery_city))
                            {{ $order->delivery_city }}<br/>
                        @endif
                        @if (!empty($order->delivery_postcode))
                            {{ $order->delivery_postcode }}<br/>
                        @endif

                @else

                        @if (!empty($order->address_line_1))
                            {{ $order->address_line_1 }}<br/>
                        @endif
                        @if (!empty($order->address_line_2))
                            {{ $order->address_line_2 }}<br/>
                        @endif
                        @if (!empty($order->address_line_3))
                            {{ $order->address_line_3 }}<br/>
                        @endif
                        @if (!empty($order->city))
                            {{ $order->city }}<br/>
                        @endif
                        @if (!empty($order->postcode))
                            {{ $order->postcode }}<br/>
                        @endif


                @endif



            </p>


            @if (!empty($order->note))

                <p style="font-size: 13px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                    Notes...</p>
                <p style="font-size: 15px; color: #89408d; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 20px 0;">{{ $order->note }}</p>

            @endif


        </td>
    </tr>
</table>

<br/>

<table width="100%" border="1" bordercolor="#222222" cellpadding="5" cellspacing="0">
    <thead>
    <tr>
        <th align="left" valign="top" bgcolor="#c0c0c0"
            style="background-color: #c0c0c0; font-size: 11px; color: #555555; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #c0c0c0; border-style: solid; border-width: 5px;">
            Quantity
        </th>
        <th align="left" valign="top" bgcolor="#c0c0c0"
            style="background-color: #c0c0c0; font-size: 11px; color: #555555; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #c0c0c0; border-style: solid; border-width: 5px;">
            Product
        </th>
        <th align="right" valign="top" bgcolor="#c0c0c0"
            style="background-color: #c0c0c0; font-size: 11px; color: #555555; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #c0c0c0; border-style: solid; border-width: 5px;">
            Subtotal
        </th>
        <th align="right" valign="top" bgcolor="#c0c0c0"
            style="background-color: #c0c0c0; font-size: 11px; color: #555555; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #c0c0c0; border-style: solid; border-width: 5px;">
            Discount
        </th>
        <th align="right" valign="top" bgcolor="#c0c0c0"
            style="background-color: #c0c0c0; font-size: 11px; color: #555555; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #c0c0c0; border-style: solid; border-width: 5px;">
            Total
        </th>
    </tr>
    <thead>
    <tbody>
    @foreach( $order->orderItems  as $item )
        <tr>
            <td align="left" valign="top"
                style="font-size: 13px; color: #222222; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 5px;">{{ $item->qty }}</td>
            <td align="left" valign="top"
                style="font-size: 13px; color: #222222; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 5px;">{{ $item->name }}</td>
            <td align="right" valign="top"
                style="font-size: 13px; color: #222222; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 5px;">
                £{{ $item->subtotal }}</td>
            <td align="right" valign="top"
                style="font-size: 13px; color: #222222; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 5px;">
                £{{ $item->discount }}</td>
            <td align="right" valign="top"
                style="font-size: 13px; color: #222222; font-family: Arial; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 5px;">
                £{{ $item->total }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<br/>

<div align="right">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>

        @if( $order->discount > 0 )
            <tr>
                <td align="right" valign="top"
                    style="font-size: 15px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                    Discount (Code: {{ $order->code }})
                </td>
                <td width="10">&nbsp;</td>
                <td align="left" valign="top"
                    style="font-size: 15px; color: #89408d; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                    -£{{ $order->discount }}</td>
            </tr>
        @endif
        <tr>
            <td align="right" valign="top"
                style="font-size: 20px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                Net total (ex VAT):
            </td>
            <td width="10">&nbsp;</td>
            <td align="left" valign="top"
                style="font-size: 20px; color: #89408d; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                £{{ number_format($order->subtotal, 2, '.', '') }}</td>

        </tr>
        <tr>
            <td align="right" valign="top"
                style="font-size: 15px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                VAT
            </td>
            <td width="10">&nbsp;</td>
            <td align="left" valign="top"
                style="font-size: 15px; color: #89408d; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                £{{ number_format($order->vat, 2, '.', '') }}</td>
        </tr>
        <tr>
            <td align="right" valign="top"
                style="font-size: 15px; color: #222222; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                Total
            </td>
            <td width="10">&nbsp;</td>
            <td align="left" valign="top"
                style="font-size: 15px; color: #89408d; font-family: Arial; font-weight: bold; padding: 0; margin: 0; border-color: #ffffff; border-style: solid; border-width: 0 0 5px 0;">
                £{{ number_format($order->total, 2, '.', '') }}</td>
        </tr>
        </tbody>
    </table>
</div>
<br/>


{!! $email->bottom !!}