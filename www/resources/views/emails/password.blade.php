{!! $email->top !!}

<p>Hi {{ $user->first_name }} {{ $user->last_name }},</p>

{!! $email->middle !!}

<p><a href="{{ route('auth.getReset',$token) }}">{{ route('auth.getReset',$token) }}</a></p>

{!! $email->bottom !!}