{!! $email->top !!}

<p>Hi {{ $user->first_name }} {{ $user->last_name }},</p>

{!! $email->middle !!}

{!! $email->bottom !!}