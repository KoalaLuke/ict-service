@extends('front.layouts.front')

@section('content')

    <div class="main-column right">

        <div class="text-content">

            <h1>Register</h1>

            @include('front.partials.errors')

            <div class="form block box">

                <h2>Create an account...</h2>


                {!! Form::open(['method'=>'POST', 'route'=>['auth.postRegister'], 'class'=>'form-horizontal']) !!}

                @include('front.partials.user_details_form', ['type'=>'add'])

                {!! Form::close() !!}

            </div>

        </div>

    </div>

    <div class="side-column left">

        @include('front.partials.twitter')

    </div>

@endsection
