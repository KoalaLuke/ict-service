@extends('front.layouts.front')

@section('content')

    <div class="main-column right">
        <div class="text-content">


            @if (count($errors) > 0)
                <div class="alert warning block box">
                    <h2>Whoops!</h2>
                    <p>There were some problems with your input.</p>
                    @foreach ($errors->all() as $error)
                        <p><em>{{ $error }}</em></p>
                    @endforeach
                </div>
            @endif

            <div class="form block box">
                <h2>Password reset request...</h2>

                <form class="form-horizontal" role="form" method="POST" action="{{ route('auth.postReset') }}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-row">
                        <label for="email">Email address</label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}"
                               placeholder="Email address" style="cursor: auto;">
                    </div>
                    <div class="form-row">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password"
                               style="cursor: auto;">
                    </div>
                    <div class="form-row">
                        <label for="password_confirmation">Password confirmation</label>
                        <input type="password" class="form-control" name="password_confirmation"
                               placeholder="Password confirmation" style="cursor: auto;">
                    </div>
                    <div class="form-row withlinks">
                        <input type="submit" class="button" value="Reset password"/>
                        <br class="clear">
                    </div>

                </form>

            </div>
        </div>
    </div>

    <div class="side-column left">

        @include('front.partials.twitter')

    </div>

@endsection