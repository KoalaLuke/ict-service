@extends('front.layouts.front')

@section('content')



    <div class="main-column right">

        <div class="text-content">

            <h1>Login</h1>

            @include('front.partials.errors')

            <div class="form block box">
                <h2>Please login...</h2>

                <form class="form-horizontal" role="form" method="POST" action="{{ route('auth.postLogin') }}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-row">

                        <label for="email">Email Address</label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}"
                               placeholder="Email address"/>

                    </div>
                    <div class="form-row">

                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">

                    </div>
                    <div class="form-row withlinks">

                        <input type="submit" class="button" id="login_submit" name="login_submit" value="Login"/>
                        <ul>
                            <li><a href="{{ route('auth.getEmail') }}"><strong>Forgot password?</strong> Click here</a>
                            </li>
                            <li><a href="{{ route('auth.getRegister') }}"><strong>Not registered?</strong> Click
                                    here</a></li>
                        </ul>
                        <br class="clear"/>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <div class="side-column left">

        @include('front.partials.twitter')

    </div>

@endsection
