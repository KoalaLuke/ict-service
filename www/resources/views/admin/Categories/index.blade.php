@extends('admin.layouts.admin')

@section('content')

    <div class="mtl mbl"></div>

    <div class="table-container">
        <table class="table table-hover table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" class="checkall"/>
                </th>
                <th>Image Name</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td colspan="3"><strong>{{ $category->icts_name }}</strong></td>
                </tr>

                @foreach($category->children as $child)

                <tr>
                    <td>
                        <input type="checkbox" class="checkall"/>
                    </td>
                    <td>{{ $child->icts_name }}</td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\ImageController@destroy',$child->id], 'class'=>'form-inline']) !!}
                        <a href="{{ route('admin.category.edit',$child->id) }}" type="button" class="btn btn-default btn-xs">Edit</a>
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

                @endforeach

            @endforeach
            </tbody>
        </table>
    </div>

@endsection