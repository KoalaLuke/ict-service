@extends('admin.layouts.admin')

@section('content')

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th>Product name</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($category->installations as $installation)
                <tr>
                    <td>{{ $installation->product->name }}</td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\InstallationController@destroy',$installation->id], 'class'=>'form-inline']) !!}
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    {!! Form::open(['action' => ['Admin\InstallationController@store'], 'class'=>'form-inline']) !!}

    {!! Form::hidden('icts_productcategoryid',$category->icts_productcategoryid) !!}

    {!! Form::select('productid', $installationProductsSelect, null, ['class'=>'form-control']) !!}

    {!! Form::submit('Add', array('class' => 'btn btn-default')) !!}

    {!! Form::close() !!}

@endsection