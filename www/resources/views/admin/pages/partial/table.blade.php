<div class="table-container">
    <table id="pagesTable" class="table table-hover table-striped table-bordered table-advanced tablesorter">
        <thead>
        <tr>
            <th width="3%">
                <input type="checkbox" class="checkall"/>
            </th>
            <th>Page Name</th>
            <th width="9%">Status</th>
            <th width="12%">Actions</th>
        </tr>
        </thead>
        <tbody class="sortable">
        @foreach($pages as $page)
            <tr id="sortPage-{{ $page->id }}">
                <td>
                    <input type="checkbox" class="checkall"/>
                </td>
                <td>{{ $page->name }}</td>
                <td>
                    @if ($page->active == 1)
                        <span class="label label-sm label-success">Active</span>
                    @else
                        <span class="label label-sm label-info">Inactive</span>
                    @endif
                </td>
                <td>
                    {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\PageController@destroy',$page->id], 'class'=>'form-inline']) !!}
                    <a href="{{ route('admin.page.edit',$page->id) }}" type="button"
                       class="btn btn-default btn-xs">Edit</a>
                    {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>