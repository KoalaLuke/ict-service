<div class="form-body pal">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('active','Active:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::select('active',[1=>'Active', 0=>'Inactive' ], null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('name','Name:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('content','Content:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::textarea('content',null,['class'=>'form-control editor']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('position','Position:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::select('position',['header'=>'Header','footer'=>'Footer','sitemap'=>'Sitemap'], null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('order','Order:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('order',null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('slug','Slug:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('slug',null,['class'=>'form-control']) !!}
        </div>
    </div>

</div>

<div class="form-actions">

    <div class="col-md-offset-3 col-md-9">
        {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
        &nbsp;
        <a class="btn btn-green" href="{{ route('admin.page.index') }}">Cancel</a>
    </div>

</div>