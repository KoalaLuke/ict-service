@extends('admin.layouts.admin')

@section('content')

    <a href="{{ route('admin.page.create') }}" class="btn btn-primary">Add page</a>

    <div class="mtl mbl"></div>

    <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href="#headerlinks" data-toggle="tab">Header</a></li>
        <li class=""><a href="#footerlinks" data-toggle="tab">Footer</a></li>
        <li class=""><a href="#sitemaplinks" data-toggle="tab">Sitemap</a></li>
    </ul>

    <div id="myTabContent" class="tab-content">

        <div id="headerlinks" class="tab-pane fade active in">

            @include('admin.pages.partial.table',['pages' => $header_pages])

        </div>

        <div id="footerlinks" class="tab-pane fade">

            @include('admin.pages.partial.table',['pages' => $footer_pages])

        </div>

        <div id="sitemaplinks" class="tab-pane fade">

            @include('admin.pages.partial.table',['pages' => $sitemap_pages])

        </div>

    </div>

@endsection