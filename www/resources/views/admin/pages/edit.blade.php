@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">{{ $page->name }}</div>

        <div class="panel-body pan">

            {!! Form::model($page, ['method'=>'PATCH', 'action'=>['Admin\PageController@update',$page->id], 'class'=>'form-horizontal']) !!}

            @include('admin.pages.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection