@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">{{ $discount->name }}</div>

        <div class="panel-body pan">

            {!! Form::model($discount, ['method'=>'PATCH', 'action'=>['Admin\DiscountController@update',$discount->id], 'class'=>'form-horizontal']) !!}

            @include('admin.discounts.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection