@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">New discount</div>

        <div class="panel-body pan">

            {!! Form::open(['route'=>'admin.discount.index','class'=>'form-horizontal']) !!}

            @include('admin.discounts.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection