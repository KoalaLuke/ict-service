@extends('admin.layouts.admin')

@section('content')

    <a href="{{ route('admin.order.index') }}" class="btn btn-primary">Back to orders</a>

    <div class="mtl mbl"></div>

    <div class="portlet box portlet-white">
        <div class="portlet-body">
            <div class="mail-content">
                <div class="mail-sender">
                    <div class="row">
                        <div class="col-md-8">
                            <strong>{{ $order->user->first_name }} {{ $order->user->last_name }}</strong>
                            <span>{{ $order->user->email }}</span></div>
                        <div class="col-md-4"><p class="date"> @dateTime($order->created_at)</p></div>
                    </div>
                </div>
                <div class="mail-view">

                    Address:<br />
                    {{ $order->organisation }}<br/>
                    {{ $order->urn }}<br/><br/>
                    {{ $order->address_line_1 }}<br/>
                    {{ $order->address_line_2 }}<br/>
                    {{ $order->address_line_3 }}<br/>
                    {{ $order->city }}<br/>
                    {{ $order->postcode }}<br/><br/>

                    Note:<br />
                    {{ $order->note }}<br /><br />

                    Delivery Address:<br />
                    {{ $order->delivery_contact }}<br/>
                    {{ $order->delivery_address_line_1 }}<br/>
                    {{ $order->delivery_address_line_2 }}<br/>
                    {{ $order->delivery_address_line_3 }}<br/>
                    {{ $order->delivery_city }}<br/>
                    {{ $order->delivery_postcode }}<br/>
                    <br/>
                    Purchase Order: {{ $order->po_number }}<br/><br/>

                    <table width="100%" border="1">
                        <tr>
                            <th>Quantity</th>
                            <th>Product</th>
                            <th>Subtotal</th>
                            <th>Discount</th>
                            <th>Total</th>
                        </tr>
                        @foreach( $order->orderItems  as $item )
                            <tr>
                                <td>{{ $item->qty }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->subtotal }}</td>
                                <td>{{ $item->discount }}</td>
                                <td>{{ $item->total }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2"></td>
                            <td>Sub totals</td>
                            <td>{{ $order->discount }}</td>
                            <td>{{ $order->subtotal }}</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">VAT</td>
                            <td>{{ $order->vat }}</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">Total</td>
                            <td>{{ $order->total }}</td>
                        </tr>
                    </table>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@endsection