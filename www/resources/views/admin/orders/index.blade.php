@extends('admin.layouts.admin')

@section('content')

    <div class="table-container">

        <div class="row mbl">
            <div class="col-sm-8">

                <a href="{{ route('admin.export.show','orders') }}" target="_blank" class="btn btn-primary">Export
                    orders</a>

                <a href="{{ route('admin.export.show','order_items')  }}" target="_blank" class="btn btn-primary">Export
                    orders items</a>
            </div>

            <div class="col-lg-4">
                {!! Form::open(['route'=>'admin.order.search']) !!}
                <div class="input-group input-group-sm mbs">
                    <input type="text" name="order_number" placeholder="Order number" class="form-control">
                    <span class="input-group-btn"><input type="submit" class="btn btn-success" value="Search"/></span>
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif

        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th width="100">Order No.</th>
                <th width="100">Date</th>
                <th>Order Name</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>
                        @dateTime($order->created_at)
                    </td>
                    <td>{{ $order->user->first_name }} {{ $order->user->last_name }}</td>
                    <td><a href="{{ route('admin.order.show',$order->id) }}" type="button"
                           class="btn btn-default btn-xs">View</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="mtl mbl"></div>

@endsection