@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">New banner</div>

        <div class="panel-body pan">

            {!! Form::open(['route'=>'admin.banner.index','class'=>'form-horizontal']) !!}

            @include('admin.banners.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection