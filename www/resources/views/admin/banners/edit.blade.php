@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">{{ $banner->name }}</div>

        <div class="panel-body pan">

            {!! Form::model($banner, ['method'=>'PATCH', 'action'=>['Admin\BannerController@update',$banner->id], 'class'=>'form-horizontal']) !!}

            @include('admin.banners.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection