@extends('admin.layouts.admin')

@section('content')

    <a href="{{ route('admin.banner.create') }}" class="btn btn-primary">Add banner</a>

    <div class="mtl mbl"></div>

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" class="checkall"/>
                </th>
                <th>Banner Name</th>
                <th width="9%">Status</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($banners as $banner)
                <tr>
                    <td>
                        <input type="checkbox" class="checkall"/>
                    </td>
                    <td>{{ $banner->name }}</td>
                    <td>
                        @if ($banner->active == 1)
                            <span class="label label-sm label-success">Active</span>
                        @else
                            <span class="label label-sm label-info">Inactive</span>
                        @endif
                    </td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\BannerController@destroy',$banner->id], 'class'=>'form-inline']) !!}
                        <a href="{{ route('admin.banner.edit',$banner->id) }}" type="button"
                           class="btn btn-default btn-xs">Edit</a>
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection