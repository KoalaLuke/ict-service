@extends('admin.layouts.admin')

@section('content')

    <a href="{{ route('admin.accreditation.create') }}" class="btn btn-primary">Add accreditation</a>

    <div class="mtl mbl"></div>

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" class="checkall"/>
                </th>
                <th>Accreditation Name</th>
                <th width="9%">Status</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($accreditations as $accreditation)
                <tr>
                    <td>
                        <input type="checkbox" class="checkall"/>
                    </td>
                    <td>{{ $accreditation->name }}</td>
                    <td>
                        @if ($accreditation->active == 1)
                            <span class="label label-sm label-success">Active</span>
                        @else
                            <span class="label label-sm label-info">Inactive</span>
                        @endif
                    </td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\AccreditationController@destroy',$accreditation->id], 'class'=>'form-inline']) !!}
                        <a href="{{ route('admin.accreditation.edit',$accreditation->id) }}" type="button"
                           class="btn btn-default btn-xs">Edit</a>
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection