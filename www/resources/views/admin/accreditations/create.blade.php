@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">New accreditation</div>

        <div class="panel-body pan">

            {!! Form::open(['route'=>'admin.accreditation.index','class'=>'form-horizontal']) !!}

            @include('admin.accreditations.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection