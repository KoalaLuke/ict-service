@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">{{ $accreditation->name }}</div>

        <div class="panel-body pan">

            {!! Form::model($accreditation, ['method'=>'PATCH', 'action'=>['Admin\AccreditationController@update',$accreditation->id], 'class'=>'form-horizontal']) !!}

            @include('admin.accreditations.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection