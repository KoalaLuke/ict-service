@extends('admin.layouts.admin')

@section('content')

    <p>Featured products</p>

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th>Product name</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->product->name }}</td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\FeaturedProductController@destroy',$product->id], 'class'=>'form-inline']) !!}
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    {!! Form::open(['action' => ['Admin\FeaturedProductController@store'], 'class'=>'form-inline']) !!}

    {!! Form::select('productid',$all_products, null,['class'=>'form-control']) !!}

    {!! Form::submit('Add', array('class' => 'btn btn-default')) !!}

    {!! Form::close() !!}

@endsection