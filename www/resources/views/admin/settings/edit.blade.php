@extends('admin.layouts.admin')

@section('content')

    {!! Form::model($website, ['class'=>'form-horizontal']) !!}

    <div class="form-body pal">

        <div class="form-group">
            {!! Form::label('name','Name:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('telephone','Telephone:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::text('telephone',null,['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('email','Email:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::text('email',null,['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('twitter','Twitter:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::text('twitter',null,['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('facebook','Facebook:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::text('facebook',null,['class'=>'form-control']) !!}
            </div>
        </div>

        {{--<div class="form-group">--}}
        {{--{!! Form::label('basket_text','Basket text:',['class'=>'col-md-3 control-label']) !!}--}}
        {{--<div class="col-md-9">--}}
        {{--{!! Form::textarea('basket_text',null,['class'=>'form-control editor']) !!}--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="form-group">
            {!! Form::label('contact_text','Contact text:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::textarea('contact_text',null,['class'=>'form-control editor']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('order_text','Order text:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::textarea('order_text',null,['class'=>'form-control editor']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('terms_text','Terms text:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::textarea('terms_text',null,['class'=>'form-control editor']) !!}
            </div>
        </div>

    </div>

    <div class="form-actions">

        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-primary">Send</button>
            &nbsp;
            <button type="button" class="btn btn-green">Cancel</button>
        </div>

    </div>

    {!! Form::close() !!}


@endsection