@extends('admin.layouts.admin')

@section('content')

    <a href="{{ route('admin.export.show','users') }}" target="_blank" class="btn btn-primary">Export users</a>

    <div class="mtl mbl"></div>

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th>Name - email</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>
                        @if ($user->admin == 1)<strong>@endif
                            {{ $user->first_name }} {{ $user->last_name }} - {{ $user->email }}
                        @if ($user->admin == 1)</strong>@endif
                    </td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\UserController@destroy',$user->id], 'class'=>'form-inline']) !!}
                        <a href="{{ route('admin.user.edit',$user->id) }}" type="button"
                           class="btn btn-default btn-xs">Edit</a>
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection