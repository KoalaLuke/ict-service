@extends('admin.layouts.admin')

@section('content')

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" class="checkall"/>
                </th>
                <th width="200">Date</th>
                <th>Email</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($messages as $message)
                <tr>
                    <td>
                        <input type="checkbox" class="checkall"/>
                    </td>
                    <td>@dateTime($message->created_at)</td>
                    <td>{{ $message->email }}</td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\MessageController@destroy',$message->id], 'class'=>'form-inline']) !!}
                        <a href="{{ route('admin.message.show',$message->id) }}" type="button"
                           class="btn btn-default btn-xs">View</a>
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="mtl mbl"></div>

    <a href="{{ route('admin.export.show','messages') }}" target="_blank" class="btn btn-primary">Export messages</a>

@endsection