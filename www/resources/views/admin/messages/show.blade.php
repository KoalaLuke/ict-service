@extends('admin.layouts.admin')

@section('content')

    <a href="{{ route('admin.message.index') }}" class="btn btn-primary">Back to messages</a>

    <div class="mtl mbl"></div>

    <div class="portlet box portlet-white">
        <div class="portlet-body">
            <div class="mail-content">
                <div class="mail-sender">
                    <div class="row">
                        <div class="col-md-8"><strong>{{ $message->first_name }} {{ $message->last_name }}</strong>
                            <span>{{ $message->email }}</span></div>
                        <div class="col-md-4"><p class="date"> @dateTime($message->created_at)</p></div>
                    </div>
                </div>
                <div class="mail-view">
                    {{ $message->message }}
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@endsection