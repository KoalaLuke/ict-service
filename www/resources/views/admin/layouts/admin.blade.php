<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard | Dashboard</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300"/>
    <link rel="stylesheet" href="{{ URL::asset('vendors/font-awesome/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ elixir('css/admin/all.css') }}"/>

</head>

<body>

<div>

    <div class="page-header-topbar">

        <nav id="topbar" role="navigation" class="navbar navbar-default navbar-static-top">

            <div class="navbar-header">
                <a id="logo" href="{{ route('admin.welcome') }}" class="navbar-brand"><span class="fa fa-rocket"></span><span
                            class="logo-text">The ICT Service</span></a>
            </div>

            <div class="topbar-main">
                <ul class="nav navbar navbar-top-links navbar-right mbn">
                    <li class="dropdown topbar-user">
                        <a data-hover="dropdown" href="{{ route('admin.welcome') }}" class="dropdown-toggle">
                            <span class="hidden-xs">{{ $admin->first_name }} {{ $admin->last_name }}</span>
                            &nbsp;
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-user pull-right">
                            <li><a href="{{ route('admin.welcome') }}"><i class="fa fa-user"></i>My Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('auth/logout') }}"><i class="fa fa-key"></i>Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

    </div>

    <div id="wrapper">

        <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
            <div class="sidebar-collapse menu-scroll">
                <ul id="side-menu" class="nav">

                    <li></li>

                    <li class="{{ Route::is('admin.page.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.page.index') }}">
                            <i class="fa fa-file"></i>
                            <span class="menu-title">Pages</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.settings.edit') ? 'active' : '' }}">
                        <a href="{{ route('admin.settings.edit') }}">
                            <i class="fa fa-cogs"></i>
                            <span class="menu-title">Settings</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.image.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.image.index') }}">
                            <i class="fa fa-file-image-o"></i>
                            <span class="menu-title">Images</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.banner.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.banner.index') }}">
                            <i class="fa fa-film"></i>
                            <span class="menu-title">Banners</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.slide.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.slide.index') }}">
                            <i class="fa fa-caret-square-o-left"></i>
                            <span class="menu-title">Slides</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.email.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.email.index') }}">
                            <i class="fa fa-envelope-o"></i>
                            <span class="menu-title">Emails</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.category.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.category.index') }}">
                            <i class="fa fa-arrow-circle-down"></i>
                            <span class="menu-title">Installations</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.featured-product.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.featured-product.index') }}">
                            <i class="fa fa-home"></i>
                            <span class="menu-title">Homepage products</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.discount.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.discount.index') }}">
                            <i class="fa fa-gbp"></i>
                            <span class="menu-title">Discounts</span>
                        </a>
                    </li>

                    {{--<li class="{{ Route::is('admin.accreditation.index') ? 'active' : '' }}">--}}
                    {{--<a href="{{ route('admin.accreditation.index') }}">--}}
                    {{--<i class="fa fa-tachometer fa-fw"></i>--}}
                    {{--<span class="menu-title">Accreditations</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    <li class="{{ Route::is('admin.message.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.message.index') }}">
                            <i class="fa fa-comments"></i>
                            <span class="menu-title">Messages</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.user.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.user.index') }}">
                            <i class="fa fa-users"></i>
                            <span class="menu-title">Users</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.order.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.order.index') }}">
                            <i class="fa fa-file-text-o"></i>
                            <span class="menu-title">Orders</span>
                        </a>
                    </li>

                    <li class="{{ Route::is('admin.import.index') ? 'active' : '' }}">
                        <a href="{{ route('admin.import.index') }}">
                            <i class="fa fa-cloud-upload"></i>
                            <span class="menu-title">Import</span>
                        </a>
                    </li>

                </ul>
            </div>
        </nav>

        <div id="page-wrapper">

            <div class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{ $meta['title'] }}</div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="page-content">
                @section('content')
                    <p>Some content</p>
                @show
            </div>

        </div>

        <div id="footer">
            <div class="copyright"><?php echo date('Y') ?> &copy; <a target="_blank" href="http://www.koala.co.uk">Koala</a></div>
        </div>

    </div>

</div>

<script type="text/javascript" src="{{ elixir('js/admin_packages.js') }}"></script>
<script type="text/javascript" src="/vendors/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/vendors/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="/js/admin.js"></script>

</body>
</html>