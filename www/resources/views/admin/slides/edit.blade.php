@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">{{ $slide->name }}</div>

        <div class="panel-body pan">

            {!! Form::model($slide, ['method'=>'PATCH', 'action'=>['Admin\SlideController@update',$slide->id], 'class'=>'form-horizontal']) !!}

            @include('admin.slides.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection