@extends('admin.layouts.admin')

@section('content')

    <a href="{{ route('admin.slide.create') }}" class="btn btn-primary">Add slide</a>

    <div class="mtl mbl"></div>

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" class="checkall"/>
                </th>
                <th>Slide Name</th>
                <th width="9%">Status</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($slides as $slide)
                <tr>
                    <td>
                        <input type="checkbox" class="checkall"/>
                    </td>
                    <td>{{ $slide->name }}</td>
                    <td>
                        @if ($slide->active == 1)
                            <span class="label label-sm label-success">Active</span>
                        @else
                            <span class="label label-sm label-info">Inactive</span>
                        @endif
                    </td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\SlideController@destroy',$slide->id], 'class'=>'form-inline']) !!}
                        <a href="{{ route('admin.slide.edit',$slide->id) }}" type="button"
                           class="btn btn-default btn-xs">Edit</a>
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection