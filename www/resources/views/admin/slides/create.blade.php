@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">New slide</div>

        <div class="panel-body pan">

            {!! Form::open(['route'=>'admin.slide.index','class'=>'form-horizontal']) !!}

            @include('admin.slides.partial.form')

            {!! Form::close() !!}

        </div>

    </div>

@endsection