<div class="form-body pal">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('active','Active:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::select('active',[1=>'Active', 0=>'Inactive' ], null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('name','Name:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('header','Header:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('header',null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('text','Text:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::textarea('text',null,['class'=>'form-control editor']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('button','Button:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('button',null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('link','Link:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('link',null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('image','Image:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::select('image_id', $images, null,['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('order','Order:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('order',null,['class'=>'form-control']) !!}
        </div>
    </div>

</div>

<div class="form-actions">

    <div class="col-md-offset-3 col-md-9">
        {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
        &nbsp;
        <a class="btn btn-green" href="{{ route('admin.slide.index') }}">Cancel</a>
    </div>

</div>