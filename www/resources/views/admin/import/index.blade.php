@extends('admin.layouts.admin')

@section('content')

    @if($latestImport)

        <p>The last import took place at @dateTime($latestImport->created_at).</p>
        <p>{{ $latestImport->products_imported }} products were imported<?php if( $latestImport->products_added){ ?>, of
            which {{ $latestImport->products_added }} were new<?php } ?>.</p>
        <p>{{ $latestImport->categories_imported }} categories were
            imported<?php if( $latestImport->categories_added){ ?>, of which {{ $latestImport->categories_added }} were
            new<?php } ?>.</p>

        @if($latestImport->relationships_failed)
            <h4>The following problems occurred</h4>
            <p>{!! nl2br($latestImport->relationships_failed) !!}</p>
        @endif

    @endif

    {!! Form::open(['method'=>'POST', 'route' => ['admin.import.update']]) !!}
    {!! Form::submit('Import', array('class' => 'btn btn-primary')) !!}
    {!! Form::close() !!}

@endsection
