@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">{{ $email->name }}</div>

        <div class="panel-body pan">

            {!! Form::model($email, ['method'=>'PATCH', 'action'=>['Admin\EmailController@update',$email->id], 'class'=>'form-horizontal']) !!}

            <div class="form-body pal">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <div class="form-group">
                    {!! Form::label('subject','Subject:',['class'=>'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::text('subject',null,['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('name','Top:',['class'=>'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::textarea('top',null,['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('link','Middle:',['class'=>'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::textarea('middle',null,['class'=>'form-control editor']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('image','Bottom:',['class'=>'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::textarea('bottom',null,['class'=>'form-control']) !!}
                    </div>
                </div>

            </div>

            <div class="form-actions">

                <div class="col-md-offset-3 col-md-9">
                    {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                    &nbsp;
                    <a class="btn btn-green" href="{{ route('admin.email.index') }}">Cancel</a>
                </div>

            </div>

            {!! Form::close() !!}

        </div>

    </div>

@endsection