@extends('admin.layouts.admin')

@section('content')

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th>Email Name</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($emails as $email)
                <tr>
                    <td>{{ $email->name }}</td>
                    <td>
                        <a href="{{ route('admin.email.edit',$email->id) }}" type="button"
                           class="btn btn-default btn-xs">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection