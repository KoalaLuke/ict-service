@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">New image</div>

        <div class="panel-body pan">

            {!! Form::open(['route'=>'admin.image.index','files' => true,'class'=>'form-horizontal']) !!}

            @include('admin.images.partial.form',['type'=>'add'])

            {!! Form::close() !!}

        </div>

    </div>

@endsection