@extends('admin.layouts.admin')

@section('content')

    <a href="{{ route('admin.image.create') }}" class="btn btn-primary">Add image</a>

    <div class="mtl mbl"></div>

    <div class="table-container">
        <table class="table table-hover table-striped table-bordered table-advanced tablesorter">
            <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" class="checkall"/>
                </th>
                <th width="168">Thumbnail</th>
                <th>Image Name</th>
                <th width="12%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($images as $image)
                <tr>
                    <td>
                        <input type="checkbox" class="checkall"/>
                    </td>
                    <td>
                        <img src="{{ route('front.image',[$image->id,150]) }}" width="150"/>
                    </td>
                    <td>{{ $image->name }}</td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action' => ['Admin\ImageController@destroy',$image->id], 'class'=>'form-inline']) !!}
                        <a href="{{ route('admin.image.edit',$image->id) }}" type="button"
                           class="btn btn-default btn-xs">Edit</a>
                        {!! Form::submit('Delete', array('class' => 'btn btn-default btn-xs')) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection