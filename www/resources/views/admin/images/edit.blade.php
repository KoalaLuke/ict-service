@extends('admin.layouts.admin')

@section('content')

    <div class="panel panel-yellow">

        <div class="panel-heading">{{ $image->name }}</div>

        <div class="panel-body pan">

            {!! Form::model($image, ['method'=>'PATCH', 'action'=>['Admin\ImageController@update',$image->id], 'class'=>'form-horizontal']) !!}

            @include('admin.images.partial.form',['type'=>'edit'])

            {!! Form::close() !!}

        </div>

    </div>

@endsection