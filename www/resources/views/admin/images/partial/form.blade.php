<div class="form-body pal">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('name','Name:',['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
    </div>

    @if($type=='add')
        <div class="form-group">
            {!! Form::label('file','File:',['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::file('file', null,['class'=>'form-control']) !!}
            </div>
        </div>
    @endif

</div>

<div class="form-actions">

    <div class="col-md-offset-3 col-md-9">
        {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
        &nbsp;
        <a class="btn btn-green" href="{{ route('admin.image.index') }}">Cancel</a>
    </div>

</div>