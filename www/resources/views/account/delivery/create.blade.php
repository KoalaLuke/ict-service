@extends('front.layouts.front')

@section('content')

    <div class="main-column right">

        <h1>Add address</h1>

        {!! Form::open(['route'=>'account.deliveries.index']) !!}

        @include('account.delivery.partials.form')

        {!! Form::close() !!}

    </div>

    <div class="side-column left">

        @include('account.partials.accountMenu')

    </div>

    <br class="clear">

@stop