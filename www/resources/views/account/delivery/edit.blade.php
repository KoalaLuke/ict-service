@extends('front.layouts.front')

@section('content')

    <div class="main-column right">

        <h1>Edit address</h1>

        {!! Form::model($deliveryAddress, ['method'=>'PATCH', 'action'=>['Account\DeliveryAddressController@update',$deliveryAddress->id], 'class'=>'form-horizontal']) !!}

        @include('account.delivery.partials.form')

        {!! Form::close() !!}

    </div>

    <div class="side-column left">

        @include('account.partials.accountMenu')

    </div>

    <br class="clear">

@stop