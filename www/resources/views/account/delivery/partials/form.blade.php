@if (count($errors) > 0)
    <div class="alert warning block box">
        <h2>Whoops!</h2>
        <p>There were some problems with your input.</p>
        @foreach ($errors->all() as $error)
            <p><em>{{ $error }}</em></p>
        @endforeach
    </div>
@endif

@if(Session::has('success'))
    <div class="alert success block box">
        <h2>Success!</h2>
        {!! Session::get('success') !!}
    </div>
@endif


<div class="form block box">

    <div class="form-row">
        {!! Form::hidden('input_id') !!}
        {!! Form::hidden('previous', $previous) !!}
        {!! Form::label('contact','Contact') !!}
        {!! Form::text('contact',null,['id'=>'contact', 'placeholder'=>'Contact']) !!}
    </div>

    <div class="form-row">
        {!! Form::label('address_line_1','Address') !!}
        {!! Form::text('address_line_1',null,['id'=>'address_line_1', 'placeholder'=>'Address line 1']) !!}
        {!! Form::text('address_line_2',null,['id'=>'address_line_2', 'placeholder'=>'Address line 2']) !!}
        {!! Form::text('address_line_3',null,['id'=>'address_line_3', 'placeholder'=>'Address line 3']) !!}
        {!! Form::text('city',null,['placeholder'=>'Town/City']) !!}
        {!! Form::text('postcode',null,['placeholder'=>'Post code']) !!}
    </div>


    <div class="form-row">

        {!! Form::submit('Save',['class'=>'button']) !!}
        &nbsp;
        <a class="button" href="{{ route('account.deliveries.index') }}">Cancel</a>


    </div>

</div>