@extends('front.layouts.front')

@section('content')

    <div class="main-column right">

        <h1>Delivery addresses</h1>

        @if(!$user->deliveryAddresses->isEmpty())
            <div class="block addresses basket box">
                <table>
                    <thead>
                    <tr>
                        <th>Address</th>
                        <th class="last">Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->deliveryAddresses as $address)
                        <tr>
                            <td>
                                <p>
                                    @if (!empty($address->address_line_1))
                                        {{ $address->address_line_1 }},
                                    @endif
                                    @if (!empty($address->address_line_2))
                                        {{ $address->address_line_2 }},
                                    @endif
                                    @if (!empty($address->address_line_3))
                                        {{ $address->address_line_3 }},
                                    @endif
                                    @if (!empty($address->city))
                                        {{ $address->city }},
                                    @endif
                                    @if (!empty($address->postcode))
                                        {{ $address->postcode }}
                                    @endif
                                </p>
                            </td>
                            <td class="last">
                                <div class="button">
                                    <a href="{{ route('account.deliveries.edit',$address->id) }}">Edit/View</a>
                                </div>

                                {!! Form::open(['method'=>'DELETE', 'action' => ['Account\DeliveryAddressController@destroy',$address->id], 'class'=>'form-inline']) !!}
                                {!! Form::submit('Delete', array('class' => 'button')) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        @endif


        <div class="button primary">
            <a href="{{ route('account.deliveries.create') }}">Add address</a>
        </div>


    </div>

    <div class="side-column left">

        @include('account.partials.accountMenu')

    </div>

    <br class="clear">

@stop