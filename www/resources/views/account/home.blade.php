@extends('front.layouts.front')

@section('content')

    <div class="main-column right">

        <div class="text-content">

            @include('front.partials.success')

            <h1>Hi {{ $user->first_name }}!</h1>
            <p>Welcome to your account area. Please select an option from the menu opposite.</p>

        </div>

    </div>

    <div class="side-column left">

        @include('account.partials.accountMenu')

    </div>

    <br class="clear">

@stop