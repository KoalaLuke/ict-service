@extends('front.layouts.front')

@section('content')

    <div class="main-column right">

        <div class="text-content order-details">

            @if(Session::has('success'))
                <div class="alert success block box">
                    <h2>Thank you for your order</h2>
                    <p>Your order has been placed successfully, please see below for details...</p>
                </div>
            @endif


            <h1>Order - {{ $order->id }}</h1>

            <div class="left">

                <h4>PO number</h4>
                <p>{{ $order->po_number }}</p>

                <h4>Order date/time</h4>
                <p>@dateTime($order->created_at)</p>

                <h4>Customer name</h4>
                <p>{{ $order->user->first_name }} {{ $order->user->last_name }}</p>


                <h4>Email address</h4>
                <p><a href="mailto:{{ $order->user->email }}">{{ $order->user->email }}</a></p>

            </div>
            <div class="right">


                @if (!empty($order->organisation))

                    <h4>School/Organisation</h4>
                    <p>{{ $order->organisation }}</p>

                @endif
                @if (!empty($order->urn))

                    <h4>School DfE Number</h4>
                    <p>{{ $order->urn }}</p>

                @endif

                <h4>Delivery address</h4>

                        @if($order->same_address=='no')

                <address>
                    <p>
                        @if (!empty($order->delivery_contact))
                            <span>{{ $order->delivery_contact }}</span>
                        @endif
                        @if (!empty($order->delivery_address_line_1))
                            <span>{{ $order->delivery_address_line_1 }}</span>
                        @endif
                        @if (!empty($order->delivery_address_line_2))
                            <span>{{ $order->delivery_address_line_2 }}</span>
                        @endif
                        @if (!empty($order->delivery_address_line_3))
                            <span>{{ $order->delivery_address_line_3 }}</span>
                        @endif
                        @if (!empty($order->delivery_city))
                            <span>{{ $order->delivery_city }}</span>
                        @endif
                        @if (!empty($order->delivery_postcode))
                            <span>{{ $order->delivery_postcode }}</span>
                        @endif
                    </p>
                </address>

                        @else

                <address>
                    <p>
                        @if (!empty($order->address_line_1))
                            <span>{{ $order->address_line_1 }}</span>
                        @endif
                        @if (!empty($order->address_line_2))
                            <span>{{ $order->address_line_2 }}</span>
                        @endif
                        @if (!empty($order->address_line_3))
                            <span>{{ $order->address_line_3 }}</span>
                        @endif
                        @if (!empty($order->city))
                            <span>{{ $order->city }}</span>
                        @endif
                        @if (!empty($order->postcode))
                            <span>{{ $order->postcode }}</span>
                        @endif
                    </p>
                </address>

                        @endif



                @if (!empty($order->note))
                    <h4>Notes...</h4>
                    <p>{{ $order->note }}</p>
                @endif

            </div>
            <br class="clear"/>


            <div class="basket order block box">
                <table>
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Qty</th>
                        <th>Subtotal</th>
                        <th>Discount</th>
                        <th class="last">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $order->orderItems  as $item )
                        <tr>
                            <td class="description">{{ $item->name }}</td>
                            <td class="quantity"><span class="table-label">Quantity:</span> {{ $item->qty }}</td>
                            <td class="quantity"><span class="table-label">Subtotal:</span> £{{ $item->subtotal }}</td>
                            <td class="quantity"><span class="table-label">Discount:</span> £{{ $item->discount }}</td>
                            <td class="quantity last"><span class="table-label">Total:</span> £{{ $item->total }}</td>
                        </tr>
                    @endforeach
                    <tbody>
                </table>

                <div class="totals right">

                    @if( $order->discount > 0 )
                        <div class="value discounts">
                            <strong>Discount (Code: {{ $order->code }})</strong>
                            <span>£{{ $order->discount }}</span>
                        </div>
                    @endif
                    <div class="value subtotal">
                        <strong>Net total (ex VAT):</strong>
                        <span>£{{ $order->subtotal }}</span>
                    </div>
                    <div class="value tax">
                        <strong>VAT</strong>
                        <span>£{{ $order->vat }}</span>
                    </div>
                    <div class="value total">
                        <strong>Total</strong>
                        <span>£{{ $order->total }}</span>
                    </div>

                </div>
                <br class="clear">
            </div>


            <div class="button-group noprint">
                <div class="button primary">
                    <a href="javascript:window.print();">Print order</a>
                </div>
                <div class="button">
                    <a href="{{ route('account.order.index') }}">Back to orders</a>
                </div>
            </div>

        </div>
    </div>

    <div class="side-column left">

        @include('account.partials.accountMenu')

    </div>

    <br class="clear">


@endsection