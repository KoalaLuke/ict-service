@extends('front.layouts.front')

@section('content')

    <div class="main-column right">
        <div class="text-content">
            <h1>Checkout</h1>
            <p>You have <strong>{{ $basket['count'] }}</strong> items in your basket...</p>
        </div>

        {!! Form::open(['route'=>'account.checkout.confirm']) !!}

        @include('front.partials.errors')

        @if (!empty($basket['count']))

            <div class="basket block box">
                <table>
                    <thead>
                    <tr>
                        <th colspan="2">Item</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($basket['items'] as $product)
                        <tr>
                            <td class="image">
                                <a href="{{ route('front.product.show',$product->slug) }}">
                                    <img src="{{ route('front.product.image',[$product->id,50]) }}"/>
                                </a>
                            </td>
                            <td class="description">
                                <h3><a href="{{ route('front.product.show',$product->slug) }}">{{ $product->name }}</a>
                                </h3>
                            </td>
                            <td class="quantity">
                                <span class="table-label">Quantity:</span> {{ $product->qty }}
                            </td>
                            <td class="price">
                                <span class="table-label">Price:</span>
                                @if (!empty($product->discount))
                                    <strike>&pound;{{ number_format($product->subtotal, 2, '.', '') }} </strike>
                                @endif
                                &pound;{{ number_format($product->total, 2, '.', '') }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="totals right">
                    <div class="value subtotal">
                        <strong>Net total (ex VAT):</strong>
                    <span>
                            @if (!empty($basket['discounts']))
                            <strike>&pound;{{ number_format($basket['dummySubtotal'], 2, '.', '') }} </strike>
                            @endif
                            &pound;{{ number_format($basket['subtotal'], 2, '.', '') }}
                    </span>
                    </div>
                    <div class="value tax">
                        <strong>VAT:</strong> <span>&pound;{{ number_format($basket['vat'], 2, '.', '') }}</span>
                    </div>
                    <div class="value total">
                        <strong>Grand total:</strong>
                        <span>&pound;{{ number_format($basket['total'], 2, '.', '') }}</span>
                    </div>
                </div>
                <br class="clear">
            </div>

            <div class="text-content">


                <div class="checkout-details">

                    <div class="left">

                        <h4>Account address:</h4>
                        <address>
                            <p>
                                @if (!empty($user->address_line_1))
                                    <span>{{ $user->address_line_1 }}</span>
                                @endif
                                @if (!empty($user->address_line_2))
                                    <span>{{ $user->address_line_2 }}</span>
                                @endif
                                @if (!empty($user->address_line_3))
                                    <span>{{ $user->address_line_3 }}</span>
                                @endif
                                @if (!empty($user->city))
                                    <span>{{ $user->city }}</span>
                                @endif
                                @if (!empty($user->postcode))
                                    <span>{{ $user->postcode }}</span>
                                @endif
                            </p>
                        </address>

                        <p><input type="checkbox" name="same_address" id="same_address" value="yes"
                                  @if($basket['same_address']!='no') checked="checked" @endif /> Deliver to this address
                        </p>

                        <div class="delivery_address_container">
                            <h4>Delivery address:</h4>
                            <div>
                                @if(!$user->deliveryAddresses->isEmpty())
                                    <select name="delivery_address" id="delivery_address">
                                        <option value="0">Please select an address</option>
                                        @foreach($user->deliveryAddresses as $address)
                                            <option value="{{ $address->id }}"
                                                    @if($basket['delivery_address']==$address->id) selected="selected" @endif >{{ $address->address_line_1 }}</option>
                                        @endforeach
                                    </select>
                                @endif

                                <div class="button">
                                    <a href="{{ route('account.deliveries.create') }}">Add new address</a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="right">

                        <h4>Purchase Order Number<span class="required">*</span>:</h4>
                        <div>
                            <input type="text" name="po_number" id="po_number" value="{{ $basket['po_number'] }}"/>
                        </div>

                        <h4>Agree terms?<span class="required">*</span></h4>
                        <p><input name="terms_check" type="hidden" value="no"/><input type="checkbox" name="terms_check"
                                                                                      id="terms_check" value="yes"
                                                                                      @if($basket['terms_check']=='yes') checked="checked" @endif />
                            I have read and agree the <a class="fancypants fancybox.ajax"
                                                         href="{{ route('front.terms') }}">terms and conditions</a></p>


                    </div>
                    <br class="clear">
                    <div class="left">
                        <p><span class="required">*</span> Indicates required field</p>
                    </div>
                    <div class="right">
                        <div class="nextstep">
                            <input type="submit" id="submit" class="button primary" value="Next step"/>
                        </div>
                    </div>
                    <br class="clear">
                </div>





                <div class="text-content">
                    @include('front.partials.cards_accepted')
                </div>
            </div>

        @endif

        {!! Form::close() !!}

    </div>

    <div class="side-column left">
        @include('front.partials.twitter')
    </div>

    <br class="clear">

@stop