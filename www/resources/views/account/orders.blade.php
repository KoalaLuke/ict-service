@extends('front.layouts.front')

@section('content')

    <div class="main-column right">

        <div class="text-content">

            <h1>Order history</h1>


            <div class="basket order block box">
                <table>
                    <thead>
                    <tr>
                        <th>Date/time</th>
                        <th>Order ID</th>
                        <th>Net Total</th>
                        <th class="last">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($orders as $order)
                        <tr>
                            <td><span class="table-label">Date/time:</span> @dateTime($order->created_at)</td>
                            <td><span class="table-label">Order ID:</span> {{ $order->id }}</td>
                            <td><span class="table-label">Net Total:</span> £{{ $order->subtotal }}</td>
                            <td class="last">
                                <div class="button">
                                    <a href="{{ route('account.order.show',$order->id) }}">View details</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    <tbody>
                </table>
            </div>
        </div>

    </div>

    <div class="side-column left">

        @include('account.partials.accountMenu')

    </div>
    <br class="clear">

@endsection
