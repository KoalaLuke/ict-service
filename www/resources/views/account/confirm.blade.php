@extends('front.layouts.front')

@section('content')

    <div class="main-column right">
        <div class="text-content">
            <h1>Confirm the details of your order</h1>
        </div>

        @if (!empty($basket['count']))

            <div class="basket block box">


                <table>

                    <thead>
                    <tr>
                        <th colspan="2">Item</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                    </thead>

                    <tbody>

                    @foreach ($basket['items'] as $product)
                        <tr>

                            <td class="image">
                                <a href="{{ route('front.product.show',$product->slug) }}">
                                    <img src="{{ route('front.product.image',[$product->id,50]) }}"/>
                                </a>

                            </td>

                            <td class="description">
                                <h3><a href="{{ route('front.product.show',$product->slug) }}">{{ $product->name }}</a>
                                </h3>
                            </td>

                            <td class="quantity">
                                <span class="table-label">Quantity:</span> {{ $product->qty }}
                            </td>

                            <td class="price">
                                <span class="table-label">Price:</span>
                                @if (!empty($product->discount))
                                    <strike>&pound;{{ number_format($product->subtotal, 2, '.', '') }} </strike>@endif
                                    &pound;{{ number_format($product->total, 2, '.', '') }}
                            </td>

                        </tr>
                    @endforeach

                    </tbody>

                </table>

                <div class="totals right">

                    <div class="value subtotal">
                        <strong>Net total (ex VAT):</strong>
                        <span>
                            @if (!empty($basket['discounts']))
                                <strike>&pound;{{ number_format($basket['dummySubtotal'], 2, '.', '') }} </strike> @endif
                                &pound;{{ number_format($basket['subtotal'], 2, '.', '') }}
                        </span>
                    </div>


                    <div class="value tax">
                        <strong>VAT:</strong> <span>&pound;{{ number_format($basket['vat'], 2, '.', '') }}</span>
                    </div>

                    <div class="value total">
                        <strong>Grand total:</strong>
                        <span>&pound;{{ number_format($basket['total'], 2, '.', '') }}</span>
                    </div>


                </div>

                <br class="clear">

            </div>


            <div class="text-content">
                <div class="checkout-details">

                    <div class="left">

                        @if (!empty($basket['po_number']))
                            <h4>Purchase Order number:</h3>
                                <p>{{ $basket['po_number'] }}</p>
                                @endif

                                <h4>Account address:</h4>

                                <address>
                                    <p>
                                        @if (!empty($user->address_line_1))
                                            <span>{{ $user->address_line_1 }}</span>
                                        @endif
                                        @if (!empty($user->address_line_2))
                                            <span>{{ $user->address_line_2 }}</span>
                                        @endif
                                        @if (!empty($user->address_line_3))
                                            <span>{{ $user->address_line_3 }}</span>
                                        @endif
                                        @if (!empty($user->city))
                                            <span>{{ $user->city }}</span>
                                        @endif
                                        @if (!empty($user->postcode))
                                            <span>{{ $user->postcode }}</span>
                                        @endif
                                    </p>
                                </address>


                    </div>
                    <div class="right">

                        <h4>Delivery address:</h4>

                        @if($basket['same_address']=='no')

                            <address>
                                <p>
                                    @if (!empty($basket['deliveryAddress']->address_line_1))
                                        <span>{{ $basket['deliveryAddress']->address_line_1 }}</span>
                                    @endif
                                    @if (!empty($basket['deliveryAddress']->address_line_2))
                                        <span>{{ $basket['deliveryAddress']->address_line_2 }}</span>
                                    @endif
                                    @if (!empty($basket['deliveryAddress']->address_line_3))
                                        <span>{{ $basket['deliveryAddress']->address_line_3 }}</span>
                                    @endif
                                    @if (!empty($basket['deliveryAddress']->city))
                                        <span>{{ $basket['deliveryAddress']->city }}</span>
                                    @endif
                                    @if (!empty($basket['deliveryAddress']->postcode))
                                        <span>{{ $basket['deliveryAddress']->postcode }}</span>
                                    @endif
                                </p>
                            </address>

                        @else

                            <address>
                                <p>
                                    @if (!empty($user->address_line_1))
                                        <span>{{ $user->address_line_1 }}</span>
                                    @endif
                                    @if (!empty($user->address_line_2))
                                        <span>{{ $user->address_line_2 }}</span>
                                    @endif
                                    @if (!empty($user->address_line_3))
                                        <span>{{ $user->address_line_3 }}</span>
                                    @endif
                                    @if (!empty($user->city))
                                        <span>{{ $user->city }}</span>
                                    @endif
                                    @if (!empty($user->postcode))
                                        <span>{{ $user->postcode }}</span>
                                    @endif
                                </p>
                            </address>



                        @endif


                    </div>
                    <br class="clear">

                    <div class="nextstep">
                        <div class="button">
                            <a href="{{ route('account.checkout') }}">Edit order</a>
                        </div>

                        <div class="button basket primary">
                            <a href="{{ route('account.process') }}"><i></i> Place order</a>
                        </div>

                    </div>


                </div>


                @include('front.partials.cards_accepted')

            </div>


        @endif


    </div>

    <div class="side-column left">
        @include('front.partials.twitter')
    </div>

    <br class="clear">

@stop