<div class="block box">
    <h3 class="header-bar">Your account</h3>
    <ul>
        <li><a href="{{ route('account.update') }}">Account Details</a></li>
        <li><a href="{{ route('account.deliveries.index') }}">Delivery Addresses</a></li>
        <li><a href="{{ route('account.order.index') }}">Order History</a></li>
        <li><a href="{{ route('auth.getLogout') }}">Logout</a></li>
    </ul>
</div>