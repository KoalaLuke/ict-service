@extends('front.layouts.front')

@section('content')

    <div class="main-column right">

        <div class="text-content">

            <h1>Account details</h1>

            @include('front.partials.errors')

            @include('front.partials.success')

            <div class="form block box">

                {!! Form::model($user, ['method'=>'POST', 'route'=>['account.update'], 'class'=>'form-horizontal']) !!}

                @include('front.partials.user_details_form', ['type'=>'edit'])

                {!! Form::close() !!}

            </div>

        </div>

    </div>

    <div class="side-column left">

        @include('account.partials.accountMenu')

    </div>

    <br class="clear">

@endsection
