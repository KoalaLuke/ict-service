<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $website->name }}</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ elixir('css/front/all.css') }}"/>

    <link rel="stylesheet" href="/css/front/updates/jquery.fancybox.css" type="text/css">
    <link rel="stylesheet" href="/css/front/updates/jqueries.css" type="text/css">
    <link rel="stylesheet" href="/css/front/updates/style.css" type="text/css">
    <link rel="stylesheet" href="/css/front/updates/print.css" type="text/css" media="print"/>

</head>
<body>
<div class="outer-wrapper">
    <header>
        <div class="header">
            <div class="content-container">
                <div class="hamburger">
                    <a href="#">Menu</a>

                    <div id="hamburger_account_header" class="hamburger-headers">
                        <h2>Your account</h2>
                    </div>

                    <div id="hamburger_categories_header" class="hamburger-headers">
                        <h2>Categories</h2>
                    </div>

                    <div id="hamburger_pages_header" class="hamburger-headers">
                        <h2>More information</h2>
                    </div>

                </div>
                <div class="logo left">
                    <h1><a href="/">{{ $website->name }}: Procurement</a></h1>
                </div>

                @include('front.partials.search')

                <br class="clear"/>
            </div>
        </div>

        @include('front.partials.navigation')

        <div class="toplinks">
            <div class="content-container">
                <div class="main-website left">
                    <ul class="horizontal">
                        <li><a href="http://www.theictservice.org.uk" target="_blank"><i></i> Return to <span>The ICT Service</span>
                                main website</a></li>
                    </ul>
                </div>
                <div class="pages-nav right">

                    @include('front.partials.header_links')

                </div>
                <br class="clear"/>
            </div>
        </div>
        <div class="account-panel">
            <div class="content-container">
                <div class="contact-details left">
                    <p><strong>Need help?</strong> Call <a
                                href="tel:{{ str_replace(' ','',$website->telephone) }}">{{ $website->telephone }}</a>
                        or email <a href="mailto:{{ $website->email }}">{{ $website->email }}</a></p>
                </div>
                <div class="account-links right">
                    <ul class="horizontal">

                        @if (Auth::guest())
                            <li class="login"><a href="{{ route('auth.getLogin') }}"><i></i> Sign in</a></li>
                            <li class="register"><a href="{{ route('auth.getRegister') }}"><i></i> Create account</a>
                            </li>
                        @else
                            <li class="account"><a href="{{ route('account.home') }}"><i></i> Account</a></li>
                            <li class="logout"><a href="{{ route('auth.getLogout') }}"><i></i> Logout</a></li>
                        @endif
                        <li class="basket"><a href="{{ route('front.basket.show') }}"><i></i> Your basket <span
                                        id="basket_count" class="counter">{{ $basket_count }}</span></a></li>

                    </ul>
                </div>
                <br class="clear"/>
            </div>
        </div>

        @include('front.partials.breadcrumb')

    </header>
    <div class="wrapper">
        <div class="content-container">

            @section('content')
                <p>Some content</p>
            @show

            <br class="clear"/>
        </div>
    </div>
    <footer>

        @include('front.partials.accreditations')

        <div class="footer">
            <div class="content-container">

                @include('front.partials.footer_links')

                <div class="website-credits right">
                    <ul class="horizontal">
                        <li><a href="http://www.koala.co.uk" target="_blank">Website by Koala</a></li>
                    </ul>
                </div>
                <br class="clear"/>
            </div>
        </div>
    </footer>
</div>
@include('front.partials.footer_javascript')
</body>
</html>
