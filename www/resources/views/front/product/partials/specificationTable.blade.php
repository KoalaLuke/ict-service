@foreach ($features as $feature)

    <?php
    $rows = '';
    $count = count($products);
    ?>

    @foreach($feature->children as $child)

        <?php
        $specifications = '';
        $atLeastOneSpecification = false;
        ?>

        @foreach ($products as $product)

            <?php $product_spec = ''; ?>

            @foreach ($product->specifications as $specification)

                @if($specification->feature_id==$child->id)

                    <?php

                    $product_spec = $specification->icts_displaytext;
                    $atLeastOneSpecification = true;

                    ?>

                @endif

            @endforeach

            <?php $specifications .= '<td>' . $product_spec . '</td>'; ?>

        @endforeach

        <?php

        if ($atLeastOneSpecification) {

            $icon = empty($child->icts_description) ? '' : '<i class="info tooltip tooltipstered" title="' . $child->icts_description . '"></i>';
            $rows .= '<tr>';
            $rows .= '<th>' . $child->icts_name . ' ' . $icon . '</th>';
            $rows .= $specifications;
            $rows .= '</tr>';
        }

        ?>

    @endforeach

    @if(!empty($rows))

        <tr>
            <th class="featurecategory" colspan="{{ $count+1 }}">{{ $feature->icts_name }}</th>
        </tr>

        {!! $rows !!}

    @endif

@endforeach
