<div class="product box {{$product->slug}}">

    <div class="selector">

        {!! Form::open(['method'=>'POST', 'route' => ['front.comparison.add'], 'class'=>'form-inline add_to_comparison']) !!}
        {!! Form::hidden('id', $product->id) !!}
        {!! Form::checkbox('compare', $product->id, $product->isInComparison(), ['id'=>'compare'.$product->id,'class' => 'compareCheck']) !!}
        {!! Form::label('compare'.$product->id,'Compare product') !!}
        {!! Form::close() !!}

    </div>


    <div class="thumbnail" style="background-image: url('{{ route('front.product.image',[$product->id,300]) }}');">
        <a href="{{ route('front.product.show',$product->slug) }}"></a>
    </div>

    <div class="details">

        <div class="product-info">

            <div class="primary-details">

                <h3><a href="{{ route('front.product.show',$product->slug) }}">{{ $product->name }}</a></h3>

                <div class="price">

                    @if (!empty($product->icts_rrp)) <h5>RRP <strike>&pound;{{ $product->icts_rrp }}</strike>
                    </h5> @endif

                    <h4>&pound;{{ $product->price }}</h4>

                </div>

            </div>

            <div class="summary-info">

                <p>{{ $product->description }}</p>

            </div>

        </div>

        <div class="buttons">

            <div class="button info">

                <a href="{{ route('front.product.show',$product->slug) }}"><i></i> View <span>details</span></a>

            </div>

            {!! Form::open(['method'=>'POST', 'route' => ['front.basket.add'], 'class'=>'form-inline add_to_basket']) !!}
            {!! Form::hidden('id', $product->id) !!}
            {!! Form::hidden('qty', 1) !!}

            <div class="qty">
                <a class="icon qtyControl subtract" href="">Subtract</a>
                <input type="number" name="qty" value="1">
                <a class="icon qtyControl add" href="">Add</a>
            </div>

            {!! Form::button('<i></i> Add', array('class' => 'button basket primary', 'type'=>'submit')) !!}
            {!! Form::close() !!}

            <br class="clear"/>

        </div>

    </div>

</div>