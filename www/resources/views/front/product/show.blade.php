@extends('front.layouts.front')

@section('content')

    @include('front.partials.banner')

    <div class="main-column right">

        <div class="infoblock">

            <div class="productimage left">

                @if (!empty($product->edict_imageurl))
                    <a class="fancypants" href="{{ $product->edict_imageurl }}">
                        <div class="image zoom box">
                            <img src="{{ route('front.product.image',[$product->id,1000]) }}" alt="{{ $product->name }}"/>
                        </div>
                    </a>
                @endif

                @if (!empty($product->icts_additionalimageurls))
                    <div class="thumbnails">

                        <div class="thumbnail box">
                            <a class="fancypants" rel="gallery1" href="{{ $product->edict_imageurl }}"><img src="{{ $product->edict_imageurl }}" alt="{{ $product->name }}"/></a>
                        </div>

                        @foreach ($product->icts_additionalimageurls as $image_url)
                            <div class="thumbnail box">
                                <a class="fancypants" rel="gallery1" href="{{ $image_url }}"><img src="{{ $image_url }}" alt="{{ $product->name }}"/></a>
                            </div>
                        @endforeach

                        <br class="clear"/>
                    </div>
                @endif

                @if (!empty($product->icts_usermanualurl))
                    <div class="button download">
                        <a href="{{ $product->icts_usermanualurl }}" target="_blank"><i></i> Download user manual</a>
                    </div>
                @endif

            </div>

            <div class="productdetails text-content right">

                @if (!empty($product->name))
                    <h1>{{ $product->name }}</h1>
                @endif

                @if (!empty($product->vendorname))
                    <h3 class="vendor">{{ $product->vendorname }}</h3>
                @endif

                @if (!empty($product->description))
                    <h3>Product features</h3>
                    <div class="description">
                        {!! nl2br($product->description) !!}
                    </div>
                @endif

                @if (!empty($product->icts_rrp))
                    <h4 class="rrp"><strike>&pound;{{ $product->icts_rrp }}</strike></h4>
                @endif

                @if (!empty($product->price))
                    <h2 class="price">Price: &pound;{{ $product->price }} <span>(ex VAT)</span></h2>
                @endif

            </div>

            <br class="clear"/>

            {!! Form::open(['method'=>'POST', 'route' => ['front.basket.add'], 'class'=>'form-inline add_to_basket']) !!}
            {!! Form::hidden('id', $product->id) !!}
            <div class="selectedsummary">
                <div class="box right">
                    <div class="button basket primary">
                        {!! Form::button('<i></i> Add to basket', array('class' => 'button basket primary', 'type'=>'submit')) !!}
                    </div>
                </div>
                <div class="summaryinfo qty box right">
                    <h3>Quantity selected:</h3>

                    <div class="qty">
                        <a class="icon qtyControl subtract" href="">Subtract</a>
                        <input type="number" name="qty" value="1">
                        <a class="icon qtyControl add" href="">Add</a>
                    </div>

                </div>


                <br class="clear">
            </div>
            {!! Form::close() !!}

            <br class="clear"/>

        </div>


        @foreach($product->categories as $category)

            @if( !$category->installations->isEmpty())

                <div class="infoblock">

                    <div class="text-content">

                        <h2>Installation options</h2>

                        <p>Cambridgeshire Schools can choose a level of installation based on school needs, ensuring devices are setup ready to use.  If you are not buying for a school in Cambridgeshire, get in touch for a bespoke installation package quote for your devices.</p>

                    </div>

                    <div class="products installations grid">

                        @foreach ($category->installations as $installation)

                            @include('front.product.partials.box', ['product' => $installation->product])

                        @endforeach

                        <br class="clear"/>

                    </div>

                </div>

            @endif

        @endforeach





        @if(!$product->specifications->isEmpty())

            <div class="infoblock">

                <div class="text-content">
                    <h2>Detailed specifications</h2>
                </div>

                <div class="product-specifications box block">

                    <table>

                        <thead>

                        @include('front.product.partials.specificationTable',['products' => [$product] ])

                        </thead>

                    </table>

                </div>

            </div>


        @endif

        @if (!$product->alternativeProducts->isEmpty())

            <div class="infoblock">

                <div class="text-content">

                    <h2>Alternative Products</h2>

                    <p>Customers who looked at this product also looked at&hellip;</p>

                </div>

                <div class="products alternative grid">

                    @foreach ($product->alternativeProducts as $alternativeProduct)
                        @include('front.product.partials.box', ['product' => $alternativeProduct])
                    @endforeach

                    <br class="clear"/>

                </div>

            </div>

        @endif

        @if (!$product->associatedProducts->isEmpty())

            <div class="infoblock">

                <div class="text-content">

                    <h2>Associated Products</h2>

                    <p>Customers who purchased this product also purchased&hellip;</p>

                </div>

                <div class="products associated grid">

                    @foreach ($product->associatedProducts as $associatedProduct)
                        @include('front.product.partials.box', ['product' => $associatedProduct])
                    @endforeach

                    <br class="clear"/>

                </div>

            </div>

        @endif


    </div>

    <div class="side-column left">
        @include('front.partials.twitter')
    </div>

@stop


