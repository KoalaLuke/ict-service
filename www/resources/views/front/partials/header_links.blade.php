<ul class="horizontal">
    @foreach($headerLinks as $page)
        <li><a href="{{ route('front.page.show',$page->slug) }}">{{ $page->name }}</a></li>
    @endforeach
    <li><a href="{{ route('front.contact.show') }}">Contact</a></li>
</ul>