<div class="promo slideshow">
    <div class="slides block box">

        @foreach($slides as $slide)
            <div class="slide" style="background-image: url('{{ route('front.image',[$slide->image->id,680,350]) }}');">
                <div class="overlay">
                    <div class="overlay-content">
                        <h2>{{ $slide->name }}</h2>
                        <h3>{{ $slide->header }}</h3>
                        <p>{!! $slide->text !!}</p>
                        <div class="button">
                            <a href="{{ $slide->link }}">{{ $slide->button }}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    <div class="controls">
        <ul class="horizontal">
            <li class="prev"><a href="#">Previous</a>
            <li class="next"><a href="#">Next</a>
        </ul>
    </div>
</div>