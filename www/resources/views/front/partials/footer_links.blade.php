<div class="footer-links left">
    <ul class="horizontal">
        <li><a href="http://www.theictservice.org.uk" target="_blank">&copy; The ICT Service {{ date('Y') }}, all rights
                reserved</a></li>
        @foreach($footerLinks as $page)
            <li><a href="{{ route('front.page.show',$page->slug) }}">{{ $page->name }}</a></li>
        @endforeach
    </ul>
</div>