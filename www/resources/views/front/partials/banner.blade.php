<div class="banner slideshow">
    <div class="slides block box">
        @foreach($banners as $banner)
            <div class="slide">
                <a href="{{ $banner->link }}"><img src="{{ route('front.image',[$banner->image->id,980,70]) }}"
                                                   alt="{{ $banner->image->name }}" border="0" width="980" height="70"/></a>
            </div>
        @endforeach
    </div>
    <div class="controls">
        <ul class="horizontal">
            <li class="prev"><a href="#">Previous</a>
            <li class="next"><a href="#">Next</a>
        </ul>
    </div>
</div>