<div class="search right">

    {!! Form::open(['method'=>'GET', 'route' => ['front.search'], 'id'=>'search']) !!}

    {!! Form::text('term',null,['id'=>'keywords','placeholder'=>'Search all products']) !!}

    {!! Form::submit('Search') !!}

    {!! Form::close() !!}

</div>