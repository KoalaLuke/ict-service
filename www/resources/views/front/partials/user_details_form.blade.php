<h3>School/Organisation...</h3>

<div class="form-row">
    {!! Form::label('organisation','School/Organisation') !!}
    {!! Form::text('organisation',null,['id'=>'organisation', 'placeholder'=>'Organisation']) !!}
</div>

<div class="form-row">
    {!! Form::label('address_line_1','Address') !!}
    {!! Form::text('address_line_1',null,['id'=>'address_line_1', 'placeholder'=>'Address line 1']) !!}
    {!! Form::text('address_line_2',null,['id'=>'address_line_2', 'placeholder'=>'Address line 2']) !!}
    {!! Form::text('address_line_3',null,['id'=>'address_line_3', 'placeholder'=>'Address line 3']) !!}
    {!! Form::text('city',null,['placeholder'=>'Town/City']) !!}
    {!! Form::text('postcode',null,['placeholder'=>'Post code']) !!}
</div>

<div class="form-row">

    {!! Form::label('urn','School DfE Number') !!}
    {!! Form::text('urn',null,['id'=>'urn', 'placeholder'=>'School DfE Number']) !!}

</div>

<h3>Contact person...</h3>

<div class="form-row">
    {!! Form::label('title','Title') !!}
    {!! Form::select('title',['Mr'=>'Mr.','Mrs'=>'Mrs.','Miss'=>'Miss.','Ms'=>'Ms.','Dr'=>'Dr.','Prof'=>'Prof.','Rev'=>'Rev.'], null) !!}
</div>

<div class="form-row">
    {!! Form::label('first_name','First name') !!}
    {!! Form::text('first_name',null,['id'=>'first_name', 'placeholder'=>'First name']) !!}
</div>

<div class="form-row">
    {!! Form::label('last_name','Last name') !!}
    {!! Form::text('last_name',null,['id'=>'last_name', 'placeholder'=>'Last name']) !!}
</div>

<div class="form-row">
    {!! Form::label('email','Email') !!}
    {!! Form::text('email',null,['id'=>'email', 'placeholder'=>'Email']) !!}
</div>

<div class="form-row">
    {!! Form::label('telephone','Telephone') !!}
    {!! Form::text('telephone',null,['id'=>'telephone', 'placeholder'=>'Telephone']) !!}
</div>

@if($type=='add')

    <h3>Login details...</h3>

    <div class="form-row">
        {!! Form::label('password','Password') !!}
        {!! Form::password('password',null,['id'=>'password', 'placeholder'=>'Password']) !!}
    </div>

    <div class="form-row">
        {!! Form::label('password_confirmation','Confirm Password') !!}
        {!! Form::password('password_confirmation',null,['id'=>'password_confirmation', 'placeholder'=>'Confirm password']) !!}
    </div>

    <div class="form-row">
        {!! Form::submit('Register',['class'=>'button']) !!}
    </div>

@else

    <div class="form-row">
        {!! Form::submit('Save',['class'=>'button']) !!}
    </div>

@endif