<div class="navigation">
    <div class="content-container">
        <ul class="horizontal">
            @foreach($navigation as $category)
                @if($category->statecode==0)
                <li>
                    <a href="{{ route('front.category.show',$category->slug) }}">{{ $category->icts_name }}</a>
                    @if (!empty($category->children))
                        <ul>
                            @foreach($category->children as $child)
                                @if($child->statecode==0)
                                <li>
                                    <a href="{{ route('front.category.show',$child->slug) }}">{{ $child->icts_name }}</a>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>