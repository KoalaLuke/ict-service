<div class="compare box">
    <div id="comparison_count_btn" class="button counter{{ $comparison_count }}">
        <a href="{{ route('front.comparison.show') }}">Compare <span id="comparison_count"
                                                                     class="counter">{{ $comparison_count }}</span></a>
    </div>
</div>