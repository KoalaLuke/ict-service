<div class="scrolling-logos">
    <ul>
        <li class="council"><a href="http://www.cambridgeshire.gov.uk">Cambridgeshire County Council</a></li>
        <li class="apple"><a href="http://www.apple.com/uk/education/rtc">Apple: Regional Training Centre</a></li>
        <li class="naace"><a href="http://www.naace.co.uk">Naace</a></li>
        <li class="e2bn"><a href="http://www.e2bn.org">E2BN</a></li>
        <li class="sims"><a href="http://www.capita-sims.co.uk">Sims Accredited Support Team</a></li>
        {{--@foreach($accreditations as $accreditation)--}}
        {{--<li style="background-image: url('{{ $accreditation->image->file }}');"><a href="{{ $accreditation->link }}">{{ $accreditation->name  }}</a></li>--}}
        {{--@endforeach--}}
    </ul>
</div>