@if(Session::has('success'))
    <div class="alert success block box">
        <h2>Success!</h2>
        {!! Session::get('success') !!}
    </div>
@endif