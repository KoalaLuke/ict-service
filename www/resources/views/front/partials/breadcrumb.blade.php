<div class="breadcrumb">
    <div class="content-container">
        <ul class="horizontal">
            <li>You are here</li>
            <li class="home"><a href="{{ route('front.home.show') }}">Home</a></li>
            @if(isset($breadcrumbs))
                @foreach($breadcrumbs as $breadcrumb)
                    <li>
                        @if($breadcrumb->link)<a href="{{ $breadcrumb->link }}"> @endif
                            {{ $breadcrumb->name }}
                            @if($breadcrumb->link)</a> @endif
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>