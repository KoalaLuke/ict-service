@if (count($errors) > 0)
    <div class="alert warning block box">
        <h2>Whoops!</h2>
        <p>There were some problems with your input.</p>
        @foreach ($errors->all() as $error)
            <p><em>{{ $error }}</em></p>
        @endforeach
    </div>
@endif