<div class="twitter">
    <div class="tweet block">
        <h3>Latest tweets</h3>
        @if(!empty($tweets))
            @foreach($tweets as $tweet)
                <p>{!! Twitter::linkify($tweet->text) !!}</p>
                <p class="ago" title="@dateTime($tweet->created_at)">@dateTime($tweet->created_at)</p>
            @endforeach
        @else
            <p>We are having a problem with our Twitter Feed right now.</p>
        @endif
    </div>
    <div class="social-media">
        <h4>Keep up to date</h4>
        <ul class="horizontal">
            <li class="facebook"><a href="{{ $facebook }}" target="_blank">Facebook</a></li>
            <li class="twitter">{!! Twitter::linkify('@'.$tweet->user->screen_name) !!}</li>
        </ul>
    </div>
</div>