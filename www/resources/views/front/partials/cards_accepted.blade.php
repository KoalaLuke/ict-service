<div class="creditcards">
    <ul class="horizontal">
        <li class="amex">Amex</li>
        <li class="diners">Diners</li>
        <li class="discover">Discover</li>
        <li class="jcb">JCB</li>
        <li class="mastercard">Mastercard</li>
        <li class="paypal">PayPal</li>
        <li class="stripe">Stripe</li>
        <li class="visa">Visa</li>
    </ul>
</div>