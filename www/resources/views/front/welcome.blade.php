@extends('front.layouts.front')

@section('content')

    @include('front.partials.slideShow')

    <div class="main-column right">

        <div class="text-content">
            <h1>Featured products</h1>
        </div>

        <div class="products featured grid">

            @foreach ($products as $product)

                @if($product->product)

                    @include('front.product.partials.box', ['product' => $product->product])

                @endif

            @endforeach

            <br class="clear"/>

        </div>

    </div>

    <div class="side-column left">
        @include('front.partials.twitter')
    </div>


@endsection