@if(!$category->children->isEmpty())
    <div class="categories block box">
        <h3 class="header-bar">{{ $category->icts_name }}</h3>
        <ul>
            @foreach( $category->children as $child)
                <li><a href="{{ route('front.category.show', $child->slug ) }}">{{ $child->icts_name }}</a></li>
            @endforeach
        </ul>
    </div>
@elseif(!$category->parent->children->isEmpty())
    <div class="categories block box">
        <h3 class="header-bar">{{ $category->parent->icts_name }}</h3>
        <ul>
            @foreach( $category->parent->children as $child)
                <li><a href="{{ route('front.category.show', $child->slug ) }}">{{ $child->icts_name }}</a></li>
            @endforeach
        </ul>
    </div>
@endif
