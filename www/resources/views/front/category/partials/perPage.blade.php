<div class="more-options right">
    <div class="items-per-page box">

        <form id='items-per-page'>
            <h3>Items per page</h3>
            {!! Form::select('quantity', $paginationChoices, $perPage, ['class'=>'form-control','id'=>'items-quantity']) !!}
        </form>

    </div>
</div>