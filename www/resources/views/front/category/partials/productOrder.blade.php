<div class="sort box">
    <form name="sort" id="sort">
        <h3>Sort by</h3>
        {!! Form::select('sort', $orderingChoices, $productOrder, ['class'=>'form-control','id'=>'sort-by']) !!}
    </form>
</div>