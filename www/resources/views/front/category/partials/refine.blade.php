<div class="filter block box">
    <h3 class="header-bar primary">Refine by...</h3>
    <form id="filter" name="filter">
        <h4 class="sub-header-bar">Brand</h4>
        <ul>
            @foreach($vendors as $value => $label)
                <li>
                    <input type="checkbox" id="vendor-{{ $value }}" name="vendors" value="{{ $value }}">
                    <label for="vendor-{{ $value }}">{{ $label }}</label>
                </li>
            @endforeach
        </ul>
    </form>
</div>