@extends('front.layouts.front')

@section('content')

    @include('front.partials.banner')

    <div class="main-column right">

        <div class="intro text-content">

            <h1>{{ $category->icts_name }}</h1>

        </div>

        <div class="results-header">
            <div class="results-count left">
                <?php $baseNoOfProducts = $products->perPage() * ($products->currentPage() - 1); ?>
                <p><strong>{{ $baseNoOfProducts + 1 }} - {{  $baseNoOfProducts + $products->count() }}</strong> of
                    <strong>{{ $products->total() }}</strong></p>
            </div>
            <div class="view-tools right">

                @include('front.partials.compare')

                <div class="view-type box">
                    <ul class="horizontal">
                        <li class="list"><a href="#">List</a></li>
                        <li class="grid active"><a href="#">Grid</a></li>
                    </ul>
                </div>

                @include('front.category.partials.productOrder')

            </div>
            <br class="clear"/>
        </div>

        <div class="products grid">

            @foreach ($products as $product)
                @include('front.product.partials.box', ['product' => $product])
            @endforeach

            <br class="clear"/>

        </div>

        <div class="results-footer">

            <div class="paging left">
                {!! $products->render() !!}
            </div>

            @include('front.category.partials.perPage')

            <br class="clear"/>
        </div>

    </div>

    <div class="side-column left">

        @include('front.category.partials.subcategories')

        {{--@include('front.category.partials.refine')--}}

        @include('front.partials.twitter')

    </div>

@stop


