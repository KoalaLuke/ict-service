<div class="text-content">
    <h2>Compare products</h2>
</div>

<div class="product-specifications compare box block">

    <table border="1">

        <thead>

        <tr class="product-info">

            <th>Product</th>

            @foreach( $comparison['items'] as $item)
                <td>
                    <h3><a href="{{ route('front.product.show',$item->slug) }}">{{ $item->name }}</a></h3>
                </td>
            @endforeach

        </tr>

        <tr class="product-image">

            <th>Image</th>

            @foreach( $comparison['items'] as $item)
                <td>
                    @if (!empty($item->edict_imageurl))
                        <a href="{{ route('front.product.show',$item->slug) }}"><img src="{{ $item->edict_imageurl }}"
                                                                                     alt="{{ $item->name }}"/></a>
                    @endif
                </td>
            @endforeach

        </tr>

        <tr class="product-info">

            <th>Brand</th>

            @foreach( $comparison['items'] as $item)
                <td>{{ $item->vendorname }}</td>
            @endforeach

        </tr>

        @include('front.product.partials.specificationTable',['products' => $comparison['items'] ])

        <tr class="product-info">

            <th>Price</th>

            @foreach( $comparison['items'] as $item)
                <td>
                    @if (!empty($item->icts_rrp)) <h4 class="rrp"><strike>RRP: &pound;{{ $item->icts_rrp }}</strike>
                    </h4> @endif
                    <h2 class="price">&pound;{{ $item->price }}</h2>
                </td>
            @endforeach

        </tr>

        <tr class="product-info">

            <th>Purchase</th>

            @foreach( $comparison['items'] as $item)
                <td>
                    <div class="button info">

                        <a href="{{ route('front.product.show',$item->slug) }}"><i></i> View <span>details</span></a>

                    </div>

                    {!! Form::open(['method'=>'POST', 'route' => ['front.basket.add'], 'class'=>'form-inline add_to_basket']) !!}
                    {!! Form::hidden('id', $item->id) !!}
                    {!! Form::hidden('qty', 1) !!}
                    {!! Form::button('<i></i> Add', array('class' => 'button basket primary', 'type'=>'submit')) !!}
                    {!! Form::close() !!}

                </td>
            @endforeach

        </tr>

        </thead>

    </table>

</div>
