@extends('front.layouts.front')

@section('content')

    @include('front.partials.banner')

    <div class="main-column right">

        <div class="text-content">

            <h1>{{ $page->name }}</h1>

            {!! $page->content !!}

        </div>

    </div>

    <div class="side-column left">

        @include('front.partials.twitter')

    </div>

@stop