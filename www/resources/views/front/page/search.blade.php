@extends('front.layouts.front')

@section('content')

    @include('front.partials.banner')

    <div class="main-column right">

        <div class="intro text-content">

            <h1>Search</h1>

            <p>You searched for {{ $term }}</p>

        </div>

        <div class="products searchresults grid">

            @foreach ($products as $product)
                @include('front.product.partials.box', ['product' => $product])
            @endforeach

            <br class="clear"/>

        </div>

    </div>

    <div class="side-column left">

        @include('front.partials.twitter')

    </div>

@stop


