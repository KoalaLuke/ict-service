@extends('front.layouts.front')

@section('content')

    <div class="main-column right">
        <div class="text-content">
            <h1>Your basket</h1>
            <p>You have <strong>{{ $basket['count'] }}</strong> items in your basket...</p>
            {!! $website->basket_text !!}
        </div>

        @if (!empty($basket['count']))

            <div class="basket block box">

                {!! Form::open(['method'=>'POST', 'route' => ['front.basket.update'], 'class'=>'form-inline update_basket']) !!}

                <table>

                    <thead>
                    <tr>
                        <th colspan="2">Item</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Remove</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($basket['items'] as $product)

                        <tr class="basketitem {{ $product->slug }}">

                            <td class="image">
                                <a href="{{ route('front.product.show',$product->slug) }}">
                                    <img src="{{ route('front.product.image',[$product->id,50]) }}"/>
                                </a>
                            </td>
                            <td class="description">
                                <h3><a href="{{ route('front.product.show',$product->slug) }}">{{ $product->name }}</a>
                                </h3>
                            </td>
                            <td class="qty">
                                <a class="icon qtyControl subtract" href="">Subtract</a>
                                <input type="number" name="products[{{ $product->id }}][qty]"
                                       value="{{ $product->qty }}">
                                <a class="icon qtyControl add" href="">Add</a>
                            </td>
                            <td class="edit">{!! Form::button('Update', array('class' => 'icon update', 'type'=>'submit')) !!}</td>
                            <td class="price">
                                @if (!empty($product->discount))
                                    <strike>&pound;{{ number_format($product->subtotal, 2, '.', '') }} </strike>@endif
                                    &pound;{{ number_format($product->total, 2, '.', '') }}
                            </td>
                            <td class="remove"><a class="icon remove" href="">Remove</a></td>

                        </tr>

                    @endforeach
                    </tbody>

                </table>


                {!! Form::close() !!}


                <div class="discount left">
                    @if($basket['code'])
                        <div class="value"><a href="{{ route('front.basket.clear') }}">Clear discount
                                "{{ $basket['code'] }}"</a></div>
                    @endif
                    @if(!$basket['code'])


                        {!! Form::open(['method'=>'POST', 'route' => ['front.basket.discount'], 'class'=>'form-inline discount_basket']) !!}

                        <input type="text" name="code" placeholder="Discount code"/>

                        {!! Form::button('Apply', array('class' => 'button update', 'type'=>'submit')) !!}

                        {!! Form::close() !!}


                    @endif

                </div>
                <div class="totals right">
                    <div class="value subtotal">
                        <strong>Net total (ex VAT):</strong>
                    <span>
                        @if (!empty($basket['discounts']))
                            <strike>&pound;{{ number_format($basket['dummySubtotal'], 2, '.', '') }} </strike> @endif
                            &pound;{{ number_format($basket['subtotal'], 2, '.', '') }}
                    </span>
                    </div>
                    <div class="value tax">
                        <strong>VAT:</strong> <span>&pound;{{ number_format($basket['vat'], 2, '.', '') }}</span>
                    </div>
                    <div class="value total">
                        <strong>Grand total:</strong>
                        <span>&pound;{{ number_format($basket['total'], 2, '.', '') }}</span>
                    </div>
                    <div class="button basket primary">
                        <a href="{{ route('account.checkout') }}"><i></i> Proceed</a>
                    </div>
                </div>

                <br class="clear">
            </div>
            <div class="button">
                <a href="javascript:window.print();">Print quote</a>
            </div>


            <div class="text-content">
                @include('front.partials.cards_accepted')
            </div>

        @endif

    </div>

    <div class="side-column left">
        @include('front.partials.twitter')
    </div>

    <br class="clear">

@stop