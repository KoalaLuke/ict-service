@extends('front.layouts.front')

@section('content')

    @include('front.partials.banner')

    <div class="main-column right">

        <div class="text-content">

            <h1>Contact us</h1>

            {!! $website->contact_text !!}

            @include('front.partials.errors')

            @include('front.partials.success')

            @if(!Session::has('success'))

                <div class="form block box">

                    <h2>Send us a message...</h2>

                    {!! Form::open(['method'=>'POST', 'route' => ['front.contact.send']]) !!}

                    <div class="form-row">
                        {!! Form::label('organisation','School/Organisation') !!}
                        {!! Form::text('organisation',null,['id'=>'organisation', 'placeholder'=>'Organisation']) !!}
                    </div>

                    <div class="form-row">
                        {!! Form::label('title','Title') !!}
                        {!! Form::select('title',['Mr'=>'Mr.','Mrs'=>'Mrs.','Miss'=>'Miss.','Ms'=>'Ms.','Dr'=>'Dr.','Prof'=>'Prof.','Rev'=>'Rev.'], null) !!}
                    </div>

                    <div class="form-row">
                        {!! Form::label('first_name','First name') !!}
                        {!! Form::text('first_name',null,['id'=>'first_name', 'placeholder'=>'First name']) !!}
                    </div>

                    <div class="form-row">
                        {!! Form::label('last_name','Last name') !!}
                        {!! Form::text('last_name',null,['id'=>'last_name', 'placeholder'=>'Last name']) !!}
                    </div>

                    <div class="form-row">
                        {!! Form::label('email','Email') !!}
                        {!! Form::text('email',null,['id'=>'email', 'placeholder'=>'Email']) !!}
                    </div>

                    <div class="form-row">
                        {!! Form::label('telephone','Telephone') !!}
                        {!! Form::text('telephone',null,['id'=>'telephone', 'placeholder'=>'Telephone']) !!}
                    </div>

                    <div class="form-row">
                        {!! Form::label('message','Message') !!}
                        {!! Form::textarea('message',null,['id'=>'message', 'placeholder'=>'Message']) !!}
                    </div>

                    <div class="form-row">
                        {!! Form::submit('Send message',['class'=>'button']) !!}
                    </div>

                    {!! Form::close() !!}

                </div>

            @endif

        </div>

    </div>

    <div class="side-column left">

        @include('front.partials.twitter')

    </div>

    <br class="clear">

@stop