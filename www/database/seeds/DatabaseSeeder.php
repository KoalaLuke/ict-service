<?php

use App\Models\Accreditation;
use App\Models\Banner;
use App\Models\Discount;
use App\Models\Email;
use App\Models\Installation;
use App\Models\FeaturedProduct;
use App\Models\Image;
use App\Models\Page;
use App\Models\Slide;
use App\Models\Website;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        $this->call(PageTableSeeder::class);

        Model::reguard();
    }
}

class PageTableSeeder extends Seeder {

    public function run()
    {
        DB::table('pages')->delete();

        Page::create([
            'name' => 'About us',
            'active' => '1',
            'position' => 'header',
            'slug' => 'about-us',
            'content' => 'Cras dictum. Maecenas ut turpis. In vitae erat ac orci dignissim eleifend. Nunc quis justo. Sed vel ipsum in purus tincidunt pharetra. Sed pulvinar, felis id consectetuer malesuada, enim nisl mattis elit, a facilisis tortor nibh quis leo. Sed augue lacus, pretium vitae, molestie eget, rhoncus quis, elit. Donec in augue. Fusce orci wisi, ornare id, mollis vel, lacinia vel, massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
        ]);

        Page::create([
            'name' => 'Privacy',
            'active' => '1',
            'position' => 'footer',
            'slug' => 'privacy',
            'content' => 'Cras dictum. Maecenas ut turpis. In vitae erat ac orci dignissim eleifend. Nunc quis justo. Sed vel ipsum in purus tincidunt pharetra. Sed pulvinar, felis id consectetuer malesuada, enim nisl mattis elit, a facilisis tortor nibh quis leo. Sed augue lacus, pretium vitae, molestie eget, rhoncus quis, elit. Donec in augue. Fusce orci wisi, ornare id, mollis vel, lacinia vel, massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
        ]);

        Page::create([
            'name' => 'Computers',
            'active' => '1',
            'position' => 'sitemap',
            'slug' => 'computers',
            'content' => 'Cras dictum. Maecenas ut turpis. In vitae erat ac orci dignissim eleifend. Nunc quis justo. Sed vel ipsum in purus tincidunt pharetra. Sed pulvinar, felis id consectetuer malesuada, enim nisl mattis elit, a facilisis tortor nibh quis leo. Sed augue lacus, pretium vitae, molestie eget, rhoncus quis, elit. Donec in augue. Fusce orci wisi, ornare id, mollis vel, lacinia vel, massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.'
        ]);

        Website::create([
            'name' => 'The ICT Service',
            'telephone' => '0121 28 28 20',
            'email' => 'info@theictservice.com',
            'facebook' => 'http://theictservice.org.uk/facebook',
            'contact_text' => 'Cras dictum. Maecenas ut turpis. In vitae erat ac orci dignissim eleifend. Nunc quis justo. Sed vel ipsum in purus tincidunt pharetra. Sed pulvinar, felis id consectetuer malesuada, enim nisl mattis elit,',
            'basket_text' => 'Sed pulvinar, felis id consectetuer malesuada, enim nisl mattis elit, a facilisis tortor nibh quis leo. Sed augue lacus.',
            'twitter' => 'The ICT Service'
        ]);

        User::create([
            'organisation'=> 'eSchools',
            'address_line_1'=> '2 Kings Court',
            'address_line_2'=> 'Willie Snaith Road',
            'address_line_3'=> '',
            'city'=> 'Newmarket',
            'postcode'=> 'CB8 7SG',
            'urn'=> 'sdfsdf8998gsdf',
            'title'=> 'Mr',
            'first_name'=> 'Luke',
            'last_name'=> 'Fretwell',
            'telephone'=> '012345678901',
            'email'=> 'luke.fretwell@eschools.co.uk',
            'password'=> '$2y$10$uOcCYY6VvQTkkZQ9qgSMw.A4w7XztUPyJrjhWhbypcNvupPOoe4UG',
            'admin' => 1
        ]);

        Image::create([
            'id' => 1,
            'name' => 'Banner 1',
            'file' => 'banner1.jpg',
        ]);

        Image::create([
            'id' => 2,
            'name' => 'Banner 2',
            'file' => 'banner2.jpg',
        ]);

        Image::create([
            'id' => 3,
            'name' => 'Banner 3',
            'file' => 'slide1.jpg',
        ]);

        Image::create([
            'id' => 4,
            'name' => 'Banner 4',
            'file' => 'slide2.jpg',
        ]);

        Image::create([
            'id' => 5,
            'name' => 'Accred 1',
            'file' => 'accred1.png',
        ]);

        Image::create([
            'id' => 6,
            'name' => 'Accred 2',
            'file' => 'accred2.png',
        ]);

        Image::create([
            'id' => 7,
            'name' => 'Accred 3',
            'file' => 'accred3.png',
        ]);

        Image::create([
            'id' => 8,
            'name' => 'Accred 4',
            'file' => 'accred4.png',
        ]);

        Image::create([
            'id' => 9,
            'name' => 'Accred 5',
            'file' => 'accred5.png',
        ]);

        Image::create([
            'id' => 10,
            'name' => 'Accred 6',
            'file' => 'accred6.png',
        ]);

        Banner::create([
            'name' => 'Banner 1',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 1
        ]);

        Banner::create([
            'name' => 'Banner 2',
            'link' => '/servers',
            'active' => 1,
            'order' => 2,
            'image_id' => 2
        ]);

        Slide::create([
            'name' => 'Slide 1',
            'header' => 'Sub heading',
            'text' => 'Pellentesque vel dui sed orci faucibus iaculis. Suspendisse dictum magna id purus tincidunt rutrum. Nulla congue.',
            'button' => 'Button',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 3
        ]);

        Slide::create([
            'name' => 'Slide 2',
            'header' => 'Sub heading',
            'text' => 'Curabitur quis ipsum. Mauris nisl tellus, aliquet eu, suscipit eu, ullamcorper quis, magna. Mauris elementum.',
            'button' => 'Button',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 4
        ]);

        FeaturedProduct::create(['productid'=>'439f6de7-0474-e111-b02a-14feb572c90c']);
        FeaturedProduct::create(['productid'=>'f8ec032b-8011-e311-a01d-001018a2a4f2']);
        FeaturedProduct::create(['productid'=>'73923c8b-cef7-e411-bca5-14feb572c90a']);
        FeaturedProduct::create(['productid'=>'13132ceb-9af9-e111-bea9-001018a2a4f2']);

        Accreditation::create([
            'name' => 'Accred 1',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 5
        ]);

        Accreditation::create([
            'name' => 'Accred 2',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 6
        ]);

        Accreditation::create([
            'name' => 'Accred 3',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 7
        ]);

        Accreditation::create([
            'name' => 'Accred 4',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 8
        ]);

        Accreditation::create([
            'name' => 'Accred 5',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 9
        ]);

        Accreditation::create([
            'name' => 'Accred 6',
            'link' => '/servers',
            'active' => 1,
            'order' => 1,
            'image_id' => 10
        ]);

        Installation::create([
            'icts_productcategoryid'=>'6d664269-d340-e511-a3a8-14feb572c90a',
            'productid'=>'0d4aa8f2-dd4c-e111-aef9-001018a2a4f2'
        ]);

        Installation::create([
            'icts_productcategoryid'=>'6d664269-d340-e511-a3a8-14feb572c90a',
            'productid'=>'a1f58aaa-dd4c-e111-aef9-001018a2a4f2',
        ]);

        Installation::create([
            'icts_productcategoryid'=>'6d664269-d340-e511-a3a8-14feb572c90a',
            'productid'=>'0cd22acf-dd4c-e111-aef9-001018a2a4f2',
        ]);

        Email::create([
            'reference'=>'signup',
            'name'=>'Sign up'
        ]);

        Email::create([
            'reference'=>'order',
            'name' => 'Order confirmation',
            'subject' => 'Order confirmation',
            'top' => '<h1>You\'ve signed up</h1>',
            'middle' => '<p>This can go in the middle</p>',
            'bottom' => '<p>This can go at the bottom</p>'
        ]);

        Email::create([
            'reference'=>'password',
            'name'=>'Password Reset'
        ]);

        Discount::create([
            'name' => 'Get 20% discount',
            'code' => 'get20',
            'type' => 'per',
            'discount' => '0.2'
        ]);
    }

}
