<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category', function(Blueprint $blueprint){

            $blueprint->unsignedInteger('product_id');
            $blueprint->unsignedInteger('category_id');

            $blueprint->unique(['product_id', 'category_id']);

            $blueprint->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $blueprint->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');

            $blueprint->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category');
    }
}
