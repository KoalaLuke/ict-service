<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccreditationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accreditations', function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->string('name');
            $blueprint->string('link');
            $blueprint->boolean('active')->index();
            $blueprint->integer('order');
            $blueprint->unsignedInteger('image_id');
            $blueprint->timestamps();

            $blueprint->foreign('image_id')->references('id')->on('images')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accreditations');
    }
}
