<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->string('organisation');
            $blueprint->string('title');
            $blueprint->string('first_name');
            $blueprint->string('last_name');
            $blueprint->string('email');
            $blueprint->string('telephone');
            $blueprint->text('message');
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
