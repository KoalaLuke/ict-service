<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_addresses', function (Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->unsignedInteger('user_id');

            $blueprint->string('contact');
            $blueprint->string('address_line_1');
            $blueprint->string('address_line_2');
            $blueprint->string('address_line_3');
            $blueprint->string('city');
            $blueprint->string('postcode');

            $blueprint->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery_addresses');
    }
}
