<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $blueprint){

            // user relate
            $blueprint->string('delivery_contact')->nullable();
            $blueprint->string('delivery_address_line_1')->nullable();
            $blueprint->string('delivery_address_line_2')->nullable();
            $blueprint->string('delivery_address_line_3')->nullable();
            $blueprint->string('delivery_city')->nullable();
            $blueprint->string('delivery_postcode')->nullable();
            $blueprint->string('po_number')->nullable();
            $blueprint->string('terms_check')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function(Blueprint $blueprint){

            // user relate
            $blueprint->dropColumn('delivery_contact');
            $blueprint->dropColumn('delivery_address_line_1');
            $blueprint->dropColumn('delivery_address_line_2');
            $blueprint->dropColumn('delivery_address_line_3');
            $blueprint->dropColumn('delivery_city');
            $blueprint->dropColumn('delivery_postcode');
            $blueprint->dropColumn('po_number');
            $blueprint->dropColumn('terms_check');

        });
    }
}
