<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->string('reference');
            $blueprint->string('name');

            $blueprint->text('subject');
            $blueprint->text('top');
            $blueprint->text('middle');
            $blueprint->text('bottom');

            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emails');
    }
}
