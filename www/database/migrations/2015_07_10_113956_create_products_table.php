<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $blueprint){

            $blueprint->increments('id');
            $blueprint->timestamps();
            $blueprint->softDeletes();

            $blueprint->string('productid')->unique();
            $blueprint->string('statecode');
            $blueprint->string('name');
            $blueprint->string('icts_saleswebsite')->nullable();
            $blueprint->string('productnumber')->nullable();
            $blueprint->decimal('price',10, 2);
            $blueprint->string('icts_rrp')->nullable();
            $blueprint->dateTime('createdon')->nullable();
            $blueprint->dateTime('modifiedon')->nullable();
            $blueprint->text('description')->nullable();
            $blueprint->string('producturl')->nullable();
            $blueprint->string('edict_imageurl')->nullable();
            $blueprint->string('icts_additionalimageurls')->nullable();
            $blueprint->string('icts_usermanualurl')->nullable();
            $blueprint->string('edict_productcategory')->nullable();
            $blueprint->string('icts_category')->nullable();
            $blueprint->text('icts_keywords')->nullable();
            $blueprint->string('producttypecode')->nullable();
            $blueprint->string('vendorname')->nullable();
            $blueprint->string('vendorpartnumber')->nullable();
            $blueprint->string('suppliername')->nullable();
            $blueprint->integer('edict_eta')->nullable();
            $blueprint->string('isstockitem')->nullable();
            $blueprint->string('quantityonhand')->nullable();
            $blueprint->integer('stockvolume')->nullable();
            $blueprint->integer('stockweight')->nullable();
            $blueprint->string('transactioncurrencyid')->nullable();
            $blueprint->string('slug')->unique();

            $blueprint->timestamp('imported_at');
            $blueprint->unsignedInteger('import_id')->nullable();

            $blueprint->foreign('import_id')->references('id')->on('imports')->onDelete('SET NULL')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
