<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_products', function(Blueprint $blueprint){

            $blueprint->unsignedInteger('parent_id');
            $blueprint->unsignedInteger('child_id');
            $blueprint->enum('type', ['associated', 'alternative'])->index();
            $blueprint->text('note')->nullable();
            $blueprint->timestamps();

            $blueprint->foreign('parent_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $blueprint->foreign('child_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_products');
    }
}
