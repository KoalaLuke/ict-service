<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function(Blueprint $blueprint){

            $blueprint->increments('id');
            $blueprint->timestamps();
            $blueprint->softDeletes();

            $blueprint->string('statecode');
            $blueprint->string('icts_name');
            $blueprint->string('icts_imageurl')->nullable();
            $blueprint->string('icts_parentcategory')->nullable();
            $blueprint->string('modifiedon')->nullable();
            $blueprint->string('icts_productcategoryid')->unique();
            $blueprint->string('slug')->unique();

            $blueprint->timestamp('imported_at');
            $blueprint->unsignedInteger('import_id')->nullable();

            $blueprint->foreign('import_id')->references('id')->on('imports')->onDelete('SET NULL')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
