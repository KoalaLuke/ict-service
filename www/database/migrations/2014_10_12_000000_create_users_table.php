<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $blueprint) {
            $blueprint->increments('id');

            $blueprint->string('organisation');
            $blueprint->string('address_line_1');
            $blueprint->string('address_line_2');
            $blueprint->string('address_line_3');
            $blueprint->string('city');
            $blueprint->string('postcode');
            $blueprint->string('urn');

            $blueprint->string('title');
            $blueprint->string('first_name');
            $blueprint->string('last_name');
            $blueprint->string('telephone');

            $blueprint->string('email')->unique();
            $blueprint->string('password', 60);

            $blueprint->boolean('admin')->default(0);

            $blueprint->rememberToken();
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
