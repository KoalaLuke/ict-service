<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->string('name');
            $blueprint->string('header');
            $blueprint->string('text');
            $blueprint->string('button');
            $blueprint->string('link');
            $blueprint->unsignedInteger('image_id');
            $blueprint->timestamps();
            $blueprint->boolean('active')->index();
            $blueprint->integer('order');

            $blueprint->foreign('image_id')->references('id')->on('images')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slides');
    }
}
