<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $blueprint){

            $blueprint->increments('id');
            $blueprint->unsignedInteger('user_id');

            // order related
            $blueprint->decimal('discount',10, 2);
            $blueprint->decimal('subtotal',10, 2);
            $blueprint->decimal('vat',10, 2);
            $blueprint->decimal('total',10, 2);
            $blueprint->string('code');

            // user relate
            $blueprint->string('organisation');
            $blueprint->string('address_line_1');
            $blueprint->string('address_line_2');
            $blueprint->string('address_line_3');
            $blueprint->string('city');
            $blueprint->string('postcode');
            $blueprint->string('urn');

            $blueprint->text('note');
            $blueprint->timestamps();

            $blueprint->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
