<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function(Blueprint $blueprint){

            $blueprint->increments('id');
            $blueprint->timestamps();

            $blueprint->string('statecode');
            $blueprint->string('icts_name');
            $blueprint->string('icts_comparisonmethod')->nullable();
            $blueprint->integer('icts_sortorder')->nullable();
            $blueprint->string('icts_parentfeature')->nullable();
            $blueprint->text('icts_description')->nullable();
            $blueprint->dateTime('modifiedon')->nullable();
            $blueprint->string('icts_productfeatureid')->unique();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('features');
    }
}
