<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('websites', function(Blueprint $blueprint){

            // user relate
            $blueprint->text('order_text')->nullable();
            $blueprint->text('terms_text')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('websites', function(Blueprint $blueprint){

            // user relate
            $blueprint->dropColumn('order_text');
            $blueprint->dropColumn('terms_text');

        });
    }
}
