<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function(Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('products_new');
            $blueprint->integer('products_imported');
            $blueprint->integer('categories_new');
            $blueprint->integer('categories_imported');
            $blueprint->text('relationships_failed');
            $blueprint->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imports');
    }
}
