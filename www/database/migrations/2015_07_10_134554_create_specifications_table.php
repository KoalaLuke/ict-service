<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specifications', function(Blueprint $blueprint){

            $blueprint->increments('id');
            $blueprint->unsignedInteger('product_id');
            $blueprint->unsignedInteger('feature_id');
            $blueprint->timestamps();

            $blueprint->string('statecode');
            $blueprint->string('icts_product');
            $blueprint->string('icts_feature');
            $blueprint->string('icts_value');
            $blueprint->string('icts_displaytext');
            $blueprint->dateTime('modifiedon')->nullable();
            $blueprint->string('icts_productspecid')->index();

            $blueprint->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $blueprint->foreign('feature_id')->references('id')->on('features')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('specifications');
    }
}
