var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {


    mix.sass('admin/app.scss','public/css/admin/admin.css');

    mix.styles([

        'vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css',
        'vendors/bootstrap/css/bootstrap.min.css',
        'css/admin/themes/style1/yellow-blue.css',
        'css/admin/style-responsive.css',
        'css/admin/admin.css'

    ],'public/css/admin/all.css','public');

    mix.sass('front/app.scss','public/css/front/front.css');

    mix.styles([

        'css/front/mowatt/fonts.css',
        'css/front/mowatt/jquery.featherlight.css',
        'css/front/mowatt/jquery.slick.css',
        'css/front/mowatt/jquery.tooltipster.css',
        'css/front/mowatt/style.css',
        'css/front/front.css'

    ],'public/css/front/all.css','public');

    mix.scripts([

        'js/jquery-1.10.2.min.js',
        'js/jquery-migrate-1.2.1.min.js',
        'js/jquery-ui.js',
        'vendors/bootstrap/js/bootstrap.min.js',
        'vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js',
        'js/html5shiv.js',
        'js/respond.min.js',
        'vendors/jquery-cookie/jquery.cookie.js',
        'vendors/metisMenu/jquery.metisMenu.js',
        'vendors/slimScroll/jquery.slimscroll.min.js',
        'vendors/iCheck/icheck.min.js',
        'vendors/responsive-tabs/responsive-tabs.js',
        'vendors/jquery-highcharts/highcharts.js',
        'js/admin-theme.js'

    ],'public/js/admin_packages.js','public');

    mix.scripts([

        'js/mowatt/jquery.js',
        'js/mowatt/jquery.cycle.js',
        'js/mowatt/jquery.featherlight.js',
        'js/mowatt/jquery.slick.js',
        'js/mowatt/jquery.tooltipster.js',
        'js/mowatt/jquery.zoom.js',
        'js/mowatt/script.js',
        'js/js.cookies.js',
        'js/front1.js'

    ],'public/js/front.js','public');

    mix.version([
        'public/js/front.js',
        'public/js/admin_packages.js',
        'public/css/admin/all.css',
        'public/css/front/all.css'
    ]);

});
