<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Accreditation
 *
 * @property integer $id
 * @property string $name
 * @property string $link
 * @property integer $image_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Image $Image
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Accreditation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Accreditation whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Accreditation whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Accreditation whereImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Accreditation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Accreditation whereUpdatedAt($value)
 */
	class Accreditation {}
}

namespace App\Models{
/**
 * App\Models\Banner
 *
 * @property integer $id
 * @property string $name
 * @property string $link
 * @property integer $image_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Image $Image
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereUpdatedAt($value)
 */
	class Banner {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $statecode
 * @property string $icts_name
 * @property string $icts_imageurl
 * @property string $icts_parentcategory
 * @property \Carbon\Carbon $modifiedon
 * @property string $icts_productcategoryid
 * @property string $slug
 * @property \Carbon\Carbon $imported_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|self[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\Specification[] $specifications
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereStatecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIctsName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIctsImageurl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIctsParentcategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereModifiedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIctsProductcategoryid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereImportedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category vendorId($value)
 */
	class Category {}
}

namespace App\Models{
/**
 * App\Models\Feature
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $statecode
 * @property string $icts_name
 * @property string $icts_comparisonmethod
 * @property integer $icts_sortorder
 * @property string $icts_parentfeature
 * @property string $icts_description
 * @property \Carbon\Carbon $modifiedon
 * @property string $icts_productfeatureid
 * @property-read \Illuminate\Database\Eloquent\Collection|self[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|Specification[] $specifications
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereStatecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsComparisonmethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsSortorder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsParentfeature($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereModifiedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsProductfeatureid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature vendorId($value)
 */
	class Feature {}
}

namespace App\Models{
/**
 * App\Models\FeaturedProduct
 *
 * @property integer $id
 * @property string $productid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Product $Product
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeaturedProduct whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeaturedProduct whereProductid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeaturedProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeaturedProduct whereUpdatedAt($value)
 */
	class FeaturedProduct {}
}

namespace App\Models{
/**
 * App\Models\Image
 *
 * @property integer $id
 * @property string $name
 * @property string $file
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereFile($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Image whereUpdatedAt($value)
 */
	class Image {}
}

namespace App\Models{
/**
 * App\Models\Page
 *
 * @property integer $id
 * @property string $name
 * @property boolean $active
 * @property string $position
 * @property integer $order
 * @property string $slug
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page position($value)
 */
	class Page {}
}

namespace App\Models{
/**
 * App\Models\Product
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $productid
 * @property string $statecode
 * @property string $name
 * @property string $icts_saleswebsite
 * @property string $productnumber
 * @property float $price
 * @property string $icts_rrp
 * @property \Carbon\Carbon $createdon
 * @property \Carbon\Carbon $modifiedon
 * @property string $description
 * @property string $producturl
 * @property string $edict_imageurl
 * @property string $icts_additionalimageurls
 * @property string $icts_usermanualurl
 * @property string $edict_productcategory
 * @property string $icts_category
 * @property string $icts_keywords
 * @property string $producttypecode
 * @property string $vendorname
 * @property string $vendorpartnumber
 * @property string $suppliername
 * @property integer $edict_eta
 * @property string $isstockitem
 * @property string $quantityonhand
 * @property integer $stockvolume
 * @property integer $stockweight
 * @property string $transactioncurrencyid
 * @property string $slug
 * @property \Carbon\Carbon $imported_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|Specification[] $specifications
 * @property-read \Illuminate\Database\Eloquent\Collection|Relationship[] $relationships
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProductid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereStatecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsSaleswebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProductnumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsRrp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCreatedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereModifiedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProducturl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereEdictImageurl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsAdditionalimageurls($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsUsermanualurl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereEdictProductcategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProducttypecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereVendorname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereVendorpartnumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereSuppliername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereEdictEta($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIsstockitem($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereQuantityonhand($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereStockvolume($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereStockweight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereTransactioncurrencyid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereImportedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product vendorId($value)
 */
	class Product {}
}

namespace App\Models{
/**
 * App\Models\ProductProducts
 *
 * @property integer $parent_id
 * @property integer $child_id
 * @property string $type
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereChildId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereUpdatedAt($value)
 */
	class ProductProducts {}
}

namespace App\Models{
/**
 * App\Models\Slide
 *
 * @property integer $id
 * @property string $name
 * @property string $header
 * @property string $text
 * @property string $button
 * @property string $link
 * @property integer $image_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Image $Image
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereHeader($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereButton($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereUpdatedAt($value)
 */
	class Slide {}
}

namespace App\Models{
/**
 * App\Models\Specification
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $feature_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $statecode
 * @property string $icts_product
 * @property string $icts_feature
 * @property string $icts_value
 * @property string $icts_displaytext
 * @property \Carbon\Carbon $modifiedon
 * @property string $icts_productspecid
 * @property-read Product $product
 * @property-read Feature $feature
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereFeatureId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereStatecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsProduct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsFeature($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsDisplaytext($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereModifiedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsProductspecid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification vendorId($value)
 */
	class Specification {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 */
	class User {}
}

namespace App\Models{
/**
 * App\Models\Website
 *
 * @property integer $id
 * @property string $name
 * @property string $telephone
 * @property string $email
 * @property string $twitter
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereTwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereUpdatedAt($value)
 */
	class Website {}
}

