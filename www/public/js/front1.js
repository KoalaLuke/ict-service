$(document).ready(function() {

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': ictservices.token }
    });

    $('.add_to_basket').on('submit',function(e){

        e.preventDefault();

        $.ajax({

            method: "POST",
            url: ictservices.routes.basket_add,
            data: $( this ).serialize()

        }).fail(function(data) {

        }).success(function( data ) {

            var basket_updated = $('<div></div>')
                .attr('id','basket_updated')
                .html('<div class="alert success block box"><h2>Basket updated</h2><p>This product has been added to your basket.</p><p class="buttons"><span class="button"><a href="javascript:basket_updated.remove();">Continue shopping</a></span> <span class="button basket primary"><a href="/basket"><i></i> Go to basket</a></span></p></div>');

            $('body').append(basket_updated);

            basket_updated.show(function(){

 //               setTimeout(function() {
 //                   basket_updated.remove();
 //               }, 2000)

            });


            $('#basket_count').html(data.count);
            $('#sidr-id-basket_count').html(data.count)

        });

    });

    $('.compareCheck').on('click', function(){

        $(this).parent('form').submit();

    })

    $('.add_to_comparison').on('submit',function(e){

        e.preventDefault();

        update_compare($(this, false));

    });

    $('.remove_from_comparison').on('submit',function(e){

        e.preventDefault();

        update_compare($(this), true);

    });

    function update_compare(form, remove){

        $.ajax({

            method: "POST",
            url: ictservices.routes.comparison_add,
            data: form.serialize()

        }).fail(function(data) {

        }).success(function( data ) {

            $('#comparison_count_btn').removeClass().addClass('button counter' + data.count);
            $('body').removeClass().addClass('counter' + data.count);
            $('#comparison_count').html(data.count);

            if(remove){
                window.location.reload()
            }

        });

    }


    $('body').addClass('counter' + $('#comparison_count').html());


    $('.qty .qtyControl').on('click', function(){

        var control = $(this);
        var counter = control.parent().find('input');
        var count = counter.val();
        if(control.hasClass('subtract')){
            count--;
        } else if(control.hasClass('add')) {
            count++;
        }
        if(count<0){count=0;}
        counter.val(count);

        highlightUpdateButton(control);

        return false;

    });

    $('.update_basket').on('click', '.icon.remove', function(e){

        e.preventDefault();

        var control = $(this);
        var counter = control.parents('tr').find('input');
        var count = counter.val(0);

        $('.update_basket').submit();

    });

    $('.update_basket .qty input').on('change', function(){

        highlightUpdateButton($(this));

    });

    function highlightUpdateButton(element){

        var row = element.closest('tr');
        var update_button = $(".update",row);

        if(!update_button.hasClass("highlight")){
            update_button.addClass("highlight");
        }

    }


    var delivery_address_container = $('.delivery_address_container');
    var delivery_address_switch = $('#same_address');
    var delivery_address_field = $('#delivery_address');

    delivery_address_container_switch(delivery_address_switch);

    delivery_address_switch.on('click',function(){

        delivery_address_container_switch(delivery_address_switch);

    });

    function delivery_address_container_switch(check){
        if(check.is(':checked')){
            delivery_address_container.hide();
            delivery_address_field.val(0);
        } else {
            delivery_address_container.show();
        }
    }

    $('#items-quantity').on('change', function(){

        $(this).parent('form').submit();

    });

    $('#items-per-page').on('submit', function(e){

        e.preventDefault();

        $.ajax({

            method: "POST",
            url: ictservices.routes.per_page,
            data: $( this ).serialize()

        }).fail(function(data) {

        }).success(function( data ) {

            location.reload(true);

        });

    });

    $('#sort-by').on('change', function(){

        $(this).parent('form').submit();

    });

    $('#sort').on('submit', function(e){

        e.preventDefault();

        $.ajax({

            method: "POST",
            url: ictservices.routes.product_order,
            data: $( this ).serialize()

        }).fail(function(data) {

        }).success(function( data ) {

            location.reload(true);

        });

    });


});//