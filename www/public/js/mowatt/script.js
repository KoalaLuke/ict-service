$(document).ready(function(){

	$('.fancypants').fancybox();
	
	$(".view-type .grid a").click(function(e) {
		e.preventDefault();
		$(".main-column .products").removeClass("list").addClass("grid");
		$(".view-type .list").removeClass("active");
		$(".view-type .grid").addClass("active");
	});

	$(".view-type .list a").click(function(e) {
		e.preventDefault();
		$(".main-column .products").removeClass("grid").addClass("list");
		$(".view-type .grid").removeClass("active");
		$(".view-type .list").addClass("active");
	});

	$(".scrolling-logos ul").carouFredSel({
		circular: true,
		infinite: true,
		items: 5,
		width: "100%",
		height: 68,
		direction: "left",
		align: "center",
		scroll: {
			items: 1,
			easing: "elastic",
			duration: 1000,							
			pauseOnHover: true
		}
	});	

	$('.banner .slides').cycle({ 
    	fx: 'scrollHorz',
    	timeout: 80000,
    	slideResize: false,
    	next: '.banner .controls .next a',
    	prev: '.banner .controls .prev a'
	});

	$('.promo .slides').cycle({ 
    	fx: 'fade',
    	slideResize: false,
    	next: '.promo .controls .next a',
    	prev: '.promo .controls .prev a' 
	});

	$('.hamburger a').sidr({
        name: 'sidrnav',
		side: 'right',
		source: '#hamburger_account_header, .account-links, #hamburger_categories_header, .navigation, #hamburger_pages_header, .pages-nav'
	});

	$(".navigation ul.horizontal").overflowNavigation({
		toggleText: "More..."
	});

	$('.navigation ul > li').bind('mouseover', openSubMenu);
	$('.navigation ul > li').bind('mouseout', closeSubMenu);


		
	function openSubMenu() {
		$(this).addClass('hover');
		$(this).find('> ul').css('visibility', 'visible');	
	};
		
	function closeSubMenu() {
		$(this).removeClass('hover');
		$(this).find('> ul').css('visibility', 'hidden');	
	};

	$('.image.zoom').zoom();

	$('.tooltip').tooltipster({
		maxWidth: '400'
	});



	$(window).resize(function(){
	    $.sidr('close', 'sidrnav');
	});


	var TO = false;

	$(window).resize(function(){
		if(TO !== false){
			clearTimeout(TO);
		}
		TO = setTimeout(resize_boxes, 200);
	});

	resize_boxes();

	setTimeout(resize_boxes, 1000);

if ($('.productdetails .description').length){

	var content = $('.productdetails .description').html().split('<br>');
	$('.productdetails .description').empty().append('<ul>');
	for (var i = 0; i < content.length; i++) {
	    $('.productdetails .description ul').append('<li>' + content[i] + '</li>')
	}

}


if ($('.products.installations .summary-info p').length){


		$('.products.installations .summary-info p', this).each(function(){

			var content = nl2br($(this).html());
			$(this).html(content);

		});




}


	$( ".product[class*='bronze']" ).addClass("bronze");
	$( ".product[class*='silver']" ).addClass("silver");
	$( ".product[class*='gold']" ).addClass("gold");


	$( ".product[class*='file-server']" ).addClass("server");

	$( ".basketitem[class*='file-server']" ).addClass("server");

});

function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function resize_boxes() {

	$('.primary-details').css('height', 'auto');
	$('.primary-details h3').css('height', 'auto');
	$('.product-info').css('height', 'auto');

	$('.products.grid').each(function(){

		var highestBoxA = 0;
		var highestBoxB = 0;
		var highestBoxC = 0;

		$('.product-info', this).each(function(){
			if($(this).height() > highestBoxA){
				highestBoxA = $(this).height();
			}
		});

		$('.product-info .primary-details', this).each(function(){
			if($(this).height() > highestBoxB){
				highestBoxB = $(this).height();
			}
		});

		$('.product-info .primary-details h3', this).each(function(){
			if($(this).height() > highestBoxC){
				highestBoxC = $(this).height();
			}
		});  

		$('.primary-details h3',this).height(highestBoxC);
		$('.primary-details',this).height(highestBoxB);
		$('.product-info',this).height(highestBoxA);

	})


}