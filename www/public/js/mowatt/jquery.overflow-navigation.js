/*!
 * jQuery Overflow Navigation
 * https://github.com/janhartmann/jquery-overflow-navigation
 *
 * Copyright 2015, Jan Hartmann
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 */
;(function ($, window, document, undefined) {

    var pluginName = "overflowNavigation";

    var defaults = {
        toggleText: "View more...",
        dropDownListItemCssClass: "dropdown"
    };

    function Plugin(element, options) {
        var _this = this;
        this.settings = $.extend({}, defaults, options);
        this.element = element;
        this.$element = $(element);
        this._defaults = defaults;
        this._name = pluginName;

        this.init();

        $(window).resize(function() {
            _this.init();
        });
    }

    $.extend(Plugin.prototype, {
        init: function () {
            var _this = this;
            var $element = this.$element;
          
            var parentWidth = $element.parent().width();
			var dropdown = this.createDropDown();
			$element.append(dropdown);
			
            var childWidth = this.calculateChildWidth(); // Called after creation of drop down to make sure there is space for it.

            if (childWidth > parentWidth) {
                var childElements = $('> li', $element).not("." + this.settings.dropDownListItemCssClass).get().reverse();
                $(childElements).each(function () {
                    if (childWidth > parentWidth) {
						var childItem = $(this);
                        childItem.attr('data-original-width', childItem.outerWidth(true));
                        dropdown.children('ul.dropdown-menu').prepend(childItem);
                    }
                    childWidth = _this.calculateChildWidth();
                });
            }
            else {
                dropdown.children('ul.dropdown-menu').children().each(function () {
                    var childItem = $(this);
                    var childOriginalWidth = parseInt(childItem.data('original-width'));
                    if ((_this.calculateChildWidth() + childOriginalWidth) < parentWidth) {
                        dropdown.before(childItem);
                    } else {
                        return false; // If the first item can't be restored, don't look any further
                    }
                });
            }

			// Show the menu if there is any items
            if (dropdown.children('ul.dropdown-menu').children().length) {
                dropdown.show();
            }
			
        },
        calculateChildWidth: function () {
            var childWidth = 0;
            $('> li', this.$element).each(function () {
                childWidth += $(this).outerWidth(true);
            });
            return childWidth;
        },
        createDropDown: function () {
            var dropdown = $("." + this.settings.dropDownListItemCssClass, this.$element);
            if (!dropdown.length) {
                dropdown = $('<li></li>').addClass(this.settings.dropDownListItemCssClass).hide();
                dropdown.append($('<a class="dropdown-toggle" data-toggle="dropdown" href="#">' + this.settings.toggleText + '</a>'));
                dropdown.append($('<ul class="dropdown-menu"></ul>'));
            }
            return dropdown;
        }
    });

    $.fn[pluginName] = function (options) {
        this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
        return this;
    };

})(jQuery, window, document);
