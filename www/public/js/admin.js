$(function () {

    var config =
    {
        toolbar: [
            ['PasteText'],
            ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList','Format'],
            ['Link', 'Unlink'], ['Undo', 'Redo', '-', 'SelectAll'],
            ['Source']
        ]
    };

    $('textarea.editor').ckeditor(config);

    var sortable = $("#pagesTable .sortable");
    sortable.sortable({
        axis: 'y',
        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            $.ajax({
                data: data,
                type: 'POST',
                url: ''
            });
        }
    });
    sortable.disableSelection();

});