<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //
        parent::boot($router);

        //admin
        $router->model('accreditation','\App\Models\Accreditation');
        $router->model('banner','\App\Models\Banner');
        $router->model('category','\App\Models\Category');
        $router->model('discount','\App\Models\Discount');
        $router->model('email','\App\Models\Email');
        $router->model('image','\App\Models\Image');
        $router->model('installation','\App\Models\Installation');
        $router->model('message','\App\Models\Message');
        $router->model('order','\App\Models\Order');
        $router->model('page','\App\Models\Page');
        $router->model('featured-product','\App\Models\FeaturedProduct');
        $router->model('slide','\App\Models\Slide');
        $router->model('user','\App\Models\User');

        //account
        $router->model('deliveries','\App\Models\DeliveryAddress');

        //front
        $router->bind('page_slug',function($slug){

            return Page::findBySlugOrFail($slug);

        });

        $router->bind('product_slug',function($slug){

            return Product::findBySlugOrFail($slug);

        });

        $router->bind('category_slug',function($slug){

            return Category::findBySlugOrFail($slug);

        });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
