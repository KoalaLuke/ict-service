<?php

namespace App\Providers;

use App\Helpers\Breadcrumb;
use App\Models\Accreditation;
use App\Models\Banner;
use App\Models\Category;
use App\Models\FeaturedProduct;
use App\Models\Page;
use App\Models\Slide;
use App\Models\Specification;
use App\Models\Website;
use App\Repositories\Basket\BasketRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Email\EmailRepository;
use App\Repositories\Page\PageRepository;
use Auth;
use Config;
use Illuminate\Support\ServiceProvider;
use Session;
use Twitter;

class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeAccreditations();
        $this->composeAdmin();
        $this->composeBanners();
        $this->composeBasketCount();
        $this->composeBreadcrumb();
        $this->composeComparisonCount();
        $this->composeEmailPassword();
        $this->composeFeaturedProducts();
        $this->composeFooterLinks();
        $this->composeHeaderLinks();
        $this->composeNavigation();
        $this->composePerPage();
        $this->composeProductOrder();
        $this->composeSlideShow();
        $this->composeSpecificationTable();
        $this->composeTwitter();
        $this->composeVendorList();
        $this->composeWebsite();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function composeAccreditations(){

        view()->composer('front.partials.accreditations', function ($view) {

            $view->with('accreditations', Accreditation::all());

        });

    }

    public function composeAdmin()
    {
        view()->composer('admin.layouts.admin', function ($view) {

            $admin = Auth::user();

            $view->with('admin',$admin);

        });
    }

    public function composeBanners(){

        view()->composer('front.partials.banner', function ($view) {

            $view->with('banners', Banner::where('active',1)->orderBy('order','asc')->orderBy(\DB::raw('RAND()'))->get());

        });

    }

    public function composeBasketCount()
    {
        view()->composer('front.layouts.front', function ($view) {

            /** @var BasketRepository $repository */
            $repository = app(\App\Repositories\Basket\BasketRepository::class);

            $view->with('basket_count', $repository->getItemCount());

        });
    }

    public function composeBreadcrumb()
    {
        view()->composer('front.partials.breadcrumb', function ($view) {

            $view->with('breadcrumbs', Breadcrumb::all());

        });
    }

    public function composeComparisonCount(){

        view()->composer('front.partials.compare', function ($view) {

            /** @var BasketRepository $repository */
            $repository = app(\App\Repositories\Comparison\ComparisonRepository::class);

            $view->with('comparison_count', $repository->getItemCount());

        });

    }

    public function composeEmailPassword()
    {
        view()->composer('emails.password', function ($view) {

            /** @var EmailRepository $repository */
            $repository = app(\App\Repositories\Email\EmailRepository::class);

            $email = $repository->getEmailByReference('password');

            $view->with('email', $email);

        });
    }

    public function composeFeaturedProducts()
    {
        view()->composer('front.welcome', function ($view) {

            $view->with('products', FeaturedProduct::all());

        });
    }

    public function composeFooterLinks()
    {
        view()->composer('front.partials.footer_links', function ($view) {
            
            $repository = app(PageRepository::class);

            $pages = $repository->getPageByPosition('footer')->whereLoose('active',1);

            $view->with('footerLinks', $pages);

        });
    }

    public function composeHeaderLinks()
    {
        view()->composer('front.partials.header_links', function ($view) {

            $repository = app(PageRepository::class);

            $pages = $repository->getPageByPosition('header')->whereLoose('active',1);

            $view->with('headerLinks', $pages);

        });
    }

    public function composeNavigation()
    {
        view()->composer('front.partials.navigation', function ($view) {

            /** @var CategoryRepository $repository */
            $repository = app(\App\Repositories\Category\CategoryRepository::class);

            $categories = $repository->getNestedCategories();

            $view->with('navigation', $categories);

        });
    }

    public function composePerPage(){

        view()->composer('front.category.partials.perPage', function ($view) {

            $perPage = Session::get('perPage');

            $paginationChoices = Config::get('app.paginationChoices');

            $perPage = empty($perPage) ? Config::get('app.pagination') : $perPage;

            $view->with('perPage', $perPage)->with('paginationChoices', $paginationChoices);

        });

    }

    public function composeProductOrder(){

        view()->composer('front.category.partials.productOrder', function ($view) {

            $productOrder = Session::get('productOrder');

            $orderingChoices = Config::get('app.orderingChoices');

            $productOrder = empty($productOrder) ? Config::get('app.productOrder') : $productOrder;

            $view->with('productOrder', $productOrder)->with('orderingChoices', $orderingChoices);

        });

    }

    public function composeSlideShow(){

        view()->composer('front.partials.slideShow', function ($view) {

            $view->with('slides', Slide::where('active',1)->orderBy('order','asc')->get());

        });

    }

    public function composeSpecificationTable()
    {
        view()->composer('front.product.partials.specificationTable', function ($view) {

            /** @var CategoryRepository $repository */
            $repository = app(\App\Repositories\Feature\FeatureRepository::class);

            $features = $repository->getNestedFeatures();

            $view->with('features', $features);

        });

    }

    public function composeTwitter()
    {
        view()->composer('front.partials.twitter', function ($view) {

            $website = Website::find(1);

            try{
                $tweets = Twitter::getUserTimeline(['screen_name' => $website->twitter, 'count' => 1, 'format' => 'object']);
            } catch (\Exception $e){
                $tweets = false;
            }

            $view->with('facebook',$website->facebook)->with('tweets', $tweets);

        });
    }

    public function composeVendorList()
    {
        view()->composer('front.category.partials.refine', function ($view) {

            /** @var CategoryRepository $repository */
            $repository = app(\App\Repositories\Product\ProductRepository::class);

            $vendors = $repository->getVendors();

            $view->with('vendors', $vendors);

        });
    }

    public function composeWebsite()
    {
        view()->composer('front.layouts.front', function ($view) {

            $view->with('website', Website::find(1));

        });

        view()->composer('front.page.contact', function ($view) {

            $view->with('website', Website::find(1));

        });

        view()->composer('front.page.basket', function ($view) {

            $view->with('website', Website::find(1));

        });

        view()->composer('front.ajax.terms', function ($view) {

            $view->with('website', Website::find(1));

        });
    }

}
