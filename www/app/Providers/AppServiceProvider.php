<?php

namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('date', function($param){
            return '<?php echo \App\Helpers\DateTime::displayDate' . $param. '; ?>';
        });

        Blade::directive('dateTime', function($param){
            return '<?php echo \App\Helpers\DateTime::displayDateTime' . $param. '; ?>';
        });

        Validator::extend('basket_validator', function ($attribute, $value, $parameters) {

            if (!is_array($value)) {
                return false;
            }

            foreach ($value as $row) {


                if(!isset($row['qty'])) {
                    return false;
                }

                if (!is_numeric($row['qty'])) {
                    return false;
                }
            }

            return true;

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
