<?php

namespace App\Console\Commands;

use App\Repositories\Import\ImportRepository;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class Inspire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the import';
    /**
     * @var ImportRepository
     */
    private $importRepository;

    /**
     * Inspire constructor.
     * @param ImportRepository $importRepository
     */
    public function __construct(ImportRepository $importRepository)
    {
        $this->importRepository = $importRepository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->importRepository->import();
        $this->warn("Import run");
    }
}
