<?php namespace App\Http\Controllers\Account;

use App\Helpers\Breadcrumb;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use App\Models\Website;
use App\Repositories\Basket\BasketRepository;
use App\Repositories\Email\EmailRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\User\UserRepository;
use Mail;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function index(BasketRepository $basketRepository)
    {
        Breadcrumb::add('Checkout',route('account.checkout'));

        $user = \Auth::user();

        return view('account.checkout')->with('basket', $basketRepository->getFullBasket())->with('user',$user);
    }

    /**
     * Confirm order
     *
     * @param BasketRepository $basketRepository
     * @param Requests\BasketDetailsUpdateRequest $basketDetailsUpdateRequest
     * @return mixed
     */
    public function confirm(BasketRepository $basketRepository, Requests\BasketDetailsUpdateRequest $basketDetailsUpdateRequest)
    {
        Breadcrumb::add('Checkout',route('account.checkout'));
        Breadcrumb::add('Confirm',route('account.checkout.confirm'));

        $user = \Auth::user();

        $basketRepository->updateBasketDetails($basketDetailsUpdateRequest);

        return view('account.confirm')->with('basket', $basketRepository->getFullBasket())->with('user',$user);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function process(OrderRepository $orderRepository, BasketRepository $basketRepository, EmailRepository $emailRepository, UserRepository $userRepository)
    {
        $emailReference = 'order';

        $order = $orderRepository->create($basketRepository);
        $email = $emailRepository->getEmailByReference($emailReference);
        $user = $userRepository->getUserById($order->user_id);
        $website = Website::find(1);

        Mail::send('emails.'. $emailReference ,['order' => $order, 'user'=>$user, 'email' => $email], function($m) use ($user, $email){
            $m->to($user->email, $user->first_name.' '.$user->last_name)->subject($email->subject);
        });

        Mail::send('emails.'. $emailReference ,['order' => $order, 'user'=>$user, 'email' => $email], function($m) use ($website, $order){
            $m->to($website->email)->subject('New ICT Service order: '.$order->id);
        });

        $basketRepository->emptyBasket();

        return redirect()->route('account.order.show', $order)->with('success', $website->order_text);
    }

}
