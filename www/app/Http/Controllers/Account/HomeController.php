<?php

namespace App\Http\Controllers\Account;

use App\Helpers\Breadcrumb;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::add('Account',route('account.home'));

        $user = \Auth::user();

        return view('account.home')->with('user',$user);
    }
}
