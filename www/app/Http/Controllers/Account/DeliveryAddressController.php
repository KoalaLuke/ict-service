<?php namespace App\Http\Controllers\Account;

use App\Helpers\Breadcrumb;
use App\Http\Requests\DeliveryAddressCreateRequest;
use App\Http\Requests\DeliveryAddressUpdateRequest;
use App\Models\DeliveryAddress;
use App\Repositories\DeliveryAddress\DeliveryAddressRepository;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Session;
use URL;

class DeliveryAddressController extends Controller
{
    /**
     * @var DeliveryAddressRepository
     */
    private $deliveryAddressRepository;

    /**
     * DeliveryAddressControlller constructor.
     * @param DeliveryAddressRepository $deliveryAddressRepository
     */
    public function __construct(
        DeliveryAddressRepository $deliveryAddressRepository
    )
    {
        Breadcrumb::add('Account',route('account.home'));
        Breadcrumb::add('Delivery Addresses',route('account.deliveries.index'));

        $this->deliveryAddressRepository = $deliveryAddressRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return view('account.delivery.index')->with('user',$user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $previous = URL::previous();

        Breadcrumb::add('Add Delivery Address',route('account.deliveries.create'));

        $user = Auth::user();

        return view('account.delivery.create')->with('user',$user)->with('previous',$previous);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryAddressCreateRequest $deliveryAddressCreateRequest)
    {
        $deliveryAddress = $this->deliveryAddressRepository->create($deliveryAddressCreateRequest);

        if(Input::get('previous') == route('account.checkout')){

            Session::put('basket.same_address', 0);
            Session::put('basket.delivery_address', $deliveryAddress->id);

            return redirect()->route('account.checkout');

        } else {

            return redirect()->route('account.deliveries.edit', $deliveryAddress)->with('success', '<p>Delivery address created.</p>');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliveryAddress $deliveryAddress)
    {
        $previous = URL::previous();

        Breadcrumb::add('Update Delivery Address',route('account.deliveries.edit',$deliveryAddress->id));

        return view('account.delivery.edit')->with('deliveryAddress',$deliveryAddress)->with('previous',$previous);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DeliveryAddress $deliveryAddress, DeliveryAddressUpdateRequest $deliveryAddressUpdateRequest)
    {
        $this->deliveryAddressRepository->update($deliveryAddress, $deliveryAddressUpdateRequest);

        return redirect()->route('account.deliveries.edit', $deliveryAddress)->with('success', '<p>Delivery address updated</p>');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliveryAddress $deliveryAddress)
    {
        $this->deliveryAddressRepository->delete($deliveryAddress);

        return redirect('/account/deliveries');
    }
}
