<?php namespace App\Http\Controllers\Account;

use App\Helpers\Breadcrumb;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\User\UserRepository;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function details()
    {
        Breadcrumb::add('Account',route('account.home'));
        Breadcrumb::add('Account Details',route('account.details'));

        $user = $this->userRepository->authorized();

        return view('account.details')->with('user',$user);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function update(UserUpdateRequest $userUpdateRequest)
    {
        $user = $this->userRepository->authorized();

        $this->userRepository->update($user, $userUpdateRequest);

        return redirect()->route('account.details', $user)->with('success', '<p>Changes have been made.</p>');
    }
}
