<?php namespace App\Http\Controllers\Account;

use App\Helpers\Breadcrumb;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Order;
use App\Repositories\Order\OrderRepository;

class OrderController extends Controller
{
    private $orderRepository;

    /**
     * OrderController constructor.
     */
    public function __construct(
        OrderRepository $orderRepository
    )
    {
        $this->orderRepository = $orderRepository;
        Breadcrumb::add('Account',route('account.home'));
        Breadcrumb::add('Orders',route('account.order.index'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Orders';

        $user = \Auth::user();

        $orders = $user->orders;

        return view('account.orders')->with('orders',$orders)->with('meta',$meta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        Breadcrumb::add('Order',route('account.order.show', $order->id));

        $meta['title'] = 'Orders';

        return view('account.order')->with('order',$order)->with('meta',$meta);
    }
}
