<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailUpdateRequest;
use App\Models\Email;
use App\Repositories\Email\EmailRepository;

class EmailController extends Controller
{
    /**
     * @var EmailRepository
     */
    private $emailRepository;

    /**
     * EmailController constructor.
     * @param EmailRepository $emailRepository
     */
    public function __construct(
        EmailRepository $emailRepository
    )
    {
        $this->emailRepository = $emailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Emails';

        $emails = \App\Models\Email::all();

        return view('admin.emails.index')->with('emails',$emails)->with('meta',$meta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Email $email)
    {
        $meta['title'] = 'Emails';

        return view('admin.emails.edit')->with('email',$email)->with('meta',$meta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Email $email
     * @param EmailUpdateRequest $emailUpdateRequest
     * @return Response
     * @internal param int $id
     */
    public function update(Email $email, EmailUpdateRequest $emailUpdateRequest)
    {
        $meta['title'] = 'Emails';

        $this->emailRepository->update($email, $emailUpdateRequest);

        return redirect()->route('admin.email.edit', $email)->with('success', 'Email updated');
    }
}
