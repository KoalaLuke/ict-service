<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Message;
use App\Repositories\Message\MessageRepository;

class MessageController extends Controller
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * MessageController constructor.
     * @param MessageRepository $messageRepository
     */
    public function __construct(
        MessageRepository $messageRepository
    )
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Messages';

        $messages = Message::all();

        return view('admin.messages.index')->with('messages',$messages)->with('meta',$meta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        $meta['title'] = 'Messages';

        return view('admin.messages.show')->with('message',$message)->with('meta',$meta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        $this->messageRepository->delete($message);

        return redirect('/admin/message');
    }
}
