<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageCreateRequest;
use App\Http\Requests\PageUpdateRequest;
use App\Models\Page;
use App\Repositories\Page\PageRepository;

class PageController extends Controller
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * PageController constructor.
     * @param PageRepository $pageRepository
     */
    public function __construct(
        PageRepository $pageRepository
    )
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $meta['title'] = 'Pages';

        $header_pages = $this->pageRepository->getPageByPosition('header');
        $footer_pages = $this->pageRepository->getPageByPosition('footer');
        $sitemap_pages = $this->pageRepository->getPageByPosition('sitemap');

        return view('admin.pages.index')
            ->with('header_pages',$header_pages)
            ->with('footer_pages',$footer_pages)
            ->with('sitemap_pages',$sitemap_pages)
            ->with('meta',$meta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $meta['title'] = 'Pages';

        return view('admin.pages.create')->with('meta',$meta);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PageCreateRequest $pageCreateRequest
     * @return Response
     */
    public function store(PageCreateRequest $pageCreateRequest)
    {
        $meta['title'] = 'Pages';

        $page = $this->pageRepository->create($pageCreateRequest);

        return redirect()->route('admin.page.edit', $page)->with('success', 'Page created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Page $page)
    {
        $meta['title'] = 'Pages';

        return view('admin.pages.edit')->with('page',$page)->with('meta',$meta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Page $page
     * @param PageUpdateRequest $pageUpdateRequest
     * @return Response
     * @internal param int $id
     */
    public function update(Page $page, PageUpdateRequest $pageUpdateRequest)
    {
        $meta['title'] = 'Pages';

        $this->pageRepository->update($page, $pageUpdateRequest);

        return redirect()->route('admin.page.edit', $page)->with('success', 'Page updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Page $page)
    {
        $this->pageRepository->delete($page);

        return redirect('/admin/page');
    }
}
