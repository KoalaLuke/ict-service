<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AccreditationCreateRequest;
use App\Http\Requests\AccreditationUpdateRequest;
use App\Models\Accreditation;
use App\Repositories\Accreditation\AccreditationRepository;

class AccreditationController extends Controller
{
    /**
     * @var AccreditationRepository
     */
    private $accreditationRepository;

    /**
     * AccreditationController constructor.
     * @param AccreditationRepository $accreditationRepository
     */
    public function __construct(
        AccreditationRepository $accreditationRepository
    )
    {
        $this->accreditationRepository = $accreditationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Accreditations';

        $accreditations = \App\Models\Accreditation::all();

        return view('admin.accreditations.index')->with('accreditations',$accreditations)->with('meta',$meta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta['title'] = 'Accreditations';

        $images = \App\Models\Image::lists('name', 'id');

        return view('admin.accreditations.create')->with('meta',$meta)->with('images',$images);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccreditationCreateRequest $accreditationCreateRequest)
    {
        $meta['title'] = 'Accreditations';

        $accreditation = $this->accreditationRepository->create($accreditationCreateRequest);

        return redirect()->route('admin.accreditation.edit', $accreditation)->with('success', 'Accreditation created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Accreditation $accreditation)
    {
        $meta['title'] = 'Accreditations';

        $images = \App\Models\Image::lists('name', 'id');

        return view('admin.accreditations.edit')->with('accreditation',$accreditation)->with('meta',$meta)->with('images',$images);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Accreditation $accreditation
     * @param AccreditationUpdateRequest $accreditationUpdateRequest
     * @return Response
     * @internal param int $id
     */
    public function update(Accreditation $accreditation, AccreditationUpdateRequest $accreditationUpdateRequest)
    {
        $meta['title'] = 'Accreditations';

        $this->accreditationRepository->update($accreditation, $accreditationUpdateRequest);

        return redirect()->route('admin.accreditation.edit', $accreditation)->with('success', 'Accreditation updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Accreditation $accreditation)
    {
        $this->accreditationRepository->delete($accreditation);

        return redirect('/admin/accreditation');
    }
}
