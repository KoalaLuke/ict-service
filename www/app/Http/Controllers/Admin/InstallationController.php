<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\InstallationCreateRequest;
use App\Models\Installation;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Installation\InstallationRepository;

class InstallationController extends Controller
{
    /**
     * @var InstallationRepository
     * @var CategoryRepository
     */
    private $installationRepository;
    private $categoryRepository;

    /**
     * InstallationController constructor.
     * @param InstallationRepository $installationRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(
        InstallationRepository $installationRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->installationRepository = $installationRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstallationCreateRequest $installationCreateRequest)
    {
        $meta['title'] = 'Installations';

        $icts_productcategoryid = $installationCreateRequest->icts_productcategoryid;

        $category = $this->categoryRepository->getCategoryByVendorId($icts_productcategoryid);

        $this->installationRepository->create($installationCreateRequest);

        return redirect()->route('admin.category.edit', $category)->with('success', 'Installation created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Installation $installation)
    {
        $category_id = $installation->category->id;

        $this->installationRepository->delete($installation);

        return redirect('/admin/category/'.$category_id.'/edit');
    }
}
