<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\OrderUpdateRequest;
use App\Models\Order;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Orders';

        $orders = Order::orderBy('created_at','DESC')->get();

        return view('admin.orders.index')->with('orders',$orders)->with('meta',$meta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $meta['title'] = 'Orders';

        return view('admin.orders.show')->with('order',$order)->with('meta',$meta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $meta['title'] = 'Orders';

        return view('admin.orders.edit')->with('order',$order)->with('meta',$meta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Order $order, OrderUpdateRequest $orderUpdateRequest)
    {
        $meta['title'] = 'Orders';

        $this->orderRepository->update($order, $orderUpdateRequest);

        return redirect()->route('admin.order.edit', $order)->with('success', 'Order updated');
    }

    /**
     * Export orders
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function export(){

        return Order::all();

    }

    /**
     * Search orders
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function search(){

        $meta['title'] = 'Orders';

        $order = Order::find(Input::get('order_number'));

        if($order){

            return redirect()->route('admin.order.show',$order->id)->with('order',$order)->with('meta',$meta);

        } else {

            $orders = [];

            return redirect()->route('admin.order.index')->with('errors',['No order with that order number.']);

        }

    }

}
