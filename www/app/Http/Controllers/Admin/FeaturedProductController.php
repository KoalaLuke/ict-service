<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\FeaturedProductCreateRequest;
use App\Models\FeaturedProduct;
use App\Models\Product;
use App\Repositories\FeaturedProduct\FeaturedProductRepository;
use Illuminate\Http\Request;

class FeaturedProductController extends Controller
{
    /**
     * @var FeaturedProductRepository
     */
    private $featuredProductRepository;

    /**
     * FeaturedProductController constructor.
     */
    public function __construct(FeaturedProductRepository $featuredProductRepository)
    {
        $this->featuredProductRepository = $featuredProductRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Featured Products';

        $products = FeaturedProduct::all();

        $all_products = [''=>'Please select a product'] + Product::lists('name', 'productid')->toArray();
        
        return view('admin.featuredProducts.index')->with('all_products',$all_products)->with('products',$products)->with('meta',$meta);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeaturedProductCreateRequest $featuredProductCreateRequest)
    {
        $this->featuredProductRepository->create($featuredProductCreateRequest);

        return redirect('/admin/featured-product')->with('success', 'Installation created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeaturedProduct $featuredProduct)
    {
        $featuredProduct->delete();

        return redirect('/admin/featured-product');
    }
}
