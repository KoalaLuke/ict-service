<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function index()
    {
        $meta['title'] = 'Welcome';

        return view('admin.welcome')->with('meta',$meta);
    }
    
}