<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAdminUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use App\Repositories\User\UserRepository;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Users';

        $users = \App\Models\User::all();

        return view('admin.users.index')->with('users',$users)->with('meta',$meta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $meta['title'] = 'Users';

        return view('admin.users.edit')->with('user',$user)->with('meta',$meta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param User $user
     * @param UserUpdateRequest $userUpdateRequest
     * @return Response
     * @internal param int $id
     */
    public function update(User $user, UserAdminUpdateRequest $userAdminUpdateRequest)
    {
        $meta['title'] = 'Users';

        $this->userRepository->switchAdminRights($user, $userAdminUpdateRequest);

        return redirect()->route('admin.user.edit', $user)->with('success', 'User updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(User $user)
    {
        $this->userRepository->delete($user);

        return redirect('/admin/user');
    }
}
