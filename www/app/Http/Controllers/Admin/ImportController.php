<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Import;
use App\Repositories\Import\ImportRepository;

class ImportController extends Controller
{
    /**
     * @var ImportRepository
     */
    private $importRepository;

    /**
     * ImportRepository constructor.
     * @param ImportRepository $importRepository
     */
    public function __construct(ImportRepository $importRepository)
    {
        $this->importRepository = $importRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Import';

        $latestImport = Import::orderBy('created_at', 'desc')->first();

        return view('admin.import.index')->with('meta',$meta)->with('latestImport',$latestImport);
    }

    public function update()
    {
        $this->importRepository->import();

        return redirect()->route('admin.import.index');
    }

}
