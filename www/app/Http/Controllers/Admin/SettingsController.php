<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Website;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        $meta['title'] = 'Settings';

        $website = Website::first();

        return view('admin.settings.edit')->with('website',$website)->with('meta',$meta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $meta['title'] = 'Settings';

        $website = Website::first();

        $website->update($request->all());

        return redirect()->route('admin.settings.edit', $website)->with('success', 'Settings updated');
    }
    
}