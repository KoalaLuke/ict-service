<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DiscountCreateRequest;
use App\Http\Requests\DiscountUpdateRequest;
use App\Models\Discount;
use App\Repositories\Discount\DiscountRepository;

class DiscountController extends Controller
{
    /**
     * @var DiscountRepository
     */
    private $discountRepository;

    /**
     * DiscountController constructor.
     * @param DiscountRepository $discountRepository
     */
    public function __construct(
        DiscountRepository $discountRepository
    )
    {
        $this->discountRepository = $discountRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Discounts';

        $discounts = \App\Models\Discount::all();

        return view('admin.discounts.index')->with('discounts',$discounts)->with('meta',$meta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta['title'] = 'Discounts';

        return view('admin.discounts.create')->with('meta',$meta);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiscountCreateRequest $discountCreateRequest)
    {
        $meta['title'] = 'Discounts';

        $discount = $this->discountRepository->create($discountCreateRequest);

        return redirect()->route('admin.discount.edit', $discount)->with('success', 'Discount created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount)
    {
        $meta['title'] = 'Discounts';

        return view('admin.discounts.edit')->with('discount',$discount)->with('meta',$meta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Discount $discount
     * @param DiscountUpdateRequest $discountUpdateRequest
     * @return Response
     * @internal param int $id
     */
    public function update(Discount $discount, DiscountUpdateRequest $discountUpdateRequest)
    {
        $meta['title'] = 'Discounts';

        $this->discountRepository->update($discount, $discountUpdateRequest);

        return redirect()->route('admin.discount.edit', $discount)->with('success', 'Discount updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discount $discount)
    {
        $this->discountRepository->delete($discount);

        return redirect('/admin/discount');
    }
}
