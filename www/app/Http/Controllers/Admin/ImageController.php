<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImageCreateRequest;
use App\Http\Requests\ImageUpdateRequest;
use App\Models\Image;
use App\Repositories\Image\ImageRepository;

class ImageController extends Controller
{
    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * ImageController constructor.
     * @param ImageRepository $imageRepository
     */
    public function __construct(
        ImageRepository $imageRepository
    )
    {
        $this->imageRepository = $imageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Images';

        $images = \App\Models\Image::all();

        return view('admin.images.index')->with('images',$images)->with('meta',$meta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta['title'] = 'Images';

        return view('admin.images.create')->with('meta',$meta);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageCreateRequest $imageCreateRequest)
    {
        $meta['title'] = 'Images';

        $image = $this->imageRepository->create($imageCreateRequest);

        return redirect()->route('admin.image.edit', $image)->with('success', 'Image created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        $meta['title'] = 'Images';

        return view('admin.images.edit')->with('image',$image)->with('meta',$meta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Image $image
     * @param ImageUpdateRequest $imageUpdateRequest
     * @return Response
     * @internal param int $id
     */
    public function update(Image $image, ImageUpdateRequest $imageUpdateRequest)
    {
        $meta['title'] = 'Images';

        $this->imageRepository->update($image, $imageUpdateRequest);

        return redirect()->route('admin.image.edit', $image)->with('success', 'Image updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $this->imageRepository->delete($image);

        return redirect('/admin/image');
    }
}
