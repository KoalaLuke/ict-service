<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SlideCreateRequest;
use App\Http\Requests\SlideUpdateRequest;
use App\Models\Slide;
use App\Repositories\Slide\SlideRepository;

class SlideController extends Controller
{
    /**
     * @var SlideRepository
     */
    private $slideRepository;

    /**
     * SlideController constructor.
     * @param SlideRepository $slideRepository
     */
    public function __construct(
        SlideRepository $slideRepository
    )
    {
        $this->slideRepository = $slideRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Slides';

        $slides = \App\Models\Slide::all();

        return view('admin.slides.index')->with('slides',$slides)->with('meta',$meta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta['title'] = 'Slides';

        $images = \App\Models\Image::lists('name', 'id');

        return view('admin.slides.create')->with('meta',$meta)->with('images',$images);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SlideCreateRequest $slideCreateRequest)
    {
        $meta['title'] = 'Slides';

        $slide = $this->slideRepository->create($slideCreateRequest);

        return redirect()->route('admin.slide.edit', $slide)->with('success', 'Slide created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slide $slide)
    {
        $meta['title'] = 'Slides';

        $images = \App\Models\Image::lists('name', 'id');

        return view('admin.slides.edit')->with('slide',$slide)->with('meta',$meta)->with('images',$images);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Slide $slide
     * @param SlideUpdateRequest $slideUpdateRequest
     * @return Response
     * @internal param int $id
     */
    public function update(Slide $slide, SlideUpdateRequest $slideUpdateRequest)
    {
        $meta['title'] = 'Slides';

        $this->slideRepository->update($slide, $slideUpdateRequest);

        return redirect()->route('admin.slide.edit', $slide)->with('success', 'Slide updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slide $slide)
    {
        $this->slideRepository->delete($slide);

        return redirect('/admin/slide');
    }
}
