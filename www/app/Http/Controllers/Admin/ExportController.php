<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

class ExportController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($table)
    {
        $name = str_singular(studly_case($table));
        $model = 'App\Models\\'.$name;
        if(class_exists($model)){
            $class =  new $model();
            return $class->all();
        } else {
            return 'No table';
        }
    }
}
