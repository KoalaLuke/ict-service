<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ImageController constructor.
     * @param CategoryRepository $categoryRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        ProductRepository $productRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Categories';

        $categories = $this->categoryRepository->getNestedCategories();

        return view('admin.categories.index')->with('categories',$categories)->with('meta',$meta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $meta['title'] = $category->icts_name;

        $installationProductsSelect = $this->categoryRepository->getNewInstallations($category)->lists('name','productid');

        $installationProductsSelect->prepend('Please select');

        return view('admin.categories.edit')->with('category',$category)->with('installationProductsSelect',$installationProductsSelect)->with('meta',$meta);
    }

}
