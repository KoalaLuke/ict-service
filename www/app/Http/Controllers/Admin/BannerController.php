<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BannerCreateRequest;
use App\Http\Requests\BannerUpdateRequest;
use App\Models\Banner;
use App\Repositories\Banner\BannerRepository;

class BannerController extends Controller
{
    /**
     * @var BannerRepository
     */
    private $bannerRepository;

    /**
     * BannerController constructor.
     * @param BannerRepository $bannerRepository
     */
    public function __construct(
        BannerRepository $bannerRepository
    )
    {
        $this->bannerRepository = $bannerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta['title'] = 'Banners';

        $banners = Banner::orderBy('order','asc')->get();

        return view('admin.banners.index')->with('banners',$banners)->with('meta',$meta);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meta['title'] = 'Banners';

        $images = \App\Models\Image::lists('name', 'id');

        return view('admin.banners.create')->with('meta',$meta)->with('images',$images);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerCreateRequest $bannerCreateRequest)
    {
        $meta['title'] = 'Banners';

        $banner = $this->bannerRepository->create($bannerCreateRequest);

        return redirect()->route('admin.banner.edit', $banner)->with('success', 'Banner created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        $meta['title'] = 'Banners';

        $images = \App\Models\Image::lists('name', 'id');

        return view('admin.banners.edit')->with('banner',$banner)->with('meta',$meta)->with('images',$images);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Banner $banner
     * @param BannerUpdateRequest $bannerUpdateRequest
     * @return Response
     * @internal param int $id
     */
    public function update(Banner $banner, BannerUpdateRequest $bannerUpdateRequest)
    {
        $meta['title'] = 'Banners';

        $this->bannerRepository->update($banner, $bannerUpdateRequest);

        return redirect()->route('admin.banner.edit', $banner)->with('success', 'Banner updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        $this->bannerRepository->delete($banner);

        return redirect('/admin/banner');
    }
}
