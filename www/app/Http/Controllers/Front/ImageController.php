<?php namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Product;

class ImageController extends Controller
{

    /**
     * Show an image
     *
     * @param Image $image
     * @param string $width
     * @param string $height
     * @return mixed
     */
    public function index(Image $image, $width = '0', $height = '0')
    {
        $key = 'image_' . $image->id . '_' . $width . '_' . $height;

        return \Cache::remember($key, 60 * 24 * 7, function() use ($image, $width, $height){

            $img = \Intervention\Image\ImageManagerStatic::make('images/uploads/'.$image->file);

            if($width !== '0' && $height !== '0'){

                $img->fit($width, $height);

            } elseif($width !== '0'  && $height === '0') {

                $img->widen($width);

            } elseif($width === '0' && $height !== '0') {

                $img->heighten($height);

            }

            return $img->response();

        });
    }


    /**
     * Show a product image
     *
     * @param $product_id
     * @param string $width
     * @param string $height
     * @return mixed
     */
    public function product($product_id, $width = '0', $height = '0')
    {

        $key = 'product_' . $product_id . '_' . $width . '_' . $height;

        return \Cache::remember($key, 60 * 24 * 7, function() use ($product_id, $width, $height){

            $product = Product::find($product_id);

            $image_file = public_path().'/images/placeholder.png';

            if($product && $product->edict_imageurl){

                if ($data = @file_get_contents($product->edict_imageurl)) {
                    $image_file = $data;
                }

            }

            $img = \Intervention\Image\ImageManagerStatic::make($image_file);

            if($width !== '0' && $height !== '0'){

                $img->fit($width, $height);

            } elseif($width !== '0'  && $height === '0') {

                $img->widen($width);

            } elseif($width === '0' && $height !== '0') {

                $img->heighten($height);

            }

            return $img->response();

        });

    }

}