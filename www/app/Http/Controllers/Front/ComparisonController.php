<?php namespace App\Http\Controllers\Front;

use App\Helpers\Breadcrumb;
use App\Http\Controllers\Controller;
use App\Http\Requests\ComparisonAddRequest;
use App\Models\Product;
use App\Repositories\Comparison\ComparisonRepository;

class ComparisonController extends Controller
{
    /**
     * @var ComparisonRepository
     */
    private $comparisonRepository;

    /**
     * ComparisonController constructor.
     */
    public function __construct(ComparisonRepository $comparisonRepository)
    {
        $this->comparisonRepository = $comparisonRepository;
    }

    /**
     * Show comparison page
     *
     * @param ComparisonRepository $comparisonRepository
     * @return $this
     */
    public function show(ComparisonRepository $comparisonRepository)
    {
        Breadcrumb::add('Comparison',route('front.comparison.show'));

        return view('front.page.comparison')->with('comparison', $comparisonRepository->getFullComparison());
    }

    /**
     * Print comparison
     *
     * @param ComparisonRepository $comparisonRepository
     * @return $this
     */
    public function printable(ComparisonRepository $comparisonRepository)
    {
        Breadcrumb::add('Comparison',route('front.comparison.show'));

        return view('front.page.comparisonPrintable')->with('comparison', $comparisonRepository->getFullComparison());
    }


    /**
     * Add a product to the comparison
     *
     * @param ComparisonAddRequest $comparisonAddRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(ComparisonAddRequest $comparisonAddRequest)
    {

        $product = Product::find($comparisonAddRequest->id);

        if($product) {

            $basket_count = $this->comparisonRepository->updateItem($comparisonAddRequest);
            $message = 'Comparison updated.';
            $success = true;
            $response = 200;

        } else {

            $basket_count = $this->comparisonRepository->getItemCount();
            $message = 'Product does not exist';
            $success = false;
            $response = 400;

        }

        return response()->json([
            'message' => $message,
            'count' => $basket_count,
            'success' => $success,
            'product' => $product->id
        ], $response);

    }

}
