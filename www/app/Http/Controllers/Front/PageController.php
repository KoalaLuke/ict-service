<?php namespace App\Http\Controllers\Front;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Website;
use App\Repositories\Page\PageRepository;

class PageController extends Controller
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * PageController constructor.
     * @param PageRepository $pageRepository
     */
    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  Page $page
     * @return Response
     */
    public function show(Page $page)
    {
        $this->pageRepository->breadcrumbs($page);

        return view('front.page.show')->with('page',$page);
    }

    /**
     * Terms
     *
     * @return Response
     */
    public function terms()
    {
        return view('front.ajax.terms');
    }



}
