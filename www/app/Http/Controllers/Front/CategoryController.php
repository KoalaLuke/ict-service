<?php namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\PerPageRequest;
use App\Http\Requests\ProductOrderRequest;
use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use Config;
use Illuminate\Support\Facades\App;
use Session;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * CategoryController constructor.
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  Category $product
     * @return Response
     */
    public function show(Category $category)
    {
        if($category->statecode == 1) {
            App::abort(404);
        }

        if(empty($category->icts_parentcategory)){
            $products = $this->categoryRepository->getProductsChildren($category);
        } else {
            $products = $category->products();
        }

        $perPage = Session::get('perPage');

        $perPage = empty($perPage) ? Config::get('app.pagination') : $perPage;

        $productOrder = Session::get('productOrder');

        $productOrder = empty($productOrder) ? Config::get('app.productOrder') : $productOrder;

        $productOrderArray = explode('-',$productOrder);

        if(count($productOrderArray)==2){

            $products->orderBy($productOrderArray[0],$productOrderArray[1]);
        }

        $products = $products->paginate($perPage);

        $this->categoryRepository->breadcrumbs($category);

        return view('front.category.show')->with(['category' => $category, 'products' => $products]);
    }

    /**
     * Save items per page cookie
     *
     * @param PerPageRequest $perPageRequest
     * @return mixed
     */
    public function perPage(PerPageRequest $perPageRequest)
    {
        Session::put('perPage',$perPageRequest->quantity);

        return $perPageRequest->quantity;
    }

    /**
     * Save product order cookie
     *
     * @param ProductOrderRequest $productOrderRequest
     * @return mixed
     */
    public function productOrder(ProductOrderRequest $productOrderRequest)
    {
        Session::put('productOrder',$productOrderRequest->sort);

        return $productOrderRequest->sort;
    }
}
