<?php namespace App\Http\Controllers\Front;

use App\Helpers\Breadcrumb;
use App\Http\Controllers\Controller;
use App\Http\Requests\ApplyDiscountRequest;
use App\Http\Requests\BasketAddRequest;
use App\Http\Requests\BasketUpdateRequest;
use App\Models\Product;
use App\Repositories\Basket\BasketRepository;

class BasketController extends Controller
{
    /**
     * @var BasketRepository
     */
    private $basketRepository;

    /**
     * BasketController constructor.
     * @param BasketRepository $basketRepository
     */
    public function __construct(BasketRepository $basketRepository)
    {
        $this->basketRepository = $basketRepository;
    }

    /**
     * Show basket
     *
     * @param BasketRepository $basketRepository
     * @return $this
     */
    public function show(BasketRepository $basketRepository)
    {
        Breadcrumb::add('Basket',route('front.basket.show'));

        return view('front.page.basket')->with('basket', $basketRepository->getFullBasket());
    }

    /**
     * Add basket
     *
     * @param BasketAddRequest $basketAddRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(BasketAddRequest $basketAddRequest)
    {

        $product = Product::find($basketAddRequest->id);

        if($product) {

            $basket_count = $this->basketRepository->addItem($basketAddRequest);
            $message = 'Product added';
            $success = true;
            $response = 200;

        } else {

            $basket_count = $this->basketRepository->getItemCount();
            $message = 'Product does not exist';
            $success = false;
            $response = 400;

        }

        return response()->json([
            'message' => $message,
            'count' => $basket_count,
            'success' => $success,
        ], $response);

    }

    /**
     * Update basket
     *
     * @param BasketUpdateRequest $basketUpdateRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(BasketUpdateRequest $basketUpdateRequest)
    {
        $this->basketRepository->updateBasket($basketUpdateRequest);

        return redirect()->route('front.basket.show');
    }

    /**
     * Discount
     *
     * @param ApplyDiscountRequest $applyDiscountRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function discount(ApplyDiscountRequest $applyDiscountRequest)
    {
        $this->basketRepository->applyDiscountCode($applyDiscountRequest);

        return redirect()->route('front.basket.show');
    }

    /**
     * Clear Basket
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clear()
    {
        $this->basketRepository->clearDiscountCode();

        return redirect()->route('front.basket.show');
    }
}