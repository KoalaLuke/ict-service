<?php namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    /**
     * Home page
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('front.welcome');
    }

}