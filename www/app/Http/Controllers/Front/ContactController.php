<?php namespace App\Http\Controllers\Front;

use App\Helpers\Breadcrumb;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessageCreateRequest;
use App\Http\Requests;
use App\Models\Website;
use App\Repositories\Message\MessageRepository;
use Mail;

class ContactController extends Controller
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * PageController constructor.
     * @param MessageRepository $messageRepository
     */
    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * Contact page
     *
     * @return $this
     */
    public function index()
    {
        Breadcrumb::add('Contact',route('front.contact.show'));

        return view('front.page.contact');
    }

    /**
     * Send message
     *
     * @param MessageCreateRequest $messageCreateRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send(MessageCreateRequest $messageCreateRequest)
    {
        $message = $this->messageRepository->create($messageCreateRequest);
        $website = Website::find(1);

//        dd($message);
        Mail::send('emails.message',['msg' => $message], function($m) use ($website){
            $m->to($website->email)->subject('New ICT Message');
        });

        return redirect()->route('front.contact.show')->with('success', '<p>Message sent</p>');
    }
}
