<?php

namespace App\Http\Controllers\Front;

use App\Helpers\Breadcrumb;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\Input;
use Searchy;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::add('Search',route('front.search'));

        $term = Input::get('term');

        $results = Searchy::driver('simple')->products('name','description')->query($term)->get();

        if(!empty($results)) {
            $products = Product::hydrate($results);
        } else {
            $products = collect([]);
        }

        return view('front.page.search')->with('products',$products)->with('term', $term);
    }

}
