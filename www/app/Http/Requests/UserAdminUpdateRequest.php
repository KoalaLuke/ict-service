<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Repositories\User\UserRepository;

class UserAdminUpdateRequest extends Request
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserUpdateRequest constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'admin' => 'required',
        ];
    }
}
