<?php

namespace App\Http\Middleware;

use App\Repositories\User\UserRepository;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class Administrate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     * @param UserRepository $userRepository
     */
    public function __construct(Guard $auth, UserRepository $userRepository)
    {
        $this->auth = $auth;
        $this->userRepository = $userRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->userRepository->isAdmin()) {
            return redirect()->guest('auth/login');
        } else {
            return $next($request);
        }
    }
}
