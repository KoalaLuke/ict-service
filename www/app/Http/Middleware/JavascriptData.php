<?php

namespace App\Http\Middleware;

use Closure;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;

class JavascriptData
{
    /**
     * JavascriptData constructor.
     * @param JavaScriptFacade $javaScriptFacade
     */
    public function __construct(JavaScriptFacade $javaScriptFacade)
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \JavaScript::put([
            'routes' => [
                'basket_add' => route('front.basket.add'),
                'comparison_add' => route('front.comparison.add'),
                'per_page' => route('front.category.perPage'),
                'product_order' => route('front.category.productOrder')
            ],
            'token' => csrf_token()
        ]);

        return $next($request);
    }
}
