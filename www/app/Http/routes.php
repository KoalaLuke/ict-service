<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

App::bind(\App\Repositories\Accreditation\AccreditationRepository::class, \App\Repositories\Accreditation\EloquentAccreditationRepository::class);
App::bind(\App\Repositories\Basket\BasketRepository::class, \App\Repositories\Basket\SessionBasketRepository::class);
App::bind(\App\Repositories\Banner\BannerRepository::class, \App\Repositories\Banner\EloquentBannerRepository::class);
App::bind(\App\Repositories\Category\CategoryRepository::class, \App\Repositories\Category\EloquentCategoryRepository::class);
App::bind(\App\Repositories\Comparison\ComparisonRepository::class, \App\Repositories\Comparison\SessionComparisonRepository::class);
App::bind(\App\Repositories\DeliveryAddress\DeliveryAddressRepository::class, \App\Repositories\DeliveryAddress\EloquentDeliveryAddressRepository::class);
App::bind(\App\Repositories\Discount\DiscountRepository::class, \App\Repositories\Discount\EloquentDiscountRepository::class);
App::bind(\App\Repositories\Email\EmailRepository::class, \App\Repositories\Email\EloquentEmailRepository::class);
App::bind(\App\Repositories\Feature\FeatureRepository::class, \App\Repositories\Feature\EloquentFeatureRepository::class);
App::bind(\App\Repositories\FeaturedProduct\FeaturedProductRepository::class, \App\Repositories\FeaturedProduct\EloquentFeaturedProductRepository::class);
App::bind(\App\Repositories\Image\ImageRepository::class, \App\Repositories\Image\EloquentImageRepository::class);
App::bind(\App\Repositories\Installation\InstallationRepository::class, \App\Repositories\Installation\EloquentInstallationRepository::class);
App::bind(\App\Repositories\Import\ImportRepository::class, \App\Repositories\Import\EloquentImportRepository::class);
App::bind(\App\Repositories\Message\MessageRepository::class, \App\Repositories\Message\EloquentMessageRepository::class);
App::bind(\App\Repositories\Order\OrderRepository::class, \App\Repositories\Order\EloquentOrderRepository::class);
App::bind(\App\Repositories\Page\PageRepository::class, \App\Repositories\Page\EloquentPageRepository::class);
App::bind(\App\Repositories\Product\ProductRepository::class, \App\Repositories\Product\EloquentProductRepository::class);
App::bind(\App\Repositories\Slide\SlideRepository::class, \App\Repositories\Slide\EloquentSlideRepository::class);
App::bind(\App\Repositories\Specification\SpecificationRepository::class, \App\Repositories\Specification\EloquentSpecificationRepository::class);
App::bind(\App\Repositories\User\UserRepository::class, \App\Repositories\User\EloquentUserRepository::class);

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function() {

    Route::get('/admin', ['uses'=>'AdminController@getLogin', 'as' => 'admin.getLogin']);
    Route::post('/admin', ['uses'=>'AdminController@postLogin', 'as' => 'admin.postLogin']);
    Route::get('/login', ['uses'=>'AuthController@getLogin', 'as' => 'auth.getLogin']);
    Route::post('/login', ['uses'=>'AuthController@postLogin', 'as' => 'auth.postLogin']);
    Route::get('/logout', ['uses'=>'AuthController@getLogout', 'as' => 'auth.getLogout']);
    Route::get('/password/email', ['uses'=>'PasswordController@getEmail', 'as' => 'auth.getEmail']);
    Route::post('/password/email', ['uses'=>'PasswordController@postEmail', 'as' => 'auth.postEmail']);
    Route::get('/password/reset/{token}', ['uses'=>'PasswordController@getReset', 'as' => 'auth.getReset']);
    Route::post('/password/reset', ['uses'=>'PasswordController@postReset', 'as' => 'auth.postReset']);
    Route::get('/register', ['uses'=>'AuthController@getRegister', 'as' => 'auth.getRegister']);
    Route::post('/register', ['uses'=>'AuthController@postRegister', 'as' => 'auth.postRegister']);

});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth.admin'], function(){

    Route::get('/', [ 'uses' => 'WelcomeController@index', 'as' => 'admin.welcome']);
    Route::get('/export/{exportTable}', ['uses'=>'ExportController@show','as'=>'admin.export.show']);
    Route::get('/settings', ['uses'=>'SettingsController@edit','as'=>'admin.settings.edit']);
    Route::post('/settings', ['uses'=>'SettingsController@update','as'=>'admin.settings.update']);
    Route::get('/import', ['uses'=>'ImportController@index','as'=>'admin.import.index']);
    Route::post('/import', ['uses'=>'ImportController@update','as'=>'admin.import.update']);
    Route::post('/order/search',['uses'=>'OrderController@search','as'=>'admin.order.search']);

    Route::resource('accreditation', 'AccreditationController', ['except' => ['show']]);
    Route::resource('banner', 'BannerController', ['except' => ['show']]);
    Route::resource('category', 'CategoryController', ['except' => ['show']]);
    Route::resource('discount', 'DiscountController', ['except' => ['show']]);
    Route::resource('email', 'EmailController', ['except' => ['create','store','destroy']]);
    Route::resource('featured-product', 'FeaturedProductController', ['except' => ['show']]);
    Route::resource('image', 'ImageController', ['except' => ['show']]);
    Route::resource('installation', 'InstallationController', ['except' => ['show']]);
    Route::resource('message', 'MessageController', ['except' => ['create','update','store','edit']]);
    Route::resource('order', 'OrderController', ['except' => ['create','store','destroy']]);
    Route::resource('page', 'PageController', ['except' => ['show']]);
    Route::resource('slide', 'SlideController', ['except' => ['show']]);
    Route::resource('user', 'UserController', ['except' => ['show','create','store']]);



});

Route::group(['prefix' => 'account', 'namespace' => 'Account', 'middleware' => 'auth'], function(){

    Route::get('/', [ 'uses' => 'HomeController@index', 'as' => 'account.home']);
    Route::post('/confirm', [ 'uses' => 'CheckoutController@confirm', 'as' => 'account.checkout.confirm']);
    Route::get('/checkout', [ 'uses' => 'CheckoutController@index', 'as' => 'account.checkout']);
    Route::get('/process', [ 'uses' => 'CheckoutController@process', 'as' => 'account.process']);
    Route::resource('/order', 'OrderController', ['except' => ['edit','update','create','store','destroy']]);
    Route::resource('/deliveries', 'DeliveryAddressController');
    Route::get('/details', [ 'uses' => 'UserController@details', 'as' => 'account.details']);
    Route::post('/details', [ 'uses' => 'UserController@update', 'as' => 'account.update']);

});

Route::group(['namespace'=>'Front'], function(){

    Route::get('/', ['uses' => 'WelcomeController@index','as'=>'front.home.show']);
    Route::get('/basket', ['uses'=>'BasketController@show','as'=>'front.basket.show']);
    Route::post('/basket/add', ['uses'=>'BasketController@add','as'=>'front.basket.add']);
    Route::post('/basket/update', ['uses'=>'BasketController@update','as'=>'front.basket.update']);
    Route::post('/basket/discount', ['uses'=>'BasketController@discount','as'=>'front.basket.discount']);
    Route::get('/basket/clear', ['uses'=>'BasketController@clear','as'=>'front.basket.clear']);
    Route::post('/category/perpage',['uses'=>'CategoryController@perPage','as'=>'front.category.perPage']);
    Route::post('/category/productorder',['uses'=>'CategoryController@productOrder','as'=>'front.category.productOrder']);
    Route::get('/comparison', ['uses'=>'ComparisonController@show','as'=>'front.comparison.show']);
    Route::post('/comparison/add', ['uses'=>'ComparisonController@add','as'=>'front.comparison.add']);
    Route::get('/comparison/printable', ['uses'=>'ComparisonController@printable','as'=>'front.comparison.printable']);
    Route::post('/comparison/update', ['uses'=>'ComparisonController@update','as'=>'front.comparison.update']);
    Route::get('/contact', ['uses'=>'ContactController@index','as'=>'front.contact.show']);
    Route::post('/contact', ['uses'=>'ContactController@send','as'=>'front.contact.send']);
    Route::get('/image/{image}/{width?}/{height?}', ['uses'=>'ImageController@index','as'=>'front.image']);
    Route::get('/page/{page_slug}', ['uses' => 'PageController@show', 'as' => 'front.page.show']);
    Route::get('/product/{product_slug}', ['uses' => 'ProductController@show', 'as' => 'front.product.show']);
    Route::get('/product_image/{product_id}/{width?}/{height?}', ['uses'=>'ImageController@product','as'=>'front.product.image']);
    Route::get('/search', ['uses'=>'SearchController@index','as'=>'front.search']);
    Route::get('/terms', ['uses'=>'PageController@terms','as'=>'front.terms']);
    Route::get('/{category_slug}',['uses' => 'CategoryController@show', 'as' => 'front.category.show']);

});

