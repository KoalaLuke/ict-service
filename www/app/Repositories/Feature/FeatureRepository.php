<?php namespace App\Repositories\Feature;


use App\Models\Feature;
use App\Models\Import;
use Illuminate\Database\Eloquent\Collection;

interface FeatureRepository
{
    /**
     * Get feature by its vendor id
     *
     * @param $vendorId
     * @return Feature
     */
    public function getFeatureByVendorId($vendorId);

    /**
     * Import features from a json file
     *
     * @param Import $import
     * @param $json
     * @return void
     */
    public function importFeatures(Import $import, $json);

    /**
     * Get top level features with children
     *
     * @return Collection
     */
    public function getNestedFeatures();
}