<?php namespace App\Repositories\Feature;

use App\Models\Feature;
use App\Models\Import;
use DB;
use Doctrine\Common\Collections\Collection;
use Illuminate\Database\QueryException;

class EloquentFeatureRepository implements FeatureRepository
{
    /**
     * @var Feature
     */
    private $feature;

    /**
     * EloquentFeatureRepository constructor.
     */
    public function __construct(Feature $feature)
    {
        $this->feature = $feature;
    }

    /**
     * Get feature by its vendor id
     *
     * @param $vendorId
     * @return Feature
     */
    public function getFeatureByVendorId($vendor_id)
    {
        return $this->feature->vendorId($vendor_id)->first();
    }

    /**
     * Import features from an json file
     *
     * @param Import $import
     * @param $json
     * @return void
     */
    public function importFeatures(Import $import, $json)
    {
        DB::transaction(function() use ($json, $import) {

            foreach($json as $featureObject){

                $attributes = $featureObject->Attributes;

                $vendorId = isset($attributes->icts_productfeatureid->Value) ? $attributes->icts_productfeatureid->Value : '';

                $feature = $this->getFeatureByVendorId($vendorId) ?: new Feature();

                $data = [

                    'statecode' => isset($attributes->statecode->Value) ? $attributes->statecode->Value : null ,
                    'icts_name' => isset($attributes->icts_name->Value) ? $attributes->icts_name->Value : null ,
                    'icts_comparisonmethod' =>  isset($attributes->icts_comparisonmethod->Value) ? $attributes->icts_comparisonmethod->Value : null ,
                    'icts_sortorder' => isset($attributes->icts_sortorder->Value) ? $attributes->icts_sortorder->Value : null ,
                    'icts_parentfeature' => isset($attributes->icts_parentfeature->Value) ? $attributes->icts_parentfeature->Value : null ,
                    'icts_description' => isset($attributes->icts_description->Value) ? $attributes->icts_description->Value : null ,
                    'modifiedon' => isset($attributes->modifiedon->Value) ? $attributes->modifiedon->Value : null ,
                    'icts_productfeatureid' => isset($attributes->icts_productfeatureid->Value) ? $attributes->icts_productfeatureid->Value : null ,

                ];

                $feature->fill($data);
                $feature->save();

            }

        });

    }

    /**
     * Get top level features with children
     *
     * @return Collection
     */
    public function getNestedFeatures(){
        return $this->feature
            ->whereNull('icts_parentfeature')
            ->with(['children'])
            ->orderBy('icts_name', 'ASC')
            ->get();
    }

}
