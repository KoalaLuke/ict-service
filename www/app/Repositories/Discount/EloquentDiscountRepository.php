<?php namespace App\Repositories\Discount;


use App\Http\Requests\DiscountCreateRequest;
use App\Http\Requests\DiscountUpdateRequest;
use App\Models\Discount;

class EloquentDiscountRepository implements DiscountRepository
{
    /**
     * @var Discount
     */
    private $discount;

    /**
     * EloquentDiscountRepository constructor.
     */
    public function __construct(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * Create discount
     *
     * @param DiscountCreateRequest $discountCreateRequest
     * @return mixed
     */
    public function create(DiscountCreateRequest $discountCreateRequest)
    {
        return $this->discount->create($discountCreateRequest->all());
    }

    /**
     * Update discount
     *
     * @param Discount $discount
     * @param DiscountUpdateRequest $discountUpdateRequest
     * @return mixed
     */
    public function update(Discount $discount, DiscountUpdateRequest $discountUpdateRequest)
    {
        return $discount->update($discountUpdateRequest->all());
    }

    /**
     * Delete discount
     *
     * @param Discount $discount
     * @return mixed
     */
    public function delete(Discount $discount)
    {
        return $discount->delete();
    }

    /**
     * @param $code
     */
    public function getDiscountByCode($code)
    {
        return $this->discount->code($code)->first();
    }

    /**
     * @param $code
     * @param $fullBasket
     * @return mixed
     */
    public function applyDiscountToBasket($code, $fullBasket)
    {
        $discount = $this->getDiscountByCode($code);

        if($discount){

            if($discount->type == 'fix'){

                if($discount->discount > $fullBasket['subtotal']){

                    $fullBasket['codeDiscount'] = $fullBasket['subtotal'];

                } else {

                    $fullBasket['codeDiscount'] = $discount->discount;

                }

            } elseif($discount->type == 'per'){

                if($discount->discount > 0 && $discount->discount < 1){

                    $fullBasket['codeDiscount'] = round($fullBasket['subtotal'] * $discount->discount, 2);

                }

            }

            $fullBasket['discounts'] += $fullBasket['codeDiscount'];
            $fullBasket['subtotal'] -= $fullBasket['codeDiscount'];

        }

        return $fullBasket;
    }
}
