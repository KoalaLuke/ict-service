<?php namespace App\Repositories\Discount;

use App\Http\Requests\DiscountCreateRequest;
use App\Http\Requests\DiscountUpdateRequest;
use App\Models\Discount;

interface DiscountRepository
{
    /**
     * Create discount
     *
     * @param DiscountCreateRequest $discountCreateRequest
     * @return mixed
     */
    public function create(DiscountCreateRequest $discountCreateRequest);

    /**
     * Update discount
     *
     * @param Discount $discount
     * @param DiscountUpdateRequest $discountUpdateRequest
     * @return mixed
     */
    public function update(Discount $discount, DiscountUpdateRequest $discountUpdateRequest);

    /**
     * Delete discount
     *
     * @param Discount $discount
     * @return mixed
     */
    public function delete(Discount $discount);

    /**
     * @param $code
     * @return mixed
     */
    public function getDiscountByCode($code);

    /**
     * @param $code
     * @param $fullBasket
     * @return mixed
     */
    public function applyDiscountToBasket($code, $fullBasket);
}