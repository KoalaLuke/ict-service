<?php namespace App\Repositories\Product;


use App\Models\Import;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

interface ProductRepository
{
    /**
     * Get product by its vendor id
     *
     * @param $vendorId
     * @return Product
     */
    public function getProductByVendorId($vendorId);

    /**
     * Import products from a json file
     *
     * @param Import $import
     * @param $json
     * @return void
     */
    public function importProducts(Import $import, $json);

    /**
     * Import product relationship json
     *
     * @param Import $import
     * @param $json
     * @param $type
     * @return void
     */
    public function importRelationships(Import $import, $json, $type);

    /**
     * Get a list of Vendors
     *
     * @return array
     */
    public function getVendors();

    /**
     * @param Product $product
     * @return mixed
     */
    public function breadcrumbs(Product $product);
}