<?php namespace App\Repositories\Product;

use App\Helpers\Breadcrumb;
use App\Models\Category;
use App\Models\Import;
use App\Models\Product;
use App\Models\ProductProducts;
use App\Repositories\Category\CategoryRepository;
use Carbon\Carbon;
use DB;
use Doctrine\Common\Collections\Collection;
use Illuminate\Database\QueryException;

class EloquentProductRepository implements ProductRepository
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * EloquentProductRepository constructor.
     * @param Product $product
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(Product $product, CategoryRepository $categoryRepository)
    {
        $this->product = $product;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Get product by its vendor id
     *
     * @param $vendorId
     * @return Product
     */
    public function getProductByVendorId($vendor_id)
    {
        return $this->product->withTrashed()->vendorId($vendor_id)->first();
    }

    /**
     * Import products from an json file
     *
     * @param Import $import
     * @param $json
     * @return array
     */
    public function importProducts(Import $import, $json)
    {

        $time = Carbon::now();

        DB::transaction(function () use ($json, $time, $import) {

            foreach ($json as $productObject) {

                $attributes = $productObject->Attributes;

                $vendorId = isset($attributes->productid->Value) ? $attributes->productid->Value : '';

                $product = $this->getProductByVendorId($vendorId) ?: new Product();

                $data = [

                    'statecode'                => isset($attributes->statecode->Value) ? $attributes->statecode->Value : null,
                    'icts_saleswebsite'        => isset($attributes->icts_saleswebsite->Value) ? $attributes->icts_saleswebsite->Value : null,
                    'productnumber'            => isset($attributes->productnumber->Value) ? $attributes->productnumber->Value : null,
                    'name'                     => isset($attributes->name->Value) ? $attributes->name->Value : null,
                    'price'                    => isset($attributes->price->Value) ? $attributes->price->Value : null,
                    'icts_rrp'                 => isset($attributes->icts_rrp->Value) ? $attributes->icts_rrp->Value : null,
                    'createdon'                => isset($attributes->createdon->Value) ? $attributes->createdon->Value : null,
                    'modifiedon'               => isset($attributes->modifiedon->Value) ? $attributes->modifiedon->Value : null,
                    'description'              => isset($attributes->description->Value) ? $attributes->description->Value : null,
                    'producturl'               => isset($attributes->producturl->Value) ? $attributes->producturl->Value : null,
                    'edict_imageurl'           => isset($attributes->edict_imageurl->Value) ? $attributes->edict_imageurl->Value : null,
                    'icts_additionalimageurls' => isset($attributes->icts_additionalimageurls->Value) ? $attributes->icts_additionalimageurls->Value : null,
                    'icts_usermanualurl'       => isset($attributes->icts_usermanualurl->Value) ? $attributes->icts_usermanualurl->Value : null,
                    'edict_productcategory'    => isset($attributes->edict_productcategory->Value) ? $attributes->edict_productcategory->Value : null,
                    'icts_category'            => isset($attributes->icts_category->Value) ? $attributes->icts_category->Value : null,
                    'icts_keywords'            => isset($attributes->icts_keywords->Value) ? $attributes->icts_keywords->Value : null,
                    'producttypecode'          => isset($attributes->producttypecode->Value) ? $attributes->producttypecode->Value : null,
                    'vendorname'               => isset($attributes->vendorname->Value) ? $attributes->vendorname->Value : null,
                    'vendorpartnumber'         => isset($attributes->vendorpartnumber->Value) ? $attributes->vendorpartnumber->Value : null,
                    'suppliername'             => isset($attributes->suppliername->Value) ? $attributes->suppliername->Value : null,
                    'edict_eta'                => isset($attributes->edict_eta->Value) ? $attributes->edict_eta->Value : null,
                    'isstockitem'              => isset($attributes->isstockitem->Value) ? $attributes->isstockitem->Value : null,
                    'quantityonhand'           => isset($attributes->quantityonhand->Value) ? $attributes->isstockitem->Value : null,
                    'stockvolume'              => isset($attributes->stockvolume->Value) ? $attributes->stockvolume->Value : 0,
                    'stockweight'              => isset($attributes->stockweight->Value) ? $attributes->stockweight->Value : 0,
                    'productid'                => isset($attributes->productid->Value) ? $attributes->productid->Value : null,
                    'transactioncurrencyid'    => isset($attributes->transactioncurrencyid->Value) ? $attributes->transactioncurrencyid->Value : null,

                ];

                $product->imported_at = $time;

                if(!$product->id){
                    $product->import_id = $import->id;
                }

                $product->fill($data);
                $product->save();
                $product->restore();

                $product->categories()->detach();

                if (!empty($attributes->icts_category->Value)) {

                    $category = $this->categoryRepository->getCategoryByVendorId($attributes->icts_category->Value);


                    if ($category) {

                        $product->categories()->attach($category);

                    } else {

                        throw new \Exception('Invalid category id');

                    }
                }

                $this->product->where('imported_at', '!=', $time->toDateTimeString())->delete();

            }

        });

        $import->products_imported = Product::where('imported_at', '=', $time->toDateTimeString())->count();
        $import->products_new = Product::where('import_id',$import->id)->count();

        $import->save();

    }

    /**
     * Import product relationship json
     *
     * @param Import $import
     * @param $json
     * @param $type
     */
    public function importRelationships(Import $import, $json, $type = 'alternative')
    {
        DB::transaction(function () use ($json, $type, $import) {

            ProductProducts::where('type', $type)->delete();

            $failed = '';

            foreach ($json as $productObject) {

                $attributes = $productObject->Attributes;

                /** @var Product $parent */
                $parent = $this->getProductByVendorId(isset($attributes->icts_primaryproduct->Value) ? $attributes->icts_primaryproduct->Value : null);

                /** @var Product $child */
                $child = $this->getProductByVendorId(isset($attributes->icts_relatedproduct->Value) ? $attributes->icts_relatedproduct->Value : null);

                $note = isset($attributes->icts_note->Value) ? $attributes->icts_note->Value : null;

                if ($parent && $child) {

                    $parent->products()->attach($child, ['type' => $type, 'note' => $note]);

                } else {

                    if (!$parent) {
                        $failed .= $productObject->Id.' - invalid parent product id for an ' . $type.",\n";
                    } else {
                        $failed .= $productObject->Id.' - invalid child product id for an ' . $type.",\n";
                    }

                }

            }

            $import->relationships_failed = $failed;

            $import->save();

        });

    }

    /**
     * Get a list of Vendors
     *
     * @return array
     */
    public function getVendors()
    {
        return Product::select('vendorname')->distinct()->orderBy('vendorname')->whereNotNull('vendorname')->lists('vendorname', 'vendorname');
    }

    /**
     * @param Product $product
     * @return mixed
     */
    public function breadcrumbs(Product $product)
    {
        $categories = $product->categories;

        if(isset($categories[0])){
            $this->categoryRepository->breadcrumbs($categories[0]);
        }

        Breadcrumb::add($product->name,route('front.product.show',$product->slug));
    }
}
