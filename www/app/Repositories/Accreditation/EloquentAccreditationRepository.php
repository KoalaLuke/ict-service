<?php namespace App\Repositories\Accreditation;


use App\Http\Requests\AccreditationCreateRequest;
use App\Http\Requests\AccreditationUpdateRequest;
use App\Models\Accreditation;

class EloquentAccreditationRepository implements AccreditationRepository
{
    /**
     * @var Accreditation
     */
    private $accreditation;

    /**
     * EloquentAccreditationRepository constructor.
     */
    public function __construct(Accreditation $accreditation)
    {
        $this->accreditation = $accreditation;
    }

    /**
     * Create accreditation
     *
     * @param AccreditationCreateRequest $accreditationCreateRequest
     * @return mixed
     */
    public function create(AccreditationCreateRequest $accreditationCreateRequest)
    {
        return $this->accreditation->create($accreditationCreateRequest->all());
    }

    /**
     * Update accreditation
     *
     * @param Accreditation $accreditation
     * @param AccreditationUpdateRequest $accreditationUpdateRequest
     * @return mixed
     */
    public function update(Accreditation $accreditation, AccreditationUpdateRequest $accreditationUpdateRequest)
    {
        return $accreditation->update($accreditationUpdateRequest->all());
    }

    /**
     * Delete accreditation
     *
     * @param Accreditation $accreditation
     * @return mixed
     */
    public function delete(Accreditation $accreditation)
    {
        return $accreditation->delete();
    }
}
