<?php namespace App\Repositories\Accreditation;

use App\Http\Requests\AccreditationCreateRequest;
use App\Http\Requests\AccreditationUpdateRequest;
use App\Models\Accreditation;

interface AccreditationRepository
{
    /**
     * Create accreditation
     *
     * @param AccreditationCreateRequest $accreditationCreateRequest
     * @return mixed
     */
    public function create(AccreditationCreateRequest $accreditationCreateRequest);

    /**
     * Update accreditation
     *
     * @param Accreditation $accreditation
     * @param AccreditationUpdateRequest $accreditationUpdateRequest
     * @return mixed
     */
    public function update(Accreditation $accreditation, AccreditationUpdateRequest $accreditationUpdateRequest);

    /**
     * Delete accreditation
     *
     * @param Accreditation $accreditation
     * @return mixed
     */
    public function delete(Accreditation $accreditation);
}