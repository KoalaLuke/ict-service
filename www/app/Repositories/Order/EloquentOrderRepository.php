<?php namespace App\Repositories\Order;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Repositories\Basket\BasketRepository;

class EloquentOrderRepository implements OrderRepository
{
    /**
     * @var Order
     */
    private $order;

    /**
     * EloquentOrderRepository constructor.
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Create order
     *
     * @param BasketRepository $basketRepository
     * @return mixed
     */
    public function create(BasketRepository $basketRepository)
    {
        $user = \Auth::user();

        $basket =  $basketRepository->getFullBasket();

        if(count($basket['items'])>0) {

            $basket['user_id'] = $user->id;
            $basket['organisation'] = $user->organisation;
            $basket['address_line_1'] = $user->address_line_1;
            $basket['address_line_2'] = $user->address_line_2;
            $basket['address_line_3'] = $user->address_line_3;
            $basket['city'] = $user->city;
            $basket['postcode'] = $user->postcode;
            $basket['urn'] = $user->urn;
            $basket['discount'] = $basket['codeDiscount'];

            if($basket['same_address']=='no'){

                $basket['delivery_contact'] = $basket['deliveryAddress']->contact;
                $basket['delivery_address_line_1'] = $basket['deliveryAddress']->address_line_1;
                $basket['delivery_address_line_2'] = $basket['deliveryAddress']->address_line_2;
                $basket['delivery_address_line_3'] = $basket['deliveryAddress']->address_line_3;
                $basket['delivery_city'] = $basket['deliveryAddress']->city;
                $basket['delivery_postcode'] = $basket['deliveryAddress']->postcode;

            }

            unset($basket['contact']);
            unset($basket['delivery_address']);
            unset($basket['deliveryAddress']);

            /** @var Order $order */
            $order = clone($this->order);
            $order->fill($basket);
            $order->save();

            $items = [];

            foreach ($basket['items'] as $product) {
                $items[] = $this->convertProductToOrderItem($product);
            }

            $order->orderItems()->saveMany($items);
        } else {

            $order = false;

        }

        return $order;
    }

    /**
     * Convert a product to order item
     *
     * @param Product $product
     * return OrderItem
     */
    private function convertProductToOrderItem(Product $product)
    {
        $new = new OrderItem();
        $product = $product->toArray();
        unset($product['icts_additionalimageurls']);
        $new->fill($product);

        return $new;
    }

}
