<?php namespace App\Repositories\Order;

use App\Models\Order;
use App\Repositories\Basket\BasketRepository;

interface OrderRepository
{
    /**
     * Create order
     *
     * @param BasketRepository $basketRepository
     * @return mixed
     */
    public function create(BasketRepository $basketRepository);

}