<?php namespace App\Repositories\Message;

use App\Http\Requests\MessageCreateRequest;
use App\Http\Requests\MessageUpdateRequest;
use App\Models\Message;

interface MessageRepository
{
    /**
     * Create message
     *
     * @param MessageCreateRequest $messageCreateRequest
     * @return mixed
     */
    public function create(MessageCreateRequest $messageCreateRequest);

    /**
     * Update message
     *
     * @param Message $message
     * @param MessageUpdateRequest $messageUpdateRequest
     * @return mixed
     */
    public function update(Message $message, MessageUpdateRequest $messageUpdateRequest);

    /**
     * Delete message
     *
     * @param Message $message
     * @return mixed
     */
    public function delete(Message $message);
}