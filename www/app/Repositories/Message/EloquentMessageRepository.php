<?php namespace App\Repositories\Message;

use App\Http\Requests\MessageCreateRequest;
use App\Http\Requests\MessageUpdateRequest;
use App\Models\Message;

class EloquentMessageRepository implements MessageRepository
{
    /**
     * @var Message
     */
    private $message;

    /**
     * EloquentMessageRepository constructor.
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Create message
     *
     * @param MessageCreateRequest $messageCreateRequest
     * @return mixed
     */
    public function create(MessageCreateRequest $messageCreateRequest)
    {
        return $this->message->create($messageCreateRequest->all());
    }

    /**
     * Update message
     *
     * @param Message $message
     * @param MessageUpdateRequest $messageUpdateRequest
     * @return mixed
     */
    public function update(Message $message, MessageUpdateRequest $messageUpdateRequest)
    {
        return $message->update($messageUpdateRequest->all());
    }

    /**
     * Delete message
     *
     * @param Message $message
     * @return mixed
     */
    public function delete(Message $message)
    {
        return $message->delete();
    }
}
