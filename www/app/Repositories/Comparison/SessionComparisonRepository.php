<?php namespace App\Repositories\Comparison;

use App\Models\Product;
use Illuminate\Session\SessionManager;

class SessionComparisonRepository implements ComparisonRepository
{
    /**
     * @var \Session
     */
    private $session;

    /**
     * SessionComparisonRepository constructor.
     *
     * @param \Session $session
     */
    public function __construct(SessionManager $session)
    {
        $this->session = $session;
    }

    /**
     * Get the count of the items in the comparison
     *
     * @return int
     */
    public function getItemCount()
    {
        $comparison = $this->session->get('comparison');

        $comparison_count = count($comparison['items']);

        return $comparison_count;
    }

    /**
     * Add an item to the comparison and return new count
     *
     * @param $item
     * @return int
     */
    public function updateItem($item)
    {
        $comparison = $this->session->get('comparison');

        $count = $this->getItemCount();

        $new_item = true;

        if($comparison['items']) {

            foreach ($comparison['items'] as $key => $comparison_item) {

                if ($comparison_item['id'] == $item->id) {

                    unset($comparison['items'][$key]);

                    $count--;

                    $new_item = false;

                }

            }

        }

        if($new_item and ($count <= 2)){

            $new_item = ['id'=>$item->id];

            $comparison['items'][] = $new_item;

            $count++;

        }

        $this->session->put('comparison.items', $comparison['items']);

        return $count;
    }

    /**
     * Get full comparison
     *
     * @return array
     */
    public function getFullComparison(){

        $comparison = $this->session->get('comparison');

        $fullComparison['items'] = [];
        $fullComparison['count'] = 0;

        if(!empty($comparison['items'])) {

            foreach ($comparison['items'] as $item) {

                $product = Product::find($item['id']);

                if ($product) {

                    $fullComparison['items'][] = $product;
                    $fullComparison['count']++;
                }


            }

        }

        return $fullComparison;

    }

    /**
     * Check if a product is in the comparison
     *
     * @param Product $product
     * @return boolean
     */
    public function isProductInComparison(Product $product)
    {
        $comparison = $this->session->get('comparison');

        $inComparison = false;

        if(isset($comparison['items'])) {

            foreach ($comparison['items'] as $item) {

                if ($item['id'] == $product->id) {

                    $inComparison = true;

                }

            }

        }

        return $inComparison;
    }
}