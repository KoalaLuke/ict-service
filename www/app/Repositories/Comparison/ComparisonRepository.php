<?php namespace App\Repositories\Comparison;

use App\Models\Product;

interface ComparisonRepository
{

    /**
     * Get the count of the items in the comparison
     *
     * @return int
     */
    public function getItemCount();

    /**
     * Add an item to the comparison
     *
     * @ return array
     */
    public function updateItem($item);

    /**
     * Get the full comparison
     *
     * @return mixed
     */
    public function getFullComparison();

    /**
     * Check if a product is in the comparison
     *
     * @param Product $product
     * @return boolean
     */
    public function isProductInComparison(Product $product);

}