<?php namespace App\Repositories\Specification;


use App\Models\Import;
use App\Models\Specification;
use Illuminate\Database\Eloquent\Collection;

interface SpecificationRepository
{
    /**
     * Get specification by its vendor id
     *
     * @param $vendorId
     * @return Specification
     */
    public function getSpecificationByVendorId($vendorId);

    /**
     * Import features from a json file
     *
     * @param Import $import
     * @param $json
     * @return void
     */
    public function importSpecifications(Import $import, $json);
}