<?php namespace App\Repositories\Specification;

use App\Models\Import;
use App\Models\Specification;
use App\Repositories\Feature\FeatureRepository;
use App\Repositories\Product\ProductRepository;
use DB;
use Doctrine\Common\Collections\Collection;
use Illuminate\Database\QueryException;

class EloquentSpecificationRepository implements SpecificationRepository
{
    /**
     * @var Specification
     */
    private $specification;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var FeatureRepository
     */
    private $featureRepository;

    /**
     * EloquentSpecificationRepository constructor.
     * @param Specification $specification
     * @param ProductRepository $productRepository
     * @param FeatureRepository $featureRepository
     */
    public function __construct(Specification $specification, ProductRepository $productRepository, FeatureRepository $featureRepository)
    {
        $this->specification = $specification;
        $this->productRepository = $productRepository;
        $this->featureRepository = $featureRepository;
    }

    /**
     * Get specification by its vendor id
     *
     * @param $vendorId
     * @return Specification
     */
    public function getSpecificationByVendorId($vendor_id)
    {
        return $this->specification->vendorId($vendor_id)->first();
    }

    /**
     * Import specifications from an json file
     *
     * @param Import $import
     * @param $json
     * @return void
     */
    public function importSpecifications(Import $import, $json)
    {
        DB::transaction(function() use ($json, $import) {

            foreach($json as $specificationObject){

                $attributes = $specificationObject->Attributes;

                $vendorId = isset($attributes->icts_productspecid->Value) ? $attributes->icts_productspecid->Value : '';

                $specification = $this->getSpecificationByVendorId($vendorId) ?: new Specification();

                $data = [

                    'statecode' => isset($attributes->statecode->Value) ? $attributes->statecode->Value : null ,
                    'icts_product' => isset($attributes->icts_product->Value) ? $attributes->icts_product->Value : null ,
                    'icts_feature' => isset($attributes->icts_feature->Value) ? $attributes->icts_feature->Value : null ,
                    'icts_value' => isset($attributes->icts_value->Value) ? $attributes->icts_value->Value : null ,
                    'icts_displaytext' => isset($attributes->icts_displaytext->Value) ? $attributes->icts_displaytext->Value : null ,
                    'modifiedon' => isset($attributes->modifiedon->Value) ? $attributes->modifiedon->Value : null ,
                    'icts_productspecid' => isset($attributes->icts_productspecid->Value) ? $attributes->icts_productspecid->Value : null ,

                ];


                $product = $this->productRepository->getProductByVendorId($attributes->icts_product->Value);

                if(!$product) {
                    \Log::warning('Spec import did not find associated product', [
                        $data
                    ]);
                    continue;
                }

                $feature = $this->featureRepository->getFeatureByVendorId($attributes->icts_feature->Value);

                if(!$feature) {
                    \Log::warning('Spec import did not find associated feature', [
                        $data
                    ]);
                    continue;
                }




                $specification->product()->associate($product);
                $specification->feature()->associate($feature);

                $specification->fill($data);
                $specification->save();

            }

        });

    }

}
