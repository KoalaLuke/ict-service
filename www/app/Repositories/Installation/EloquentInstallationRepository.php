<?php namespace App\Repositories\Installation;

use App\Http\Requests\InstallationCreateRequest;
use App\Models\Installation;

class EloquentInstallationRepository implements InstallationRepository
{
    /**
     * @var Installation
     */
    private $installation;

    /**
     * EloquentInstallationRepository constructor.
     */
    public function __construct(Installation $installation)
    {
        $this->installation = $installation;
    }

    /**
     * Create installation
     *
     * @param InstallationCreateRequest $installationCreateRequest
     * @return mixed
     */
    public function create(InstallationCreateRequest $installationCreateRequest)
    {
        return $this->installation->create($installationCreateRequest->all());
    }
    
    /**
     * Delete installation
     *
     * @param Installation $installation
     * @return mixed
     */
    public function delete(Installation $installation)
    {
        return $installation->delete();
    }
}