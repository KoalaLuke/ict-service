<?php namespace App\Repositories\Installation;

use App\Http\Requests\InstallationCreateRequest;
use App\Models\Installation;

interface InstallationRepository
{
    /**
     * Create installation
     *
     * @param InstallationCreateRequest $installationCreateRequest
     * @return mixed
     */
    public function create(InstallationCreateRequest $installationCreateRequest);
    
    /**
     * Delete installation
     *
     * @param Installation $installation
     * @return mixed
     */
    public function delete(Installation $installation);
}