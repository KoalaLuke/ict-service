<?php namespace App\Repositories\Slide;


use App\Http\Requests\SlideCreateRequest;
use App\Http\Requests\SlideUpdateRequest;
use App\Models\Slide;

class EloquentSlideRepository implements SlideRepository
{
    /**
     * @var Slide
     */
    private $slide;

    /**
     * EloquentSlideRepository constructor.
     */
    public function __construct(Slide $slide)
    {
        $this->slide = $slide;
    }

    /**
     * Create slide
     *
     * @param SlideCreateRequest $slideCreateRequest
     * @return mixed
     */
    public function create(SlideCreateRequest $slideCreateRequest)
    {
        return $this->slide->create($slideCreateRequest->all());
    }

    /**
     * Update slide
     *
     * @param Slide $slide
     * @param SlideUpdateRequest $slideUpdateRequest
     * @return mixed
     */
    public function update(Slide $slide, SlideUpdateRequest $slideUpdateRequest)
    {
        return $slide->update($slideUpdateRequest->all());
    }

    /**
     * Delete slide
     *
     * @param Slide $slide
     * @return mixed
     */
    public function delete(Slide $slide)
    {
        return $slide->delete();
    }
}
