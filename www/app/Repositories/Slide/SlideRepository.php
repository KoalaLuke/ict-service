<?php namespace App\Repositories\Slide;

use App\Http\Requests\SlideCreateRequest;
use App\Http\Requests\SlideUpdateRequest;
use App\Models\Slide;

interface SlideRepository
{
    /**
     * Create slide
     *
     * @param SlideCreateRequest $slideCreateRequest
     * @return mixed
     */
    public function create(SlideCreateRequest $slideCreateRequest);

    /**
     * Update slide
     *
     * @param Slide $slide
     * @param SlideUpdateRequest $slideUpdateRequest
     * @return mixed
     */
    public function update(Slide $slide, SlideUpdateRequest $slideUpdateRequest);

    /**
     * Delete slide
     *
     * @param Slide $slide
     * @return mixed
     */
    public function delete(Slide $slide);
}