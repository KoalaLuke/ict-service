<?php namespace App\Repositories\Website;

use App\Models\Website;

class EloquentWebsiteRepository implements WebsiteRepository
{
    /**
     * @var Website
     */
    private $website;

    /**
     * EloquentWebsiteRepository constructor.
     */
    public function __construct(Website $website)
    {
        $this->website = $website;
    }
}