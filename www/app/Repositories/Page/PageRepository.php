<?php namespace App\Repositories\Page;


use App\Http\Requests\PageCreateRequest;
use App\Http\Requests\PageUpdateRequest;
use App\Models\Page;

interface PageRepository
{
    /**
     * Get page by position
     *
     * @param $position
     * @return Page
     */
    public function getPageByPosition($position);

    /**
     * Create page
     *
     * @param PageCreateRequest $pageCreateRequest
     * @return mixed
     */
    public function create(PageCreateRequest $pageCreateRequest);

    /**
     * Update page
     *
     * @param Page $page
     * @param PageUpdateRequest $pageUpdateRequest
     * @return mixed
     */
    public function update(Page $page, PageUpdateRequest $pageUpdateRequest);

    /**
     * Delete page
     *
     * @param Page $page
     * @return mixed
     */
    public function delete(Page $page);

    /**
     * Breadcrumbs
     *
     * @param Page $page
     * @return mixed
     */
    public function breadcrumbs(Page $page);
}