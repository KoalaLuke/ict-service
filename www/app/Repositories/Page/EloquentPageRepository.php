<?php namespace App\Repositories\Page;


use App\Helpers\Breadcrumb;
use App\Http\Requests\PageCreateRequest;
use App\Http\Requests\PageUpdateRequest;
use App\Models\Page;

class EloquentPageRepository implements PageRepository
{
    /**
     * @var Page
     */
    private $page;

    /**
     * EloquentPageRepository constructor.
     */
    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Get page by position
     *
     * @param $position
     * @return Page
     */
    public function getPageByPosition($position)
    {
        return $this->page->position($position)->orderBy('order','asc')->get();
    }

    /**
     * Create page
     *
     * @param PageCreateRequest $pageCreateRequest
     * @return mixed
     */
    public function create(PageCreateRequest $pageCreateRequest)
    {
        return $this->page->create($pageCreateRequest->all());
    }

    /**
     * Update page
     *
     * @param Page $page
     * @param PageUpdateRequest $pageUpdateRequest
     * @return mixed
     */
    public function update(Page $page, PageUpdateRequest $pageUpdateRequest)
    {
        return $page->update($pageUpdateRequest->all());
    }

    /**
     * Delete page
     *
     * @param Page $page
     * @return mixed
     */
    public function delete(Page $page)
    {
        return $page->delete();
    }

    /**
     * Breadcrumbs
     *
     * @param Page $page
     */
    public function breadcrumbs(Page $page)
    {
        Breadcrumb::add($page->name,$page->slug);
    }
}
