<?php namespace App\Repositories\User;


use App\Http\Requests\UserAdminUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;

class EloquentUserRepository implements UserRepository
{
    /**
     * @var User
     */
    private $user;

    /**
     * EloquentUserRepository constructor.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the authorized user
     *
     * @return User
     */
    public function authorized()
    {
        return \Auth::user();
    }

    /**
     * Update user
     *
     * @param User $user
     * @param UserUpdateRequest $userUpdateRequest
     * @return mixed
     */
    public function update(User $user, UserUpdateRequest $userUpdateRequest)
    {
        return $user->update($userUpdateRequest->all());
    }

    /**
     * Is the user an admin?
     *
     * @return bool
     */
    public function isAdmin()
    {
        if($user = $this->authorized()) {
            return $user->admin;
        } else {
            return false;
        }
    }

    /**
     * Get user by Id
     *
     * @param $id
     * @return mixed
     */
    public function getUserById($id)
    {
        return $this->user->id($id)->first();
    }

    /**
     * Switch Admin Rights
     *
     * @param $user
     * @param $userAdminUpdateRequest
     * @return mixed
     */
    public function switchAdminRights(User $user, UserAdminUpdateRequest $userAdminUpdateRequest)
    {
        return $user->update($userAdminUpdateRequest->all());
    }

    /**
     * @param $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->delete();
    }
}