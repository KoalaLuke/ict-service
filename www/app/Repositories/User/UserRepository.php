<?php namespace App\Repositories\User;


use App\Http\Requests\UserAdminUpdateRequest;
use App\Models\User;

interface UserRepository
{

    /**
     * Get the authorized user
     *
     * @return User
     */
    public function authorized();

    /**
     * Is the user an admin?
     *
     * @return bool
     */
    public function isAdmin();

    /**
     * Get user by Id
     *
     * @param $id
     * @return mixed
     */
    public function getUserById($id);

    /**
     * Switch Admin Rights
     *
     * @param $user
     * @param $userAdminUpdateRequest
     * @return mixed
     */
    public function switchAdminRights(User $user, UserAdminUpdateRequest $userAdminUpdateRequest);

    /**
     * @param $user
     * @return mixed
     */
    public function delete(User $user);
}