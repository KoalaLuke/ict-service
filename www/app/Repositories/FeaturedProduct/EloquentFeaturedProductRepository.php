<?php namespace App\Repositories\FeaturedProduct;

use App\Http\Requests\FeaturedProductCreateRequest;
use App\Models\FeaturedProduct;
use App\Repositories\Product\ProductRepository;

class EloquentFeaturedProductRepository implements FeaturedProductRepository
{
    /**
     * @var FeaturedProduct
     */
    private $featuredProduct;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * EloquentFeaturedProductRepository constructor.
     */
    public function __construct(
        FeaturedProduct $featuredProduct,
        ProductRepository $productRepository
    )
    {
        $this->featuredProduct = $featuredProduct;
        $this->productRepository = $productRepository;
    }

    /**
     * Create featuredProduct
     *
     * @param FeaturedProductCreateRequest $featuredProductCreateRequest
     * @return mixed
     */
    public function create(FeaturedProductCreateRequest $featuredProductCreateRequest)
    {
        $product=$this->productRepository->getProductByVendorId($featuredProductCreateRequest->productid);

        if($product){

            $featuredProduct = $this->featuredProduct->create($featuredProductCreateRequest->all());

        } else {

            $featuredProduct = false;

        }

        return $featuredProduct;
    }

    /**
     * Delete featuredProduct
     *
     * @param FeaturedProduct $featuredProduct
     * @return mixed
     */
    public function delete(FeaturedProduct $featuredProduct)
    {
        return $featuredProduct->delete();
    }
}
