<?php namespace App\Repositories\FeaturedProduct;

use App\Http\Requests\FeaturedProductCreateRequest;
use App\Models\FeaturedProduct;

interface FeaturedProductRepository
{
    /**
     * Create featuredProduct
     *
     * @param FeaturedProductCreateRequest $featuredProductCreateRequest
     * @return mixed
     */
    public function create(FeaturedProductCreateRequest $featuredProductCreateRequest);

    /**
     * Delete featuredProduct
     *
     * @param FeaturedProduct $featuredProduct
     * @return mixed
     */
    public function delete(FeaturedProduct $featuredProduct);
}