<?php namespace App\Repositories\DeliveryAddress;

use App\Http\Requests\DeliveryAddressCreateRequest;
use App\Http\Requests\DeliveryAddressUpdateRequest;
use App\Models\DeliveryAddress;

interface DeliveryAddressRepository
{
    /**
     * Create deliveryAddress
     *
     * @param DeliveryAddressCreateRequest $deliveryAddressCreateRequest
     * @return mixed
     */
    public function create(DeliveryAddressCreateRequest $deliveryAddressCreateRequest);

    /**
     * Update deliveryAddress
     *
     * @param DeliveryAddress $deliveryAddress
     * @param DeliveryAddressUpdateRequest $deliveryAddressUpdateRequest
     * @return mixed
     */
    public function update(DeliveryAddress $deliveryAddress, DeliveryAddressUpdateRequest $deliveryAddressUpdateRequest);

    /**
     * Delete deliveryAddress
     *
     * @param DeliveryAddress $deliveryAddress
     * @return mixed
     */
    public function delete(DeliveryAddress $deliveryAddress);
}