<?php namespace App\Repositories\DeliveryAddress;


use App\Http\Requests\DeliveryAddressCreateRequest;
use App\Http\Requests\DeliveryAddressUpdateRequest;
use App\Models\DeliveryAddress;

class EloquentDeliveryAddressRepository implements DeliveryAddressRepository
{
    /**
     * @var DeliveryAddress
     */
    private $deliveryAddress;

    /**
     * EloquentDeliveryAddressRepository constructor.
     */
    public function __construct(DeliveryAddress $deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * Create deliveryAddress
     *
     * @param DeliveryAddressCreateRequest $deliveryAddressCreateRequest
     * @return mixed
     */
    public function create(DeliveryAddressCreateRequest $deliveryAddressCreateRequest)
    {
        return $this->deliveryAddress->create($deliveryAddressCreateRequest->all());
    }

    /**
     * Update deliveryAddress
     *
     * @param DeliveryAddress $deliveryAddress
     * @param DeliveryAddressUpdateRequest $deliveryAddressUpdateRequest
     * @return mixed
     */
    public function update(DeliveryAddress $deliveryAddress, DeliveryAddressUpdateRequest $deliveryAddressUpdateRequest)
    {
        return $deliveryAddress->update($deliveryAddressUpdateRequest->all());
    }

    /**
     * Delete deliveryAddress
     *
     * @param DeliveryAddress $deliveryAddress
     * @return mixed
     */
    public function delete(DeliveryAddress $deliveryAddress)
    {
        return $deliveryAddress->delete();
    }
}
