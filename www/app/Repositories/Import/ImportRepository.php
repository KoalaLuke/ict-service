<?php namespace App\Repositories\Import;

use App\Models\Import;

interface ImportRepository
{
    /**
     * Create import
     *
     * @param Import $import
     * @return mixed
     */
    public function create(Import $import);

    /**
     * Import import
     */
    public function import();

}