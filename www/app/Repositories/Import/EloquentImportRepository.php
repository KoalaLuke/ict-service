<?php namespace App\Repositories\Import;

use App\Models\Category;
use App\Models\Feature;
use App\Models\Import;
use App\Models\Product;
use App\Models\Specification;
use App\Repositories\Feature\FeatureRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Specification\SpecificationRepository;
use File;

class EloquentImportRepository implements ImportRepository
{
    /**
     * @var Import
     * @var CategoryRepository
     * @var FeatureRepository
     * @var ProductRepository
     * @var SpecificationRepository
     */
    private $import;
    private $categoryRepository;
    private $featureRepository;
    private $productRepository;
    private $specificationRepository;


    /**
     * EloquentImportRepository constructor.
     */
    public function __construct(
        Import $import,
        CategoryRepository $categoryRepository,
        FeatureRepository $featureRepository,
        ProductRepository $productRepository,
        SpecificationRepository $specificationRepository
    )
    {
        $this->import = $import;
        $this->categoryRepository = $categoryRepository;
        $this->featureRepository = $featureRepository;
        $this->productRepository = $productRepository;
        $this->specificationRepository = $specificationRepository;
    }

    /**
     * Create import
     *
     * @param Import $import
     * @return mixed
     */
    public function create(Import $import)
    {
        return $import;
    }

    /**
     * Import all the sections
     */
    public function import()
    {
        $import = new Import;
        $import->save();

        $app_directory = '/var/www/ict-service/www/';

        // keep in this order or else!!!
        $this->featureRepository->importFeatures($import, json_decode(file_get_contents($app_directory.'imports/features.json')));
        $this->categoryRepository->importCategories($import, json_decode(file_get_contents($app_directory.'imports/categories.json')));
        $this->productRepository->importProducts($import, json_decode(file_get_contents($app_directory.'imports/products.json')));
        $this->productRepository->importRelationships($import, json_decode(file_get_contents($app_directory.'imports/alternative-products.json')),'alternative');
        $this->productRepository->importRelationships($import, json_decode(file_get_contents($app_directory.'imports/associated-products.json')),'associated');
        $this->specificationRepository->importSpecifications($import, json_decode(file_get_contents($app_directory.'imports/specifications.json')));

        $directory = $app_directory.'storage/framework/cache';
        File::deleteDirectory($directory, true);
    }

}
