<?php namespace App\Repositories\Image;

use App\Http\Requests\ImageCreateRequest;
use App\Http\Requests\ImageUpdateRequest;
use App\Models\Image;

interface ImageRepository
{
    /**
     * Create image
     *
     * @param ImageCreateRequest $imageCreateRequest
     * @return mixed
     */
    public function create(ImageCreateRequest $imageCreateRequest);

    /**
     * Update image
     *
     * @param Image $image
     * @param ImageUpdateRequest $imageUpdateRequest
     * @return mixed
     */
    public function update(Image $image, ImageUpdateRequest $imageUpdateRequest);

    /**
     * Delete image
     *
     * @param Image $image
     * @return mixed
     */
    public function delete(Image $image);
}