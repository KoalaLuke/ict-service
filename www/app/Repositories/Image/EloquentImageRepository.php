<?php namespace App\Repositories\Image;

use App\Http\Requests\ImageCreateRequest;
use App\Http\Requests\ImageUpdateRequest;
use App\Models\Image;

class EloquentImageRepository implements ImageRepository
{
    /**
     * @var Image
     */
    private $image;

    /**
     * EloquentImageRepository constructor.
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * Create image
     *
     * @param ImageCreateRequest $imageCreateRequest
     * @return mixed
     */
    public function create(ImageCreateRequest $imageCreateRequest)
    {
        $image_file = $imageCreateRequest->file('file');

        $image = $this->image->create($imageCreateRequest->all());

        $filename  = $image->id.'.'.$image_file->getClientOriginalExtension();

        $path = public_path('/images/uploads/' . $filename);

        \Intervention\Image\ImageManagerStatic::make($image_file->getRealPath())->save($path);

        $image->file = $filename;

        $image->save();

        return $image;
    }

    /**
     * Update image
     *
     * @param Image $image
     * @param ImageUpdateRequest $imageUpdateRequest
     * @return mixed
     */
    public function update(Image $image, ImageUpdateRequest $imageUpdateRequest)
    {
        return $image->update($imageUpdateRequest->all());
    }

    /**
     * Delete image
     *
     * @param Image $image
     * @return mixed
     */
    public function delete(Image $image)
    {

        try {
            if(!empty($image->file)) {
                unlink(public_path().'/images/uploads/' . $image->file);
            }
        } catch(\Exception $e) {
            
        }

        return $image->delete();
    }
}