<?php namespace App\Repositories\Banner;


use App\Http\Requests\BannerCreateRequest;
use App\Http\Requests\BannerUpdateRequest;
use App\Models\Banner;

class EloquentBannerRepository implements BannerRepository
{
    /**
     * @var Banner
     */
    private $banner;

    /**
     * EloquentBannerRepository constructor.
     */
    public function __construct(Banner $banner)
    {
        $this->banner = $banner;
    }

    /**
     * Create banner
     *
     * @param BannerCreateRequest $bannerCreateRequest
     * @return mixed
     */
    public function create(BannerCreateRequest $bannerCreateRequest)
    {
        return $this->banner->create($bannerCreateRequest->all());
    }

    /**
     * Update banner
     *
     * @param Banner $banner
     * @param BannerUpdateRequest $bannerUpdateRequest
     * @return mixed
     */
    public function update(Banner $banner, BannerUpdateRequest $bannerUpdateRequest)
    {
        return $banner->update($bannerUpdateRequest->all());
    }

    /**
     * Delete banner
     *
     * @param Banner $banner
     * @return mixed
     */
    public function delete(Banner $banner)
    {
        return $banner->delete();
    }
}
