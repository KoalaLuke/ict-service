<?php namespace App\Repositories\Banner;

use App\Http\Requests\BannerCreateRequest;
use App\Http\Requests\BannerUpdateRequest;
use App\Models\Banner;

interface BannerRepository
{
    /**
     * Create banner
     *
     * @param BannerCreateRequest $bannerCreateRequest
     * @return mixed
     */
    public function create(BannerCreateRequest $bannerCreateRequest);

    /**
     * Update banner
     *
     * @param Banner $banner
     * @param BannerUpdateRequest $bannerUpdateRequest
     * @return mixed
     */
    public function update(Banner $banner, BannerUpdateRequest $bannerUpdateRequest);

    /**
     * Delete banner
     *
     * @param Banner $banner
     * @return mixed
     */
    public function delete(Banner $banner);
}