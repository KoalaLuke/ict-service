<?php namespace App\Repositories\Email;

use App\Http\Requests\EmailUpdateRequest;
use App\Models\Email;

class EloquentEmailRepository implements EmailRepository
{
    /**
     * @var Email
     */
    private $email;

    /**
     * EloquentEmailRepository constructor.
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Update email
     *
     * @param Email $email
     * @param EmailUpdateRequest $emailUpdateRequest
     * @return mixed
     */
    public function update(Email $email, EmailUpdateRequest $emailUpdateRequest)
    {
        return $email->update($emailUpdateRequest->all());
    }

    /**
     * Get email by reference
     *
     * @param $reference
     * @return mixed
     */
    public function getEmailByReference($reference)
    {
        return $this->email->reference($reference)->first();
    }
}
