<?php namespace App\Repositories\Email;

use App\Http\Requests\EmailUpdateRequest;
use App\Models\Email;

interface EmailRepository
{

    /**
     * Get email by reference
     *
     * @param $reference
     * @return mixed
     */
    public function getEmailByReference($reference);

    /**
     * Update email
     *
     * @param Email $email
     * @param EmailUpdateRequest $emailUpdateRequest
     * @return mixed
     */
    public function update(Email $email, EmailUpdateRequest $emailUpdateRequest);

}