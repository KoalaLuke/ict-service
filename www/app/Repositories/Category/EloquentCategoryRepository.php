<?php namespace App\Repositories\Category;

use App\Helpers\Breadcrumb;
use App\Models\Category;
use App\Models\Import;
use App\Models\Product;
use Carbon\Carbon;
use DB;
use Doctrine\Common\Collections\Collection;

class EloquentCategoryRepository implements CategoryRepository
{
    /**
     * @var Category
     */
    private $category;

    /**
     * EloquentCategoryRepository constructor.
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get category by its vendor id
     *
     * @param $vendorId
     * @return Category
     */
    public function getCategoryByVendorId($vendor_id)
    {
        return $this->category->withTrashed()->vendorId($vendor_id)->first();
    }

    /**
     * Import categories from an json file
     *
     * @param Import $import
     * @param $json
     * @return Collection
     */
    public function importCategories(Import $import, $json)
    {
        $time = Carbon::now();

        DB::transaction(function () use ($json, $time, $import) {

            foreach ($json as $categoryObject) {

                $attributes = $categoryObject->Attributes;

                $vendorId = isset($attributes->icts_productcategoryid->Value) ? $attributes->icts_productcategoryid->Value : '';

                $category = $this->getCategoryByVendorId($vendorId) ?: new Category();

                $data = [

                    'statecode'              => isset($attributes->statecode->Value) ? $attributes->statecode->Value : null,
                    'icts_name'              => isset($attributes->icts_name->Value) ? $attributes->icts_name->Value : null,
                    'icts_imageurl'          => isset($attributes->icts_imageurl->Value) ? $attributes->icts_imageurl->Value : null,
                    'icts_parentcategory'    => isset($attributes->icts_parentcategory->Value) ? $attributes->icts_parentcategory->Value : null,
                    'modifiedon'             => isset($attributes->modifiedon->Value) ? $attributes->modifiedon->Value : null,
                    'icts_productcategoryid' => isset($attributes->icts_productcategoryid->Value) ? $attributes->icts_productcategoryid->Value : null,
                    'icts_sortorder'         => isset($attributes->icts_sortorder->Value) ? $attributes->icts_sortorder->Value : null,

                ];

                $category->imported_at = $time;

                if(!$category->id){
                    $category->import_id = $import->id;
                }

                $category->fill($data);
                $category->save();
                $category->restore();

            }

            $this->category->where('imported_at', '!=', $time->toDateTimeString())->delete();

        });

        $import->categories_imported = Category::where('imported_at', '=', $time->toDateTimeString())->count();
        $import->categories_new = Category::where('import_id',$import->id)->count();

        $import->save();
    }

    /**
     * Get top level categories with children
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNestedCategories()
    {
        return $this->category
            ->whereNull('icts_parentcategory')
            ->where('icts_productcategoryid', '!=' , $this->category->serviceCategory)
            ->with(['children'])
            ->orderBy(\DB::raw('-icts_sortorder'), 'DESC')
            ->orderBy('icts_name', 'ASC')
            ->get();
    }


    /**
     * Get installations that haven't been added
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNewInstallations(Category $category)
    {
        // Get a blank Category so we can get the installation Category
        $categoryModel = New Category;

        // Get installation Category products
        $allInstallations = $this->getCategoryByVendorId($categoryModel->installationCategory)->products;

        // Turn the productids of this category into an array
        $categoryInstallations = $category->installations->lists('productid')->toArray();

        // Remove the products that are already in this category
        $installationProducts = $allInstallations->filter(function ($item) use ($categoryInstallations) {

            return !in_array($item->productid,$categoryInstallations) ;

        });

        // Turn the products that are left into list for select
        return $installationProducts;
    }

    /**
     * Get the products from the categories children
     *
     * @param Category $category
     * @return mixed
     */
    public function getProductsChildren(Category $category)
    {

        $children = $this->category->where('icts_parentcategory',$category->icts_productcategoryid)->lists('id');

        return Product::whereHas('categories', function($query) use ($children) {
            $query->whereIn('id', $children);
        });
    }

    /**
     * Get the breadcrumbs for category
     *
     * @param Category $category
     * @return collection
     */
    public function breadcrumbs(Category $category)
    {
        if($category->parent){

            Breadcrumb::add($category->parent->icts_name,route('front.category.show',$category->parent->slug));

        }

        Breadcrumb::add($category->icts_name,route('front.category.show',$category->slug));
    }
}
