<?php namespace App\Repositories\Category;


use App\Models\Category;
use App\Models\Import;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepository
{
    /**
     * Get category by its vendor id
     *
     * @param $vendorId
     * @return Category
     */
    public function getCategoryByVendorId($vendorId);

    /**
     * Import categories from an json file
     *
     * @param Import $import
     * @param $json
     * @return Collection
     */
    public function importCategories(Import $import, $json);

    /**
     * Get top level categories with children
     *
     * @return Collection
     */
    public function getNestedCategories();

    /**
     * Get installations that haven't been added
     *
     * @return Collection
     */
    public function getNewInstallations(Category $category);

    /**
     * Get the products from the categories children
     *
     * @param Category $category
     * @return mixed
     */
    public function getProductsChildren(Category $category);

    /**
     * Get the breadcrumbs for category
     *
     * @param Category $category
     * @return mixed
     */
    public function breadcrumbs(Category $category);
}