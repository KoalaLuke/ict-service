<?php namespace App\Repositories\Basket;

use App\Http\Requests\ApplyDiscountRequest;
use App\Http\Requests\BasketDetailsUpdateRequest;
use App\Http\Requests\BasketUpdateRequest;

interface BasketRepository
{

    /**
     * Get the count of the items in the basket
     *
     * @return int
     */
    public function getItemCount();

    /**
     * Add an item to the basket
     *
     * @ return array
     */
    public function addItem($item);

    /**
     * Get the full basket
     *
     * @return mixed
     */
    public function getFullBasket();

    /**
     * Update basket details
     *
     * @param $basket
     */
    public function updateBasketDetails(BasketDetailsUpdateRequest $basketDetailsUpdateRequest);

    /**
     * Update basket
     *
     * @return mixed
     */
    public function updateBasket(BasketUpdateRequest $basketUpdateRequest);

    /**
     * Empty basket
     *
     * @return mixed
     */
    public function emptyBasket();


    /**
     * Apply discount
     *
     * @param ApplyDiscountRequest $applyDiscountRequest
     */
    public function applyDiscountCode(ApplyDiscountRequest $applyDiscountRequest);


    /**
     * Remove discount
     */
    public function clearDiscountCode();

}