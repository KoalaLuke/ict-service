<?php namespace App\Repositories\Basket;

use App\Http\Requests\ApplyDiscountRequest;
use App\Http\Requests\BasketDetailsUpdateRequest;
use App\Http\Requests\BasketUpdateRequest;
use App\Models\DeliveryAddress;
use App\Models\Discount;
use App\Models\Product;
use App\Repositories\Discount\DiscountRepository;
use Illuminate\Session\SessionManager;

class SessionBasketRepository implements BasketRepository
{
    /**
     * @var \Session
     */
    private $session;
    /**
     * @var DiscountRepository
     */
    private $discountRepository;

    /**
     * SessionBasketRepository constructor.
     *
     * @param SessionManager|\Session $session
     * @param DiscountRepository $discountRepository
     */
    public function __construct(
        SessionManager $session,
        DiscountRepository $discountRepository
    )
    {
        $this->session = $session;
        $this->discountRepository = $discountRepository;
    }

    /**
     * Get the count of the items in the basket
     *
     * @return int
     */
    public function getItemCount()
    {
        $basket = $this->session->get('basket');

        $basket_count = 0;

        if($basket['items']) {

            foreach ($basket['items'] as $key => $basket_item) {

                $basket_count += $basket_item['qty'];

            }

        }

        return $basket_count;
    }

    /**
     * Add an item to the basket and return new count
     *
     * @param $item
     * @return int
     */
    public function addItem($item)
    {
        $basket = $this->session->get('basket');

        $new_item = true;

        if($basket['items']) {

            foreach ($basket['items'] as $key => $basket_item) {

                if ($basket_item['id'] == $item->id) {

                    $basket['items'][$key]['qty'] += $item->qty;

                    $new_item = false;

                }

            }

        }

        if($new_item){

            $new_item = ['id'=>$item->id, 'qty'=>$item->qty];

            $basket['items'][] = $new_item;

        }

        $this->session->put('basket.items', $basket['items']);

        $count = $this->getItemCount();

        return $count;
    }

    /**
     * Get full basket
     *
     * @return array
     */
    public function getFullBasket(){

        $basket = $this->session->get('basket');

        $fullBasket['same_address'] = isset($basket['same_address']) ? $basket['same_address'] : 'yes';
        $fullBasket['delivery_address'] = isset($basket['delivery_address']) ? $basket['delivery_address'] : '';
        $fullBasket['deliveryAddress'] = $fullBasket['same_address'] == 'no' ? DeliveryAddress::find($fullBasket['delivery_address']) : '' ;
        $fullBasket['po_number'] = isset($basket['po_number']) ? $basket['po_number'] : '';
        $fullBasket['terms_check'] = isset($basket['terms_check']) ? $basket['terms_check'] : 'no';

        $fullBasket['count'] = 0;
        $fullBasket['discounts'] = 0;
        $fullBasket['subtotal'] = 0;
        $fullBasket['code'] = isset($basket['code']) ? $basket['code'] : '';
        $fullBasket['codeDiscount'] = 0;

        $fullBasket['items'] = [];

        if(!empty($basket['items'])) {

            foreach ($basket['items'] as $item) {

                $product = Product::find($item['id']);
                $product->qty = $item['qty'];
                $product->subtotal = $product['price'] * $item['qty'];

                $product->discount = 0;

                // This is hard coded discounts based on qty
                // This is ICT Services shit idea
                if ($product->isInstallation() && $product->name != 'Bronze Installation' && (strpos($product->name,'File Server Installation') === false) ) {

                    if ($product->qty >= 2 and $product->qty <= 4) {

                        $product->discount = round( ($product->subtotal - $product->price) * 0.5, 2);

                    } elseif ($product->qty >= 5 and $product->qty <= 11) {

                        $product->discount = round( ($product->subtotal - $product->price) * 0.6, 2);

                    } elseif ($product->qty >= 12 and $product->qty <= 29) {

                        $product->discount = round( ($product->subtotal - $product->price) * 0.75, 2);

                    } elseif ($product->qty >= 30) {

                        $product->discount = round( ($product->subtotal - $product->price) * 0.775, 2);

                    }

                };

                $product->total = $product->subtotal - $product->discount;

                $fullBasket['count'] += $product->qty;
                $fullBasket['discounts'] += $product->discount;
                $fullBasket['subtotal'] += $product->total;

                $fullBasket['items'][] = $product;

            }

        }

        if(!empty($basket['code'])) {

            $fullBasket = $this->discountRepository->applyDiscountToBasket($basket['code'], $fullBasket);

        }

        $fullBasket['dummySubtotal'] = $fullBasket['subtotal'] + $fullBasket['discounts'];
        $fullBasket['vat'] = round($fullBasket['subtotal'] * 0.2,2);
        $fullBasket['total'] = $fullBasket['subtotal'] + $fullBasket['vat'];

        return $fullBasket;

    }

    /**
     * Update basket details
     *
     * @param $basket
     */
    public function updateBasketDetails(BasketDetailsUpdateRequest $basketDetailsUpdateRequest){

        $basketDetails['same_address'] = $basketDetailsUpdateRequest->get('same_address') == 'yes' ? 'yes' : 'no';
        $basketDetails['delivery_address'] = $basketDetailsUpdateRequest->get('delivery_address');
        $basketDetails['po_number'] = $basketDetailsUpdateRequest->get('po_number');
        $basketDetails['terms_check'] = $basketDetailsUpdateRequest->get('terms_check') == 'yes' ? 'yes' : 'no';

        $this->session->put('basket.same_address', $basketDetails['same_address']);
        $this->session->put('basket.delivery_address', $basketDetails['delivery_address']);
        $this->session->put('basket.po_number', $basketDetails['po_number']);
        $this->session->put('basket.terms_check', $basketDetails['terms_check']);

        return $basketDetails;
    }

    /**
     * Update basket
     *
     * @param $basket
     */
    public function updateBasket(BasketUpdateRequest $basketUpdateRequest){

        $basket['items'] = [];

        foreach($basketUpdateRequest->get('products') as $id => $item){

            if($item['qty']>0){
                $item['id'] = $id;
                $basket['items'][] = $item;
            }

        }

        $this->session->put('basket.items', $basket['items']);

        return $basket;
    }

    /**
     * Empty basket
     *
     * @param $basket
     */
    public function emptyBasket(){

        $this->session->forget('basket');

        $basket['items'] = [];
        $basket['same_address'] = '';
        $basket['delivery_address'] = '';
        $basket['po_number'] = '';
        $basket['terms_check'] = 'no';

        return $basket;
    }

    /**
     * Apply discount
     *
     * @param ApplyDiscountRequest $applyDiscountRequest
     */
    public function applyDiscountCode(ApplyDiscountRequest $applyDiscountRequest)
    {
        $code = $applyDiscountRequest->get('code');

        $this->session->put('basket.code', $code);
    }

    /**
     * Remove discount
     */
    public function clearDiscountCode()
    {
        $this->session->forget('basket.code');
    }
}