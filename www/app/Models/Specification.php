<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Specification
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $feature_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $statecode
 * @property string $icts_product
 * @property string $icts_feature
 * @property string $icts_value
 * @property string $icts_displaytext
 * @property \Carbon\Carbon $modifiedon
 * @property string $icts_productspecid
 * @property-read Product $product
 * @property-read Feature $feature
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereFeatureId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereStatecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsProduct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsFeature($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsDisplaytext($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereModifiedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification whereIctsProductspecid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Specification vendorId($value)
 */
class Specification extends Model
{
    /**
     * @var array
     */
    protected $fillable = [

        'statecode',
        'icts_product',
        'icts_feature',
        'icts_value',
        'icts_displaytext',
        'modifiedon',
        'icts_productspecid'

    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'modifiedon'
    ];

    /**
     * Modify date to correct format
     *
     * @param $date
     */
    public function setModifiedonAttribute($date){

        $date = str_replace(['T','Z'],' ',$date);
        $this->attributes['modifiedon'] = Carbon::parse($date);

    }

    /**
     * Scope for get by vendor id
     *
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeVendorId($query, $value)
    {
        return $query->where('icts_productspecid', $value);
    }

    /**
     * Relationship to product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Relationship to feature
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo(Feature::class);
    }

}
