<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Website
 *
 * @property integer $id
 * @property string $name
 * @property string $telephone
 * @property string $email
 * @property string $facebook
 * @property string $twitter
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereFacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereTwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Website whereUpdatedAt($value)
 */
class Website extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'telephone',
        'email',
        'twitter',
        'basket_text',
        'contact_text',
        'order_text',
        'terms_text'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
