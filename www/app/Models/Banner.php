<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Banner
 *
 * @property integer $id
 * @property string $name
 * @property string $link
 * @property integer $image_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Image $Image
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner whereUpdatedAt($value)
 */
class Banner extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'link',
        'active',
        'order',
        'image_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationship to image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Image()
    {
        return $this->belongsTo(Image::class);
    }
}
