<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductProducts
 *
 * @property integer $parent_id
 * @property integer $child_id
 * @property string $type
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereChildId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductProducts whereUpdatedAt($value)
 */
class ProductProducts extends Model
{
    /**
     * @var string
     */
    protected $table = 'product_products';
}