<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Installation extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'productid',
        'icts_productcategoryid',
        'order'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationship to product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class,'productid','productid');
    }

    /**
     * Relationship to category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class,'icts_productcategoryid','icts_productcategoryid');
    }

}
