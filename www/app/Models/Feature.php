<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Feature
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $statecode
 * @property string $icts_name
 * @property string $icts_comparisonmethod
 * @property integer $icts_sortorder
 * @property string $icts_parentfeature
 * @property string $icts_description
 * @property \Carbon\Carbon $modifiedon
 * @property string $icts_productfeatureid
 * @property-read \Illuminate\Database\Eloquent\Collection|self[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|Specification[] $specifications
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereStatecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsComparisonmethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsSortorder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsParentfeature($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereModifiedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature whereIctsProductfeatureid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Feature vendorId($value)
 */
class Feature extends Model
{
    /**
     * @var array
     */
    protected $fillable = [

        'statecode',
        'icts_name',
        'icts_comparisonmethod',
        'icts_sortorder',
        'icts_parentfeature',
        'icts_description',
        'modifiedon',
        'icts_productfeatureid'

    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'modifiedon'
    ];

    /**
     * Modify date to correct format
     *
     * @param $date
     */
    public function setModifiedonAttribute($date){

        $date = str_replace(['T','Z'],' ',$date);
        $this->attributes['modifiedon'] = Carbon::parse($date);

    }

    /**
     * Scope for get by vendor id
     *
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeVendorId($query, $value)
    {
        return $query->where('icts_productfeatureid', $value);
    }

    /**
     * Relationship to children
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(self::class, 'icts_parentfeature', 'icts_productfeatureid');
    }

    /**
     * Relationship to specifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specifications()
    {
        return $this->hasMany(Specification::class);
    }

}
