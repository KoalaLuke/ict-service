<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'discount',
        'subtotal',
        'vat',
        'total',
        'code',
        'organisation',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'city',
        'postcode',
        'urn',
        'note',
        'delivery_contact',
        'delivery_address_line_1',
        'delivery_address_line_2',
        'delivery_address_line_3',
        'delivery_city',
        'delivery_postcode',
        'po_number',
        'terms_check'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $visible = [
        'user_id',
        'discount',
        'subtotal',
        'vat',
        'total',
        'code',
        'organisation',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'city',
        'postcode',
        'urn',
        'note',
        'delivery_contact',
        'delivery_address_line_1',
        'delivery_address_line_2',
        'delivery_address_line_3',
        'delivery_city',
        'delivery_postcode',
        'po_number',
        'terms_check'
    ];

    /**
     * Relationship to order items
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship to order items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }
}
