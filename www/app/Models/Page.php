<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * App\Models\Page
 *
 * @property integer $id
 * @property string $name
 * @property boolean $active
 * @property string $position
 * @property integer $order
 * @property string $slug
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page position($value)
 */
class Page extends Model implements SluggableInterface
{

    use SluggableTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'content',
        'position',
        'order',
        'slug',
        'active'
    ];

    /**
     * @var array
     */
    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
        'include_trashed' => true,
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Scope the position
     *
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopePosition($query, $value)
    {
        return $query->where('position', $value);
    }

}
