<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'products_added',
        'products_updated',
        'products_deleted',
        'categories_added',
        'categories_updated',
        'categories_deleted',
        'relationships_failed',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
