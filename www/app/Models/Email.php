<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'reference',
        'subject',
        'top',
        'middle',
        'bottom'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Scope Reference
     *
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeReference($query, $value)
    {
        return $query->where('reference', $value);
    }
}
