<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'type',
        'discount'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Scope for get by code
     *
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeCode($query, $value)
    {
        return $query->where('code', $value);
    }
}
