<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryAddress extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'contact',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'city',
        'postcode',
        'user_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $visible = [
        'id',
        'contact',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'city',
        'postcode',
        'user_id'
    ];

    /**
     * Relationship to order items
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
