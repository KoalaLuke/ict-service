<?php namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Category
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $statecode
 * @property string $icts_name
 * @property string $icts_imageurl
 * @property string $icts_parentcategory
 * @property \Carbon\Carbon $modifiedon
 * @property string $icts_productcategoryid
 * @property string $slug
 * @property \Carbon\Carbon $imported_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|self[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\Specification[] $specifications
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereStatecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIctsName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIctsImageurl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIctsParentcategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereModifiedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIctsProductcategoryid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereImportedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category vendorId($value)
 */
class Category extends Model implements SluggableInterface
{

    use SluggableTrait, SoftDeletes;

    /**
     * Hard coded categories -> ICT Services shit idea
     *
     * @var string
     */
    public $serviceCategory = '37db6dc6-f052-e511-986b-001018a2a4f0';
    public $installationCategory = '9116d5d2-f052-e511-986b-001018a2a4f0';

    /**
     * @var array
     */
    protected $fillable = [
        'statecode',
        'icts_name',
        'icts_imageurl',
        'icts_parentcategory',
        'modifiedon',
        'icts_productcategoryid',
        'imported_at',
        'import_id',
        'icts_sortorder',
    ];

    /**
     * @var array
     */
    protected $sluggable = [
        'build_from' => 'icts_name',
        'save_to'    => 'slug',
        'include_trashed' => true,
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'imported_at',
        'modifiedon'
    ];

    /**
     * @param $date
     */
    public function setModifiedonAttribute($date)
    {
        $date = str_replace(['T','Z'],' ',$date);
        $this->attributes['modifiedon'] = Carbon::parse($date);

    }

    /**
     * Scope for get by vendor id
     *
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeVendorId($query, $value)
    {
        return $query->where('icts_productcategoryid', $value);
    }

    /**
     * Relationship to products
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category')->withTimestamps();
    }

    /**
     * Relationship to children
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(self::class, 'icts_parentcategory', 'icts_productcategoryid')->orderBy(\DB::raw('-icts_sortorder'),'DESC')->orderBy('icts_name','ASC');
    }

    /**
     * Relationships to parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(self::class, 'icts_parentcategory', 'icts_productcategoryid');
    }

    /**
     * Relationship to specification
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specifications()
    {
        return $this->hasMany(Specification::class);
    }

    /**
     * Relationship to installations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function installations()
    {
        return $this->hasMany(Installation::class,'icts_productcategoryid','icts_productcategoryid');
    }

}