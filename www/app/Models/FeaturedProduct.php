<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FeaturedProduct
 *
 * @property integer $id
 * @property string $productid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Product $Product
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeaturedProduct whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeaturedProduct whereProductid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeaturedProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeaturedProduct whereUpdatedAt($value)
 */
class FeaturedProduct extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'productid'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationship to image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Product()
    {
        return $this->belongsTo(Product::class,'productid','productid');
    }
}
