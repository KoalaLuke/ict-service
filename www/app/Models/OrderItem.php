<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'order_id',
        'qty',
        'subtotal',
        'discount',
        'total',
        'productid',
        'statecode',
        'name',
        'icts_saleswebsite',
        'productnumber',
        'price',
        'icts_rrp',
        'createdon',
        'modifiedon',
        'description',
        'producturl',
        'edict_imageurl',
        'icts_additionalimageurls',
        'icts_usermanualurl',
        'edict_productcategory',
        'icts_category',
        'icts_keywords',
        'producttypecode',
        'vendorname',
        'vendorpartnumber',
        'suppliername',
        'edict_eta',
        'isstockitem',
        'quantityonhand',
        'stockvolume',
        'stockweight',
        'transactioncurrencyid',
        'note'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationship to order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Order()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
