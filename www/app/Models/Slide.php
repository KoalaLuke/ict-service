<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Slide
 *
 * @property integer $id
 * @property string $name
 * @property string $header
 * @property string $text
 * @property string $button
 * @property string $link
 * @property integer $image_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Image $Image
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereHeader($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereButton($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slide whereUpdatedAt($value)
 */
class Slide extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'header',
        'text',
        'button',
        'link',
        'image_id',
        'order',
        'active'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationship to image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Image()
    {
        return $this->belongsTo(Image::class);
    }
}
