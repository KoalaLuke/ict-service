<?php namespace App\Models;

use App\Repositories\Comparison\ComparisonRepository;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Product
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $productid
 * @property string $statecode
 * @property string $name
 * @property string $icts_saleswebsite
 * @property string $productnumber
 * @property float $price
 * @property string $icts_rrp
 * @property \Carbon\Carbon $createdon
 * @property \Carbon\Carbon $modifiedon
 * @property string $description
 * @property string $producturl
 * @property string $edict_imageurl
 * @property string $icts_additionalimageurls
 * @property string $icts_usermanualurl
 * @property string $edict_productcategory
 * @property string $icts_category
 * @property string $icts_keywords
 * @property string $producttypecode
 * @property string $vendorname
 * @property string $vendorpartnumber
 * @property string $suppliername
 * @property integer $edict_eta
 * @property string $isstockitem
 * @property string $quantityonhand
 * @property integer $stockvolume
 * @property integer $stockweight
 * @property string $transactioncurrencyid
 * @property string $slug
 * @property \Carbon\Carbon $imported_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|Specification[] $specifications
 * @property-read \Illuminate\Database\Eloquent\Collection|Relationship[] $relationships
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProductid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereStatecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsSaleswebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProductnumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsRrp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCreatedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereModifiedon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProducturl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereEdictImageurl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsAdditionalimageurls($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsUsermanualurl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereEdictProductcategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIctsKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProducttypecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereVendorname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereVendorpartnumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereSuppliername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereEdictEta($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereIsstockitem($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereQuantityonhand($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereStockvolume($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereStockweight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereTransactioncurrencyid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereImportedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product vendorId($value)
 */
class Product extends Model implements SluggableInterface
{

    use SluggableTrait, SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'statecode',
        'icts_saleswebsite',
        'productnumber',
        'name',
        'price',
        'icts_rrp',
        'createdon',
        'modifiedon',
        'description',
        'producturl',
        'edict_imageurl',
        'icts_additionalimageurls',
        'icts_usermanualurl',
        'edict_productcategory',
        'icts_category',
        'icts_keywords',
        'producttypecode',
        'vendorname',
        'vendorpartnumber',
        'suppliername',
        'edict_eta',
        'isstockitem',
        'quantityonhand',
        'stockvolume',
        'stockweight',
        'productid',
        'transactioncurrencyid',
        'imported_at',
        'import_id'
    ];

    /**
     * @var array
     */
    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
        'include_trashed' => true,
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'imported_at',
        'modifiedon',
        'createdon'
    ];

    /**
     * Hard coded categories -> ICT Services shit idea
     *
     * @var string
     */
    public $serviceCategory = '37db6dc6-f052-e511-986b-001018a2a4f0';
    public $installationCategory = '9116d5d2-f052-e511-986b-001018a2a4f0';

    /**
     * Modify date to correct format
     *
     * @param $date
     */
    public function setCreatedonAttribute($date){

        $date = str_replace(['T','Z'],' ',$date);
        $this->attributes['createdon'] = Carbon::parse($date);

    }

    /**
     * Modify date to correct format
     *
     * @param $date
     */
    public function setModifiedonAttribute($date){

        $date = str_replace(['T','Z'],' ',$date);
        $this->attributes['modifiedon'] = Carbon::parse($date);

    }

    /**
     * Additional Image Urls to array
     *
     * @param $string
     * @return array
     */
    public function getIctsAdditionalimageurlsAttribute($string)
    {
        $image_urls = explode(PHP_EOL, $string);

        foreach($image_urls as $key => $image_url){

            $image_urls[$key] = trim($image_url);

        }

        if(empty($image_url[0])){
            $image_urls = [];
        }

        return $image_urls;
    }

    /**
     * Scope for get by vendor id
     *
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeVendorId($query, $value)
    {
        return $query->where('productid', $value);
    }

    /**
     * Whether the production is an installation or not
     *
     * @return bool
     */
    public function isInstallation()
    {
        return $this->icts_category == $this->installationCategory;

    }

    /**
     * Relationship to categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category')->withTimestamps();
    }

    /**
     * Relationship to specifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specifications()
    {
        return $this->hasMany(Specification::class);
    }

    /**
     * Relationship to specifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relationships()
    {
        return $this->hasMany(Relationship::class);
    }

    /**
     * Relationship to products
     *
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_products', 'parent_id', 'child_id')->withTimestamps()->withPivot('type');
    }

    /**
     * Relationship to alternative products
     *
     * @return HasMany
     */
    public function alternativeProducts()
    {
        return $this->products()->wherePivot('type', 'alternative');
    }

    /**
     * Relationship to associated products
     *
     * @return HasMany
     */
    public function associatedProducts()
    {
        return $this->products()->wherePivot('type', 'associated');
    }

    /**
     * is in comparison
     *
     * @return bool
     */
    public function isInComparison()
    {
        return app(ComparisonRepository::class)->isProductInComparison($this);
    }


}
