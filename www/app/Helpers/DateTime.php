<?php namespace App\Helpers;

use Carbon\Carbon;

class DateTime
{
    /**
     * Change date format
     *
     * @param $date
     * @param $display_date
     */
    public static function displayDate($date)
    {
        $displayDate = new Carbon($date);

        return $displayDate->format('d/m/Y');
    }

    /**
     * Change date format
     *
     * @param $date
     * @param $display_date
     */
    public static function displayDateTime($dateTime)
    {
        $displayDateTime = new Carbon($dateTime);

        return $displayDateTime->format('d/m/Y, H:i:s');
    }

}