<?php namespace App\Helpers;

class Breadcrumb
{
    /**
     * @var array
     */
    private static $breadcrumbs;

    /**
     * Set a breadcrumb
     *
     * @param $name
     * @param $link
     */
    public static function add($name, $link)
    {
        self::$breadcrumbs[] = (object) [
            'name' => $name,
            'link' => $link
        ];
    }

    /**
     * Get all the bread
     *
     * @return array
     */
    public static function all()
    {
        return self::$breadcrumbs;
    }
}